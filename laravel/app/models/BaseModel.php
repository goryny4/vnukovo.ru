<?php

class BaseModel extends Eloquent {

    public function save(array $options = [], $writeInLogs = true) {

        $operation = isset($this->id) ? 'update' : 'create';

        if (in_array(get_class($this),['News','CmsPages'])) {
            /*

            $parser = new JBBCode\Parser();
            $parser->addCodeDefinitionSet(new JBBCode\DefaultCodeDefinitionSet());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('h1', '<h1>{param}</h1>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('h2', '<h2>{param}</h2>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('h3', '<h3>{param}</h3>'))->build());

            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('table', '<table>{param}</table>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('th', '<th>{param}</th>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('tr', '<tr>{param}</tr>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('td', '<td>{param}</td>'))->build());
            $parser->addCodeDefinition((new JBBCode\CodeDefinitionBuilder('internal-link', '<internal-link>{param}</internal-link>'))->build());
*/
            foreach (['ru', 'en'] as $lang) {
                $this->{'text_' . $lang} = (new WikiParser)->parse(htmlentities($this->{"bbcode_$lang"}));

//                $parser->parse(htmlentities($this->{"bbcode_$lang"}));
  //              $this->{'text_' . $lang} = $parser->getAsHtml();
            }
        }

        parent::save();
        if (in_array(get_class($this),['News','Ads','Maps','Cities','Airlines'])) {
           $this->_setImagePath($operation);
           parent::save();
        }


        if ($writeInLogs) { // if we chose standard option, we won't write it in logs
            Event::fire('log.register', ['data' => [
                'operation' => $operation,
                'category' => isset($this->object_type_id) ? $this->objectType->name : str_replace('_', '-', snake_case(get_class($this))), //foo-bar format
                'entity_id' => $this->id,
            ]
            ]);
        }


        return 1;
    }

    private function _setImagePath($operation)
    {
        $entity = lcfirst(get_class($this));
        $tmpId = ($operation == 'create'?0:$this->id);
        $id = $this->id;

        foreach (['ru', 'en'] as $lang) {
            $userId = Auth::id();
            $tmpDir = storage_path("images/tmp/$userId/$entity/$tmpId/$lang/");
            if (File::isDirectory($tmpDir)) {
                $images = scandir($tmpDir);
                $images = array_slice($images, 2);
                if (count($images)) {
                    $this->{"image_$lang"} = NULL;

                    $folderDir = storage_path("images/$entity/$id/$lang/");
                    if (!File::isDirectory($folderDir)) {
                        File::makeDirectory($folderDir, $mode = 0777, true, true);
                    }
                    File::deleteDirectory($folderDir,true);

                    File::move($tmpDir.$images[0], $folderDir.$images[0]);
                    $this->{"image_$lang"} = "images/$entity/$id/$lang/".$images[0];
                }
            }
        }
        File::deleteDirectory(storage_path("images/tmp/$userId/$entity/"),true); // думаю, не слишком жёстко
    }


    public static function destroy($id) {
        if (is_array($id)) {
            foreach ($id as $i) self::destroy($i);
        } else {
            $model = get_called_class();
            $entity = $model::find($id);
            parent::destroy($id);

            Event::fire('log.register', ['data' => [
                'operation' => 'delete',
                'category' => isset($entity->object_type_id) ? snake_case($entity->objectType->name) : str_replace('_', '-', snake_case($model)), //foo-bar format
                'entity_id' => $id,
            ]
            ]);

            return 1;
        }
    }

    // возможно, сделаю темп-директорию.. на случай если пользователь не нажал Сохранить
    private function _assignImagesFromTmp() {
        $entity = lcfirst(get_class($this));
        $tmpPath = storage_path()."/images/$entity/0";
        $newPath = storage_path()."/images/$entity/$this->id";
        File::copyDirectory($tmpPath,$newPath);
        File::deleteDirectory($tmpPath);
        return $entity;
    }

}