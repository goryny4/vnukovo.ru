<?php

class CommercialObject extends BaseModel {

    protected $fillable = ['commercial_object_type_id', 'name_ru', 'name_en', 'description_ru', 'description_en', 'terminal', 'floor',
    'time_start', 'time_end', 'centre_lat', 'centre_lon'];
    static public $outputable = ['commercial_object_type_id' => '', 'name_ru' => '', 'name_en' => '', 'description_ru' => '', 'description_en' => '', 'terminal' => 'A', 'floor' => '1',
        'time_start' => '', 'time_end' => '', 'centre_lat' => '', 'centre_lon' => ''];
    static public $list = ['id','commercial_object_type_id', 'name_ru', 'name_en', 'description_ru', 'description_en', 'terminal', 'floor',
        'time_start', 'time_end', 'centre_lat', 'centre_lon', 'created_at', 'updated_at'];

    public function objectType() {
        return $this->belongsTo('ObjectType', 'commercial_object_type_id');
    }

    public function specialProposals() {
        return $this->hasMany('SpecialProposal', 'commercial_object_id');
    }

    public function scopeLang($query, $lang)
    {
        return $query->select(array("name_$lang","name_$lang as name","description_$lang", "description_$lang as description", 'terminal', 'floor', 'time_start', 'time_end', 'centre_lat', 'centre_lon'));
    }

    public static $typeNamesSing = ['duty-free' => 'Магазин Duty Free', 'car-rent' => 'Сервис проката авто', 'cafes-and-restaurants' => 'Кафе / ресторан'];
    public static $typeNamesPl = ['duty-free' => 'Магазины Duty Free', 'car-rent' => 'Сервисы проката авто', 'cafes-and-restaurants' => 'Кафе / рестораны'];

}