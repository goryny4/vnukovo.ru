<?php

class Flight extends BaseModel {

    protected $fillable = [
        'flt_id', 'bound', 'systimestamp', 'flight_number_for_pass', 'airline_vnt', 'airline_name_rus', 'airline_name_eng', 'st', 'et_for_pass',
        'at_for_pass', 'vnukovo_ru_status', 'ac_type_name_rus', 'ac_reg', 'term', 'handling_type_for_pass', 'category' ,'fls_id', 'fls_full_name_rus',
        'fls_full_name_eng', 'fls_st', 'fls_et', 'fls_at_for_pass', 'vnukovo_ru_status_short', 'vnukovo_en_status', 'vnukovo_en_status_short', 'codeshare_1',
        'codeshare_2', 'checkin_list_actual_for_pass', 'checkin_sst', 'checkin_set', 'checkin_ast', 'checkin_aet', 'checkin_list_with_status', 'gate_st',
        'gate_et', 'gates_list_for_pass', 'delay_name_pas_rus', 'delay_name_pas_eng', 'checkin_business', 'via1_id', 'via1_full_name_rus', 'via1_full_name_eng',
        'via1_sta', 'via1_eta', 'via1_std', 'via1_etd', 'via1_ata_for_pass', 'via2_id', 'via2_full_name_rus', 'via2_full_name_eng', 'via2_sta', 'via2_std',
        'via1_atd_for_pass', 'via2_eta', 'via2_etd'
    ];

    public static $flightFields = ['flt_id' => '', 'bound' => '', 'systimestamp' => '', 'flight_number_for_pass' => '', 'airline_vnt' => '',
        'airline_name_rus' => '', 'airline_name_eng' => '', 'st' => '', 'et_for_pass' => '', 'at_for_pass' => '', 'vnukovo_ru_status' => '',
        'ac_type_name_rus' => '', 'ac_reg' => '', 'term' => '', 'handling_type_for_pass' => '', 'category' => '' ,'fls_id' => '', 'fls_full_name_rus' => '',
        'fls_full_name_eng' => '', 'fls_st' => '', 'fls_et' => '', 'fls_at_for_pass' => '', 'vnukovo_ru_status_short' => '', 'vnukovo_en_status' => '',
        'vnukovo_en_status_short' => '', 'codeshare_1' => '', 'codeshare_2' => '', 'checkin_list_actual_for_pass' => '', 'checkin_sst' => '',
        'checkin_set' => '', 'checkin_ast' => '', 'checkin_aet' => '', 'checkin_list_with_status' => '', 'gate_st' => '', 'gate_et' => '',
        'gates_list_for_pass' => '', 'delay_name_pas_rus' => '', 'delay_name_pas_eng' => '', 'checkin_business' => '', 'via1_id' => '', 'via1_full_name_rus' => '',
        'via1_full_name_eng' => '', 'via1_sta' => '', 'via1_eta' => '', 'via1_std' => '', 'via1_etd' => '', 'via1_ata_for_pass' => '', 'via2_id' => '',
        'via2_full_name_rus' => '', 'via2_full_name_eng' => '', 'via2_sta' => '', 'via2_std' => '', 'via1_atd_for_pass' => '', 'via2_eta' => '', 'via2_etd' => '',
        'city_ru'=>'','city_en'=>'',
        'airport_ru'=>'','airport_en'=>'',
        'ac_type_name_eng' => '',
    ];

    public function scopeLang($query, $lang, $type = 'list')
    {
        //determining 3-letter locale name
        $locale = ($lang == 'ru')?'rus':'eng';

        switch ($type) {
            case 'detailed': $fields = [
            //    "id",
                "flt_id","flight_number_for_pass",
                'city_en', "city_$lang as city", "airport_$lang as airport",  // "fls_full_name_$locale as fls_full_name",
                'st', "et_for_pass",
                "vnukovo_${lang}_status_short as vnukovo_status_short",
                'airline_vnt', "airline_name_$locale as airline_name","ac_type_name_$locale as ac_type_name",
                "term",
                'checkin_list_actual_for_pass',
                'gates_list_for_pass',
            ]; break;
            case 'full': $fields = [
                'airline_vnt', "airline_name_$locale as airline_name", "vnukovo_$lang"."_status  as vnukovo_short", // "fls_full_name_$locale as fls_full_name",
                'city_en', "city_$lang as city", "airport_$lang as airport",
                "vnukovo_$lang"."_status_short as vnukovo_status_short", "delay_name_pas_$locale as delay_name_pas",
                "via1_full_name_$locale as via1_full_name", "via2_full_name_$locale as via2_full_name", 'flt_id','bound', 'systimestamp', 'flight_number_for_pass', 'airline_vnt', 'st', 'et_for_pass', 'at_for_pass',
                "ac_type_name_$locale as ac_type_name", 'ac_reg', 'term', 'handling_type_for_pass', 'category' ,'fls_id',  'fls_st', 'fls_et', 'fls_at_for_pass',  'codeshare_1',
                'codeshare_2', 'checkin_list_actual_for_pass', 'checkin_sst', 'checkin_set', 'checkin_ast', 'checkin_aet', 'checkin_list_with_status', 'gate_st',
                'gate_et', 'gates_list_for_pass', 'checkin_business', 'via1_id','via1_sta', 'via1_eta', 'via1_std', 'via1_etd', 'via1_ata_for_pass', 'via2_id', 'via2_sta', 'via2_std',
                'via1_atd_for_pass', 'via2_eta', 'via2_etd',
            ]; break;
            default:
                $fields = [
                    'bound',
                    "flt_id","flight_number_for_pass",
                    'st',
                    "city_$lang as city", "airport_$lang as airport",
                    "vnukovo_${lang}_status_short as vnukovo_status_short",
                    "term",
                ];
        }
        return $query->select($fields);
    }

    public function airline()
    {
        return $this->hasOne('Airlines','airline_vnt','airline_vnt');
    }

    public function cityRel()
    {
        return $this->hasOne('Cities','city_en','city_en');
    }


}