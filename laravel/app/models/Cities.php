<?php

class Cities extends BaseModel {

    protected $fillable = ['image_ru', 'image_en', 'version'];
    static public $outputable = ['image_ru' => '','image_en' => ''
     //   ,'created_at'=>'','updated_at'=>''
    ];
    static public $list = ['id', 'city_en', 'image_ru', 'image_en', 'version', 'created_at', 'updated_at'];

    protected $table = 'cities';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("city_en, COALESCE(image_$lang,image_$otherLang) as image, version, weather_t, weather_text"));
    }
}