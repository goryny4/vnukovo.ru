<?php

class AdsView extends BaseModel {

    protected $fillable = ['user_id', 'ad_id'];

    public function user() {
        return $this->belongsTo('User');
    }

    public function ad()
    {
        return $this->belongsTo('Ads');
    }
}