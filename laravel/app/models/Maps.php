<?php

class Maps extends BaseModel {

    protected $fillable = ['image_ru', 'image_en', 'terminal', 'floor', 'version'];
    static public $outputable = ['image_ru' => '','image_en' => ''
     //   ,'created_at'=>'','updated_at'=>''
    ];
    static public $list = ['id', 'image_ru', 'image_en', 'moder_id as moder', 'terminal', 'floor', 'version', 'created_at', 'updated_at'];
    protected $table = 'maps';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("COALESCE(image_$lang,image_$otherLang) as image, terminal, floor, version"));
    }
}