<?php

class UserDevice extends BaseModel {
   protected $fillable = ['user_id','device_type','device_id','registration_id'];

   protected $table = 'user_devices';

    public function user() {
        $this->belongsTo('User','user_id','id');
    }

}