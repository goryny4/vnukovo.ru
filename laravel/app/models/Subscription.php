<?php

class Subscription extends BaseModel {

    protected $fillable = ['user_id', 'flt_id'];
    protected $table = 'subscriptions';

    public function user() {
        return $this->belongsTo('User');
    }

    public function flight() {
        return $this->belongsTo('Flight','flt_id','flt_id');
    }

    public function userDevices()
    {
        return $this->hasMany('UserDevice', 'user_id','user_id');
    }

}