<?php

class OperationLog extends Eloquent {

    protected $table = 'logs';

    protected $fillable = ['operation', 'moder_id', 'moder_name', 'moder_group', 'request_ip', 'category', 'entity_id'];
}