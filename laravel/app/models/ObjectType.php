<?php

class ObjectType extends BaseModel {

    protected $fillable = ['location_id', 'type', 'picture_url_ru', 'target_url_ru', 'picture_url_en', 'target_url_en', 'times_shown', 'times_opened'];

    public $timestamps = false;

    protected $table = 'commercial_object_types';
}