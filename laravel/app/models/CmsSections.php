<?php

class CmsSections extends BaseModel {

    protected $fillable = ['section', 'root', 'title_ru', 'title_en'];
    static public $outputable = ['root' => '', 'section' => '', 'title_ru' => '', 'title_en' => ''];
    static public $list =  ['root', 'section', 'title_ru', 'title_en'];
    protected $table = 'cms_sections';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("section, COALESCE(title_$lang,title_$otherLang) as title"));
    }

    public function pages() {
        return $this->hasMany('CmsPages','section','section');
    }

    public $timestamps = false;
}