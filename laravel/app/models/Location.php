<?php

class Location extends BaseModel {

    protected $fillable = ['id', 'object_type_id', 'name', 'entity_name'];

    public function objectType() {
        return $this->belongsTo('ObjectType', 'object_type_id');
    }

    public $timestamps = false;

}