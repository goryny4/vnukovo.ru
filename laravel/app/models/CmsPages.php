<?php

class CmsPages extends BaseModel {

    protected $fillable = ['title_ru', 'is_primary','title_en', 'section', 'image_ru', 'image_en', 'creator_id','bbcode_ru', 'bbcode_en'];
    static public $outputable = ['section' => '','is_primary' => '','image_ru' => '','image_en' => '', 'creator_id' => '',  'bbcode_ru'=>'', 'bbcode_en'=>'',  'title_ru' => '', 'title_en' => '', 'text_ru' => '', 'text_en' => ''
     //   ,'created_at'=>'','updated_at'=>''
    ];
    static public $list = ['id','section', 'is_primary', 'image_ru', 'image_en', 'creator_id', 'bbcode_en', 'title_ru', 'bbcode_ru', 'title_en', 'created_at','updated_at'];
    protected $table = 'cms_pages';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("id, is_primary, COALESCE(image_$lang,image_$otherLang) as image, title_$lang as title, text_$lang as text"));
    }

    public function scopeAdmin($query)
    {
        $lang = 'ru'; $otherLang = 'en';
        return $query->select(DB::raw("id, is_primary, section, COALESCE(image_$lang,image_$otherLang) as image, title_$lang as title, text_$lang as text"));
    }

    public function section() {
        return $this->belongsTo('CmsSections','section');
    }
}