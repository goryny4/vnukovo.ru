<?php

class Airlines extends BaseModel {

    protected $fillable = ['airline_vnt','airline_name_rus','image_ru', 'image_en', 'version'];
    static public $outputable = ['image_ru' => '','image_en' => ''
     //   ,'created_at'=>'','updated_at'=>''
    ];
    static public $list = ['id', 'airline_vnt', 'airline_name_rus', 'image_ru', 'image_en', 'version', 'created_at', 'updated_at'];
    protected $table = 'airlines';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("airline_vnt, airline_name_rus as airline_name, COALESCE(image_$lang,image_$otherLang) as image, version"));
    }
}