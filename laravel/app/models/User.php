<?php

class User extends BaseModel {

    protected $fillable = ['email', 'first_name', 'last_name', 'social_network', 'social_network_id', 'confirmation_code'];

    protected $guarded = ['token','confirmation_code'];

    public function devices() {
        return $this->hasMany('UserDevice','user_id','id');
    }


    public function flights()
    {
        return $this->hasManyThrough('Flight','Subscription');
    }


    public function subscriptions()
    {
        return $this->hasMany('Subscription');
    }



    public function adsViews()
    {
        return $this->hasMany('AdsView');
    }


}