<?php

class News extends BaseModel {

    protected $fillable = ['image_ru', 'image_en', 'creator_id', 'title_ru', 'title_en', 'bbcode_ru', 'bbcode_en', 'published_at'];
    static public $outputable = ['image_ru' => '','image_en' => '', 'creator_id' => '',  'bbcode_ru'=>'', 'bbcode_en'=>'',  'title_ru' => '', 'title_en' => '', 'text_ru' => '', 'text_en' => '', 'published_at' => ''
     //   ,'created_at'=>'','updated_at'=>''
    ];
    static public $list = ['id','image_ru', 'image_en', 'creator_id', 'bbcode_en', 'title_ru', 'bbcode_ru', 'title_en', 'published_at','created_at','updated_at'];
    protected $table = 'news';

    public function scopeLang($query,$lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("id, COALESCE(image_$lang,image_$otherLang) as image, published_at, title_$lang as title, text_$lang as text"));
    }
}