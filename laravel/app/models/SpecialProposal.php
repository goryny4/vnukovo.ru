<?php

class SpecialProposal extends BaseModel {

    protected $fillable = ['commercial_object_id', 'picture_url_en', 'picture_url_ru', 'title_en', 'title_ru', 'description_en',
    'description_ru', 'priority'];

    static public $outputable = ['title_ru','description_ru'];
    static public $list = ['title_ru','description_ru'];

    public function commercialObject() {
        return $this->belongsTo('CommercialObject', 'commercial_object_id');
    }

    public function scopeLang($query,$lang)
    {
        return $query->select(array("title_$lang as title","description_$lang as description"));
    }
}