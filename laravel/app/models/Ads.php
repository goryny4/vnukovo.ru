<?php

class Ads extends BaseModel {

    protected $fillable = ['location_id', 'name', 'type', 'image_ru', 'target_url_ru', 'image_en', 'target_url_en', 'times_shown', 'times_opened'];
    public static $outputable = ['location_id' => '', 'name' => '', 'type' => '', 'image_ru' => '', 'target_url_ru' => '', 'image_en' => '', 'target_url_en' => ''];
    public static $list = ['id', 'location_id', 'name', 'type', 'image_ru', 'target_url_ru', 'image_en', 'target_url_en', 'created_at', 'updated_at'];

    public function location() {
        return $this->belongsTo('Location', 'location_id');
    }

    public function scopeLang($query, $lang)
    {
        $otherLang = str_replace($lang,'','ruen');
        return $query->select(DB::raw("id, COALESCE(image_$lang,image_$otherLang) as image, COALESCE(target_url_$lang,target_url_$otherLang) as target_url"));
    }

    static public function fillWithAds($locationName, $source, $lang)
    {
        $pagesize = 10;

        $adsCurrentOffsetKey = 'adsCurrentOffset' . $locationName;
        $adsCountPerPageKey = 'adsCountPerPage' . $locationName;
        $adsCurrentTypeOffset = Cache::rememberForever($adsCurrentOffsetKey, function () {
            return 0;
        });

        // -- for testing only
    //    Cache::forget($adsCountPerPageKey);

        $adsCountPerPage = Cache::rememberForever($adsCountPerPageKey, function () {
            return Config::get('params.adsCountPerPage', 2);
        });

        $adsCountPerCurrentPage = floor(count($source)/$pagesize*$adsCountPerPage);

        $adBlocks = array();
        $isLooped = false;
        while (count($adBlocks) < $adsCountPerCurrentPage) {
            $adBlocks = array_merge($adBlocks,self::lang($lang)->whereHas('location', function ($q) use ($locationName) {
                $q->where('name', $locationName);
            })->take($adsCountPerCurrentPage - count($adBlocks))->offset($adsCurrentTypeOffset)->get()->toArray());

            if (count($adBlocks) < $adsCountPerCurrentPage) {
                $adsCurrentTypeOffset = 0;
                Cache::forever($adsCurrentOffsetKey, count($adBlocks)?$adsCountPerCurrentPage-count($adBlocks):0); // с заделом на будущее, потом добавится в плюс :)
            } elseif (!$isLooped) {
                Cache::increment($adsCurrentOffsetKey, $adsCountPerCurrentPage);
            }
            $isLooped = true;
        }
        if (!count($adBlocks)) $adBlocks = [0=>false];

        // ------------

        $result = array();
        $stepFloat = $adsCountPerCurrentPage?count($source)/$adsCountPerCurrentPage:0;

        $k = 1;

        foreach ($source as $i=>$s) {
            $result[] = ['feedItemType'=>'item','feedItemContent'=>$s];
            if (($i+1) == ceil(($k * $stepFloat)) ) {
                // если не false
                if (is_array($adBlock = current($adBlocks))) {
                    Ads::where('id',$adBlock['id'])->first()->increment('times_shown');
                }
                $result[] = ['feedItemType'=>'ad','feedItemContent'=>array_shift($adBlocks)];
                $k++;
            }
        }
/*
        foreach ($adBlocks as $key=>$block) {
            while ($i++<round($stepFloat*($key+1)+$key)) {
                $result[] = ['feedItemType'=>'item','feedItemContent'=>array_shift($source)];
            }
            if ($block) $result[] = ['feedItemType'=>'ad','feedItemContent'=>$block];
        }
        foreach ($source as $s) $result[] = ['feedItemType'=>'item','feedItemContent'=>$s];
*/
        return $result;
    }

    function adsViews() {
        return $this->hasMany('adsView');
    }
}