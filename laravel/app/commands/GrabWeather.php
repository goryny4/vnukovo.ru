<?php

use Illuminate\Console\Command;

class GrabWeather extends Command {
    protected $name = 'command:weather';

    public function fire()
    {
        $cities = array_column(Cities::all(['city_en'])->toArray(), 'city_en');
        foreach ($cities as $city) {
            $wc = current(DB::table('cities')->where('city_en', $city)->get(['weather_id','city_en']));
            $request = $wc->weather_id?"id=$wc->weather_id":"q=$wc->city_en";
            try {
                $result = file_get_contents("http://api.openweathermap.org/data/2.5/weather?$request&units=metric");
            } catch (Exception $e) {
                $this->info($city . ': ERROR 500');
                continue;
            }
            $json = json_decode($result, true);
            if (isset($json['id'])) {
                DB::table('cities')->where('city_en', $city)->update([
                    'weather_id' => $json['id'],
                    'weather_t' => isset($json['main']['temp'])?$json['main']['temp']:0,
                    'weather_text' => $json['weather'][0]['main']
                ]);
                $this->info($city . ': ' . $json['id']);
            } else {
              //  DB::table('cities')->where('city_en', $city)->update(['weather_id' => null]);
                $this->info($city . ': NULL');
            }
            sleep(0.2);
        }
    }

}