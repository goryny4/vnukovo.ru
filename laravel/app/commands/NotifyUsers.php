<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class NotifyUsers extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:notify';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Notify users about theirs flights.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info($a = "NOTIFY\n");
        echo $a;

        $flights = $this->argument('flights');
        $androidRegistrationIds = $this->argument('androidRegistrationIds');
        $iosRegistrationIds = $this->argument('iosRegistrationIds');

        foreach ($flights as $flight) {
            echo "Flight id: $flight[flt_id]\n";
            $this->info('Flight id: ' . $flight['flt_id']);
            $this->info($a = ('$androidRegistration ids: ' . json_encode($androidRegistrationIds)));
            echo $a;
            echo json_encode($flight);
            $image = isset($flight['city_rel']) ? $flight['city_rel']['image_ru'] : '/images/cities/initial/Tbilisi.jpg';
            $info = [
                'flt_id' => $flight['flt_id'],
                'ticker_text' => $flight['vnukovo_ru_status'],
                'title_text' => $flight['city_ru'],
                'notification_text' => $flight['vnukovo_ru_status'],
                'image' => $image,
            ];
            $message = PushNotification::Message('Hey',$info);
            $this->push('androidProd', $message, $androidRegistrationIds[$flight['flt_id']]);
/*
            $message = PushNotification::Message('Hey',[
                "aps" => [
                    "alert"=>$flight['city_ru'],
                    "sound"=>"default"
                ],
                "data"=>$info
            ]);
*/

            $message = PushNotification::Message($flight['city_ru'].':'.$flight['vnukovo_ru_status'],array(
                "alert" => [
                    "title" => $flight['city_ru'],
                    "body" => $flight['vnukovo_ru_status']
                ],
                'custom' => $info
            ));

            $this->push(
                // 'iosProd',
                'iosDev',
                $message, $iosRegistrationIds[$flight['flt_id']]
            );

        }

        $this->_unsubscribe([1,2,3,4,5]);
    }

    private function _unsubscribe($array) {
        return true;
    }

    public function push($config, $message, $registrationIds) {
        $devices = [];
        foreach ($registrationIds as $regId) {
            $devices[] = PushNotification::Device($regId);
        }

        $devices = PushNotification::DeviceCollection($devices);

        $collection = PushNotification::app($config)
            ->to($devices)
            ->send($message);

        foreach ($collection->pushManager as $push) {
            $response = $push->getAdapter()->getResponse();
            echo "$config ".json_encode($response);
        }
    }

    public function androidOld($flights, $androidRegistrationIds) {
        echo '$flights count = '.count($flights)."\n";
        echo '$androidRegistrationIds count = '.count($androidRegistrationIds)."\n";

        //    $userDevices = Subscription::with('userDevices')->whereIn('flight_id',$updatedFlightIds)->get()->toArray();
        //    die(json_encode($userDevices)."\n");
// API access key from Google API's Console
        $key = 'AIzaSyCEhf1zYwEmkRifh-87v17RC5OtoKHqfvg'; // vnukovoapp@gmail.com
        //     $key = 'AIzaSyDGL9FVZlNTsNtk4J7r2-utLEyzTu7UIII'; // egoryny4@gmail.com
        define( 'API_ACCESS_KEY', $key );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// prep the bundle
        $msg = [
            'flt_id' => 2880182,
            'ticker_text'=> 'Рейс задерживается на 20 минут',
            'title_text'		=> 'Санкт-Петербург',
            'notification_text'	=> 'Рейс задерживается на 20 минут',
            'image' => '/images/cities/initial/London.jpg',
            //    'vibrate'	=> 1,
            //    'sound'		=> 1,
            //    'largeIcon'	=> 'large_icon',
            //    'smallIcon'	=> 'small_icon',
        ];

        foreach ($flights as $flight) {
            echo "Flight id: $flight[flt_id]\n";
            $this->info('Flight id: ' . $flight['flt_id']);
            $this->info($a = ('$androidRegistration ids: ' . json_encode($androidRegistrationIds)));
            echo $a;
            echo json_encode($flight);
            $msg = [
                'flt_id' => $flight['flt_id'],
                'ticker_text' => $flight['vnukovo_ru_status'],
                'title_text' => $flight['city_ru'],
                'notification_text' => $flight['vnukovo_ru_status'],
                'image' => isset($flight['city_rel']) ? $flight['city_rel']['image_ru'] : '/images/cities/initial/Tbilisi.jpg',
            ];

            $fields = array
            (
                'registration_ids' => $androidRegistrationIds[$flight['flt_id']],
                'data' => $msg
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);

            echo $result . "\n";
        }
    }

    /**
     * This example sends one pushnotification with an alert to Apples production push servers
     */
    public function ios($flights,$iosRegistrationIds)
    {
/*
        $iosRegistrationIds = '15dcdfd1127268a245d9f0ea12d0a10e74b6c99c744dee9fb0a894fd26c37937';
        // First we get a Notificato instance and tell it what certificate to use as default certificate
        PushNotification::app('iosDev')
            ->to($iosRegistrationIds)
            ->send('Hello World, i`m a push message');


        // ------------

        foreach ($flights as $flight) {

        }
*/
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('flights', InputArgument::OPTIONAL, 'Flights'),
            array('androidRegistrationIds', InputArgument::OPTIONAL, 'Android RegistrationIds ids'),
            array('iosRegistrationIds', InputArgument::OPTIONAL, 'Ios RegistrationIds ids'),
        );
    }
	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		//	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}








    /**
     * Execute the console command.
     *
     * @return mixed */
    public function FireTarrifs()
    {
        $usersSubscribed = Tariff::getSubscribedUsers();

        $this->info(count($usersSubscribed));
        foreach($usersSubscribed as $user){
            if(count($user->tariffs) == 0) break;

            $tariff = $user->tariffs[0];
            $paidTill = $tariff->pivot->paidTill;
            $notification = $tariff->pivot->notification;
            $dateEnd = Carbon::createFromFormat('Y-m-d H:i:s', $paidTill);

            foreach([3, 2, 1, 0] as $week){

                $this->info($dateEnd.'    '.Carbon::now()->subWeeks($week));

                if( $dateEnd <= Carbon::now()->subWeeks($week) && $notification == $week+1 ){

                    $notification += 1;
                    $user->tariffs()->updateExistingPivot($tariff->id, ['active' => 'yes', 'notification' => $notification, 'paidTill' => $paidTill], false);
                    $this->info($week.'  notify: '.$user->user_name);
                    if($week == 0){
                        // Конец подписки на тариф
                        mailSend::queue('end', ['user_name' => $user->user_name, 'user_email' => $user->email, 'tariffName' => $tariff->name ]);
                    }
                    else{
                        // $week недель прошло с конца подписки
                        mailSend::queue('warning', ['user_name' => $user->user_name, 'user_email' => $user->email,
                            'tariffName' => $tariff->name, 'week' => $week, 'weeklost' => 4-$week ]);
                    }
                }
            }

            $this->info($dateEnd.'    '.Carbon::now()->addWeeks(1));
            if( $dateEnd <= Carbon::now()->addWeeks(1) && $notification == 0 ){

                $this->info( '-1 notify: '.$user->user_name );
                $notification += 1;
                $user->tariffs()->updateExistingPivot($tariff->id, ['active' => 'yes', 'notification' => $notification, 'paidTill' => $paidTill], false);

                // Неделя до конца подписки
                mailSend::queue('oneweekend', ['user_name' => $user->user_name, 'user_email' => $user->email, 'tariffName' => $tariff->name ]);
            }

        }

    }
}
