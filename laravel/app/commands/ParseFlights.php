<?php

use Illuminate\Console\Command;

class ParseFlights extends Command {

    public function __construct() {
        parent::__construct();
    }

    protected $name = 'command:parse';

    public function fire() {
        $result = $this->collectSubscribedFlights();
        $pushFlightsIds = array_keys($result['flights']);
        $notifyFlights = [];
        //die(json_encode($subscribedFlights)."\n");
        // ---- start ------------
        $start1 = time();
        $this->info('Last run: '.date('Y-m-d H:i:s',$start1));
        // обновление файла
        $url = Config::get('flights.url');
        $credentials = Config::get('flights.credentials');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_USERPWD, "$credentials[username]:$credentials[password]");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $data = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        $destination = "../xml/Vnukovo.ru_FIP.xml";
        $file = fopen($destination, "w+");
        fputs($file, $data);
        fclose($file);


        Cache::forget('minTime');
        Cache::forget('maxTime');
        // работа с файлом
        $start2 = time();
        $reader = new XMLReader();
        $reader->open($destination);
        $rows = [];
        $total = 0;

        $timeZoneDiff = (new DateTimeZone('Europe/Moscow'))->getOffset(new DateTime());
        $this->info('Timezone diff in hours: '.($timeZoneDiff/60/60));

        $airlines = array_column(Airlines::all(['airline_vnt'])->toArray(),'airline_vnt');
        $cities = array_column(Cities::all(['city_en'])->toArray(),'city_en');

        $isDataCollectionMode = true;

        $from = time() - 2 * 3600 + $timeZoneDiff;
        $till = time() + 24 * 3600 + $timeZoneDiff;

        $existingColumns = Schema::getColumnListing('flights');

        while ($reader->read()) {
            switch ($reader->nodeType) {
                case (XMLReader::ELEMENT):
                    if ($reader->localName == 'ROW') {
                        $row = Flight::$flightFields;
                        while ($reader->read()){
                            if ($reader->nodeType == XMLReader::ELEMENT) {
                                $name = strtolower($reader->localName);
                                while ($reader->moveToNextAttribute()){
                                    $row[$name]['__attribs'][$reader->localName] = $reader->value;
                                }
                                $reader->read();
                                if (isset($row[$name]) && is_array($row[$name])){
                                    $row[$name]['value'] = $reader->value;
                                } else
                                    $row[$name] = $reader->value;
                            }
                            if ($reader->nodeType == XMLReader::END_ELEMENT && $reader->localName == 'ROW') {
                                break;
                            }
                        }
                        $st = strtotime($row['st']);
                        if (($st > $from) && ($st < $till)) {
                            $row['st'] = date('Y-m-d H:i:s', $st);
                            $row['et_for_pass'] = date('Y-m-d H:i:s', strtotime($row['et_for_pass']));

                            $ru = explode('(',$row['fls_full_name_rus']);

                            // выравниваем красиво буквы в городах на русском
                            $row['city_ru'] = $this->_ucfirst(mb_strtolower($ru[0]));
                            if (($p = mb_strrpos($row['city_ru'],' ')) || ($p = mb_strrpos($row['city_ru'],'-'))) {
                                $p++;
                                $row['city_ru'] = mb_substr($row['city_ru'],0,$p).$this->_ucfirst(mb_substr($row['city_ru'],$p));
                            }

                            if (isset($ru[1])) $row['airport_ru'] = rtrim($ru[1],')');

                            // -------- ENGLISH CITIES AND AIRPORTS -----------

                            // splitting into cities and airports (works almost always except Kogalym International and Adler/Sochi
                            $en = explode('(',$row['fls_full_name_eng']);

                            // for such cases as ROSTOV, ANTALYA etc
                            $row['city_en'] = ctype_upper($en[0])?ucfirst(strtolower($en[0])):$en[0];

                            // specially for Kogalym
                            $row['city_en'] = str_replace(' International','',$row['city_en']);

                            // specially for Adler/Sochi
                            $adlerSochi = explode('/',$row['city_en']);
                            if (isset($adlerSochi[1])) $row['city_en'] = $adlerSochi[1];

                            if (isset($en[1])) $row['airport_en'] = rtrim($en[1],')');

                            $row['ac_type_name_rus'] = str_replace(' (в)','',$row['ac_type_name_rus']);
                            $row['ac_type_name_eng'] = str_replace(
                                ['Боинг','Аэробус','АН-','АТР ','Бомбардье',' ЦЛ-',' ДХЦ','ТУ-','СУ-','ЯК-42Д','ЯК-','ЦРЙ','200ЕР'],
                                ['Boeing','Airbus','AN-','ATR ','Bombardier',' CL-',' DHC','TU-','SU-','Yak-42D','Yak-','CRJ','200ER'],
                                $row['ac_type_name_rus']
                            );

                            // - отключить на боевом - нужно для сбора городов и авиакомпаний ---
                            if ($isDataCollectionMode) {

                                if (!in_array($row['airline_vnt'], $airlines)) {
                                    $data = array_intersect_key($row, array_flip(['airline_vnt', 'airline_name_rus']));
                                    DB::table('airlines')->insert($data);
                                    array_push($airlines,$row['airline_vnt']);
                                }

                                if (!in_array($row['city_en'], $cities)) {
                                    $data = array_intersect_key($row, array_flip(['city_en']));
                                    $data['image_ru'] = 'images/cities/initial/'.$row['city_en'].'.jpg';
                                    DB::table('cities')->insert($data);
                                    array_push($cities,$row['city_en']);
                                }
                            }

                            if (in_array($row['flt_id'],$pushFlightsIds)) {
                                if ($row['vnukovo_ru_status'] != $result['flights'][$row['flt_id']]['vnukovo_ru_status']) {
                                    $result['flights'][$row['flt_id']]['vnukovo_ru_status'] = $row['vnukovo_ru_status'];
                                    $notifyFlights[] = $result['flights'][$row['flt_id']];
                                }
                            }

                            $rows[] = array_intersect_key($row, array_flip($existingColumns));
                        }
                        $total++;
                    }
            }
        }

        if (count($rows)) {
        //    DB::table('flights')->truncate();
            DB::table('flights')->delete();
            $chunks = intval(count($rows) / 100) + 1;
            for ($i = 0; $i < $chunks; $i++) {
                $chunk = array_slice($rows, $i * 100, 100);
                DB::table('flights')->insert($chunk);
            }
            $this->info(count($rows)." of $total rows inserted");
            $this->info('Time spent on file download: '.($start2-$start1).' ms');
            $this->info('Time spent on file parsing:  '.(time()-$start2).' ms');
            Cache::forever('lastUpdateTime',time());
            Cache::forever('lastUpdateCount',count($rows));
        } else {
            $this->error('... something went wrong. No rows');
        }

        $androidRegistrationIds = $iosRegistrationIds = [];
        foreach ($result['subscriptions'] as $subs) {
            if (isset($result['flights'][$subs['flt_id']])) {
                if (!isset($androidRegistrationIds[$subs['flt_id']])) $androidRegistrationIds[$subs['flt_id']] = [];
                if (!isset($iosRegistrationIds[$subs['flt_id']])) $iosRegistrationIds[$subs['flt_id']] = [];

                $androidRegistrationIds[$subs['flt_id']] = array_merge($androidRegistrationIds[$subs['flt_id']], array_column(UserDevice::select(['registration_id'])->whereIn('device_type',['Android','android'])->where('user_id',$subs['user_id'])->get()->toArray(),'registration_id'));
                $iosRegistrationIds[$subs['flt_id']] = array_merge($iosRegistrationIds[$subs['flt_id']], array_column(UserDevice::select(['registration_id'])->where('device_type','ios')->where('user_id',$subs['user_id'])->get()->toArray(),'registration_id'));
            } else {
            //    $unsubscribe arrays
            }
        }
        echo '$notifyFlights count = '.count($notifyFlights)."\n";
        echo '$androidRegistrationIds count = '.count($androidRegistrationIds)."\n";
        echo '$iosRegistrationIds count = '.count($iosRegistrationIds)."\n";

        Artisan::call('command:notify',[
            'flights' => $notifyFlights,
            'androidRegistrationIds' => $androidRegistrationIds,
            'iosRegistrationIds' => $iosRegistrationIds,

            //    'unsubscribeRegistrationIds' => []
        ]);
    }

    private function _ucfirst($str) {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }

    function collectSubscribedFlights() {
    //    $subscriptions = Subscription::distinct()->select('flt_id')->groupBy('flt_id')->get()->toArray();
        $subscriptions = Subscription::select(['user_id','flt_id'])->get()->toArray();
        $ids = array_column($subscriptions, 'flt_id');
    //    die('$subscriptions = '.json_encode($subscriptions)."\n");
    //    die('$ids = '.json_encode($ids)."\n");
        $flights = Flight::with('cityRel')->whereIn('flt_id',$ids)->get(['flt_id','vnukovo_ru_status','city_ru'])->keyBy('flt_id')->toArray();

        // cleaning obsolete subscriptions
        $toRemoveSubscribersByFlightsIds = array_diff($ids,array_keys($flights));
        Subscription::whereIn('flt_id',$toRemoveSubscribersByFlightsIds)->delete();

     //   Subscription::
    //    UserDevice::
        return [
            'subscriptions' => $subscriptions,
            'flights' => $flights
        ];
    }



}