<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TestPush extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'command:test';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Test push notifications';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Test\n");
        $name = $this->argument('name');

        $this->$name();
        //   $this->curl();
         //  $this->ios();
    }

    public function android() {
        $message = PushNotification::Message('Hey',[
            'flt_id' => 2890893,
            'ticker_text' => 'Рейс задерживается на 20 минут',
            'title_text' => 'Android',
            'notification_text' => 'Рейс задерживается на 20 минут',
            'image' => '/images/cities/initial/London.jpg',
            //    'vibrate'	=> 1,
            //    'sound'		=> 1,
            //    'largeIcon'	=> 'large_icon',
            //    'smallIcon'	=> 'small_icon',
        ]);
        $this->push('androidProd',$message,
            ['APA91bFFhiI7S4JrFChmQ_OcwHMSuAOKStIRDwO7k2VJ9molSO1wXUEVcXY95qFYKnxghCyvSqZnRzk2fDoOWlKynKqBVSh3Nbq1l9mqLpO0cS3uQlnBuhDZlSZiBSSJ1KpEdbvGx1Et']
        );
    }


    /**
     * This example sends one pushnotification with an alert to Apples production push servers
     */
    public function ios()
    {
/*
        $message = PushNotification::Message('Test de la Egor',array(
            'badge' => 1,
            'sound' => 'default',

            'launchImage' => 'image.jpg',

            'custom' =>[
                'flt_id' => 2897476,
                'ticker_text'=> 'Рейс задерживается на 20 минут',
                'title_text'		=> 'Санкт-Петербург',
                'notification_text'	=> 'Рейс задерживается на 20 минут',
                'image' => '/images/cities/initial/London.jpg',
                //    'vibrate'	=> 1,
                //    'sound'		=> 1,
                //    'largeIcon'	=> 'large_icon',
                //    'smallIcon'	=> 'small_icon',
            ]
        ));
*/

        $message = PushNotification::Message('Test de la Egor',array(
             'custom' =>[
                'flt_id' => 2897476,
                'ticker_text'=> 'Рейс задерживается на 20 минут',
                'title_text'		=> 'Санкт-Петербург',
                'notification_text'	=> 'Рейс задерживается на 20 минут',
                'image' => '/images/cities/initial/London.jpg',
                //    'vibrate'	=> 1,
                //    'sound'		=> 1,
                //    'largeIcon'	=> 'large_icon',
                //    'smallIcon'	=> 'small_icon',
            ]
       ));
        $this->push('iosProd',$message,
            ['b296770c450f961a613373f7bf6a5dea614de893beb6be6581ba0df7c4438210']
        );

    }


    public function curl() {
        $key = 'AIzaSyCEhf1zYwEmkRifh-87v17RC5OtoKHqfvg'; // vnukovoapp@gmail.com
        //     $key = 'AIzaSyDGL9FVZlNTsNtk4J7r2-utLEyzTu7UIII'; // egoryny4@gmail.com
        define( 'API_ACCESS_KEY', $key );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// prep the bundle
        $msg = [
            'flt_id' => 2890120,
            'ticker_text'=> 'Рейс задерживается на 20 минут',
            'title_text'		=> 'CURL',
            'notification_text'	=> 'Рейс задерживается на 20 минут',
            'image' => '/images/cities/initial/London.jpg',
            //    'vibrate'	=> 1,
            //    'sound'		=> 1,
            //    'largeIcon'	=> 'large_icon',
            //    'smallIcon'	=> 'small_icon',
        ];

        $fields = [
            'registration_ids' => $this->regIds,
            'data' => $msg
        ];

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        echo $result . "\n";

    }



    public function androidOld($flights, $androidRegistrationIds) {
        echo '$flights count = '.count($flights)."\n";
        echo '$androidRegistrationIds count = '.count($androidRegistrationIds)."\n";

        //    $userDevices = Subscription::with('userDevices')->whereIn('flight_id',$updatedFlightIds)->get()->toArray();
        //    die(json_encode($userDevices)."\n");
// API access key from Google API's Console
        $key = 'AIzaSyCEhf1zYwEmkRifh-87v17RC5OtoKHqfvg'; // vnukovoapp@gmail.com
        //     $key = 'AIzaSyDGL9FVZlNTsNtk4J7r2-utLEyzTu7UIII'; // egoryny4@gmail.com
        define( 'API_ACCESS_KEY', $key );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// prep the bundle
        $msg = [
            'flt_id' => 2880182,
            'ticker_text'=> 'Рейс задерживается на 20 минут',
            'title_text'		=> 'Санкт-Петербург',
            'notification_text'	=> 'Рейс задерживается на 20 минут',
            'image' => '/images/cities/initial/London.jpg',
            //    'vibrate'	=> 1,
            //    'sound'		=> 1,
            //    'largeIcon'	=> 'large_icon',
            //    'smallIcon'	=> 'small_icon',
        ];

        foreach ($flights as $flight) {
            echo "Flight id: $flight[flt_id]\n";
            $this->info('Flight id: ' . $flight['flt_id']);
            $this->info($a = ('$androidRegistration ids: ' . json_encode($androidRegistrationIds)));
            echo $a;
            echo json_encode($flight);
            $msg = [
                'flt_id' => $flight['flt_id'],
                'ticker_text' => $flight['vnukovo_ru_status'],
                'title_text' => $flight['city_ru'],
                'notification_text' => $flight['vnukovo_ru_status'],
                'image' => isset($flight['city_rel']) ? $flight['city_rel']['image_ru'] : '/images/cities/initial/Tbilisi.jpg',
            ];

            $fields = array
            (
                'registration_ids' => $androidRegistrationIds[$flight['flt_id']],
                'data' => $msg
            );

            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);

            echo $result . "\n";
        }
    }


    protected function getArguments()
    {
        return array(
            array('name', InputArgument::REQUIRED, 'Name'),
        );
    }




    public function push($config, $message, $registrationIds) {
        $devices = [];
        foreach ($registrationIds as $regId) {
            $devices[] = PushNotification::Device($regId);
        }

        $devices = PushNotification::DeviceCollection($devices);

        $collection = PushNotification::app($config)
            ->to($devices)
            ->send($message);

        foreach ($collection->pushManager as $push) {
            $response = $push->getAdapter()->getResponse();
            echo "$config ".json_encode($response)."\n";
        }
    }
}
