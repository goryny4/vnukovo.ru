<?php

// AngularJS routes
Route::get('/', ['before' => 'auth', 'uses' => 'HomeController@index']);


// External API routes v1

Route::group(
	['prefix' => 'api/v1'], function () {
        Route::resource('/news','NewsController');

        Route::resource('/maps','MapsController');
        Route::resource('/cities','CitiesController');
        Route::resource('/airlines','AirlinesController');

        Route::post('/user/register', 'UsersController@apiRegisterWithEmail'); // only with email
        Route::post('/user/anonymous', 'UsersController@apiAnonymous');
        Route::post('/user/login', 'UsersController@apiLogin');
        Route::post('/user/logout', 'UsersController@apiLogout');
        Route::get('/user/confirm', 'UsersController@apiConfirm');
        Route::post('/user/forgot', 'UsersController@apiForgot');

        Route::get('/{commercial}','CommercialsController@index')->where('commercial', '(duty-free|cafes-and-restaurants|car-rent)');
        Route::get('/{commercial}/{id}','CommercialsController@show')->where('commercial', '(duty-free|cafes-and-restaurants|car-rent)');

        Route::resource('/flights', 'FlightsController');
        Route::get('/fields', 'FlightsController@fields');
        Route::post('/fields', 'FlightsController@setFieldDescription');

        Route::get('/cms-sections', 'CmsController@sections');
        Route::get('/cms-pages', 'CmsController@pages');


        Route::get('/ads-full', 'AdsController@getApiFullAd');
        Route::get('/ads-block', 'AdsController@getApiBlockAd');

    //    Route::post('/ads/counter', 'AdsController@postApiAdsCounter' );
        Route::post('/ads/click','AdsController@postApiClick');

        Route::resource('/subscriptions', 'SubscriptionsController');

/*
        Route::any('/{abc}',function(){
            throw new NotFoundHttpException;
        });*/
}
);

// AngularJS request routes

Route::group(
	['prefix' => 'ang', 'before' => 'auth'], function ($route) {

    // News, access granted to content managers and superadmins
    Route::group(['before' => 'content'], function() {
        Route::get('/news','NewsController@getAngNewsList');
        Route::get('/news/{id}', 'NewsController@getEditAngNews');
        Route::post('/news/0', 'NewsController@postCreateAngNews');
        Route::put('/news/{id}', 'NewsController@putEditAngNews');
        Route::delete('/news', 'NewsController@destroy');

        Route::get('/how-to-get', 'CmsController@getAngHowToGetCmsPagesList');
        Route::get('/how-to-get/{id}', 'CmsController@getEditAngCms');
        Route::post('/how-to-get/0', 'CmsController@postCreateAngCms');
        Route::put('/how-to-get/{id}', 'CmsController@putEditAngCms');
        Route::delete('/how-to-get', 'CmsController@destroy');

        Route::get('/services', 'CmsController@getAngServicesCmsPagesList');
        Route::get('/services/{id}', 'CmsController@getEditAngCms');
        Route::post('/services/0', 'CmsController@postCreateAngCms');
        Route::put('/services/{id}', 'CmsController@putEditAngCms');
        Route::delete('/services', 'CmsController@destroy');
        
        Route::get('/commercials','CommercialsController@getAngCommercialObjectList');
        Route::get('/commercials/{id}', 'CommercialsController@getEditAngCommercialObject');
        Route::post('/commercials/0', 'CommercialsController@postCreateAngCommercialObject');
        Route::put('/commercials/{id}', 'CommercialsController@putEditAngCommercialObject');
        Route::delete('/commercials', 'CommercialsController@destroy');

        Route::get('/special-proposals','SpecialProposalsController@getAngSpecialProposalList');
        Route::get('/special-proposals/{id}', 'SpecialProposalsController@getEditAngSpecialProposal');
        Route::post('/special-proposals/0', 'SpecialProposalsController@postCreateAngSpecialProposal');
        Route::put('/special-proposals/{id}', 'SpecialProposalsController@putEditAngSpecialProposal');
        Route::delete('/special-proposals', 'SpecialProposalsController@destroy');

        Route::get('/ads','AdsController@getAngAdsList');
        Route::get('/ads/{id}', 'AdsController@getEditAngAd');
        Route::post('/ads/0', 'AdsController@postCreateAngAd');
        Route::put('/ads/{id}', 'AdsController@putEditAngAd');
        Route::delete('/ads', 'AdsController@destroy');

        Route::post('/images/{entity}/{id}/{lang}', 'ImagesController@postImage');
    });
    
    //Users, access granted to analysts and superadmins
    Route::group(['before' => 'analyst'], function() {
        Route::get('/users','UsersController@getAngUsersList');
        Route::get('/users/{id}', 'UsersController@getEditAngUser');
        Route::post('/users/0', 'UsersController@postCreateAngUser');
        Route::put('/users/{id}', 'UsersController@putEditAngUser');
        Route::delete('/users', 'UsersController@destroy');
        Route::get('/ads-list', 'StatisticsController@getAngAdsList');
    });
    
    //Moders, access granted to admins and superadmins
    Route::group(['before' => 'admin'], function() {
        Route::get('/moders','ModersController@getAngModersList');
        Route::get('/moders/{id}', 'ModersController@getEditAngModer');
        Route::post('/moders/0', 'ModersController@postCreateAngModer');
        Route::put('/moders/{id}', 'ModersController@putEditAngModer');
        Route::delete('/moders', 'ModersController@destroy');

        Route::get('/maps','MapsController@getAngMapsList');
        Route::get('/maps/{id}', 'MapsController@getEditAngMaps');
        Route::post('/maps/0', 'MapsController@postCreateAngMaps');
        Route::put('/maps/{id}', 'MapsController@putEditAngMaps');
        Route::delete('/maps', 'MapsController@destroy');

        Route::get('/airlines','AirlinesController@getAngAirlinesList');
        Route::get('/airlines/{id}', 'AirlinesController@getEditAngAirlines');
        Route::post('/airlines/0', 'AirlinesController@postCreateAngAirlines');
        Route::put('/airlines/{id}', 'AirlinesController@putEditAngAirlines');
        Route::delete('/airlines', 'AirlinesController@destroy');

        Route::get('/cities','CitiesController@getAngCitiesList');
        Route::get('/cities/{id}', 'CitiesController@getEditAngCities');
        Route::post('/cities/0', 'CitiesController@postCreateAngCities');
        Route::put('/cities/{id}', 'CitiesController@putEditAngCities');
        Route::delete('/cities', 'CitiesController@destroy');

        Route::get('/logs','LogsController@getAngLogsList');
    });


});

// Service routes
Route::group(
    ['prefix' => 'auth', 'before' => 'guest'], function () {

    Route::get('/login', 'AuthController@getLogin');
    Route::post('/login', 'AuthController@postLogin');

});


Route::group(['prefix' => 'auth', 'before' => 'auth'], function () {
    Route::get('/logout', 'AuthController@logout');
});

Route::group(['before' => 'auth'], function () {
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});




