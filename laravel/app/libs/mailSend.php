<?php
class mailSend {

    public static function getTemplateByCode( $code, $data){
        $template = Template::where('code', '=', $code)->first();
        $message = $template->message;
        foreach($data as $key => $value){
            $message = str_replace('{'.$key.'}', $value, $message);
        }
        $template->message = $message;
        return $template;
    }

    public static function queue($code, $data){
        $template = mailSend::getTemplateByCode($code, $data);
        Mail::queue('emails.index', ['template' => $template->message], function($message) use ($template, $data){
            $message->to($data['user_email'], $data['user_name'])->subject($template->subject);
        });
    }
}