<?php
class CmsSectionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('cms_sections')->truncate();

        $sectionsTree = [
            'how-to-get' => [
                'car' => ['Автомобиль','Car'],
                'aeroexpress' => ['Аэроэкспресс','Aeroexpress'],
                'bus' => ['Автобус','Bus'],
                'minibus' => ['Маршрутное такси','Minibus'],
                'order-taxi'=> ['Заказать такси','Get taxi'],
            ],
            'services' =>[
                'vip' => ['VIP-услуги, первый и бизнес-класс','VIP Services, first and business class'],
            ]
        ];

        foreach ($sectionsTree as $root => $sections) {
            foreach ($sections as $section => $titles) {
                CmsSections::insert([
                    'root' => $root,
                    'section' => $section,
                    'title_ru' => $titles[0],
                    'title_en' => $titles[1]
                ]);
            }
        }


    }

}