<?php
class LocationsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('locations')->delete();
        $locations = ['news' => 'News', 'duty-free' => 'CommercialObject', 'cafes-and-restaurants' => 'CommercialObject', 'car-rent' => 'CommercialObject'];
        $i = 0;
        $data = ['name' => 'fullscreen-ads', 'id' => $i]; // first element
        Location::create($data);

        foreach ($locations as $locationName => $entity) {
            $data = ['name' => $locationName, 'entity_name' => $entity, 'id' => ++$i];
            Location::create($data);
        }


    }

}