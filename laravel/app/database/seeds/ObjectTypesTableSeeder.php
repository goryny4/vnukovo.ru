<?php
class ObjectTypesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('object_types')->delete();
        $types = ['duty-free', 'cafes-and-restaurants', 'car-rent'];

        foreach ($types as $type) {
            $objectType = new ObjectType;
            $objectType->name = $type;
            $objectType->save();
        }
    }

}