<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
    public function run()
    {
        $this->call('ObjectTypesTableSeeder');
        $this->call('LocationsTableSeeder');
        $this->call('CmsSectionsTableSeeder');

        $this->command->info('Object Types seeded!');
    }

}
