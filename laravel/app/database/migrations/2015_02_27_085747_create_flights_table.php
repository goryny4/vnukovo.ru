<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('flights', function(Blueprint $table) {
            $table->integer('flt_id')->unsigned();
            $table->integer('bound')->unsigned();
            $table->string('systimestamp');
            $table->string('flight_number_for_pass');
            $table->string('airline_vnt');
            $table->string('airline_name_rus');
            $table->string('airline_name_eng');
            $table->dateTime('st');
            $table->dateTime('et_for_pass');
            $table->string('at_for_pass');
            $table->string('vnukovo_ru_status');
            $table->string('ac_type_name_rus');
            $table->string('ac_reg');
            $table->string('term');
            $table->string('handling_type_for_pass');
            $table->string('category');
            $table->string('fls_id');
            $table->string('fls_full_name_rus');
            $table->string('fls_full_name_eng');
            $table->string('fls_st');
            $table->string('fls_et');
            $table->string('fls_at_for_pass');
            $table->string('vnukovo_ru_status_short');
            $table->string('vnukovo_en_status');
            $table->string('vnukovo_en_status_short');
            $table->string('codeshare_1');
            $table->string('codeshare_2');
            $table->string('checkin_list_actual_for_pass');
            $table->string('checkin_sst');
            $table->string('checkin_set');
            $table->string('checkin_ast');
            $table->string('checkin_aet');
            $table->string('checkin_list_with_status');
            $table->string('gate_st');
            $table->string('gate_et');
            $table->string('gates_list_for_pass');
            $table->string('delay_name_pas_rus');
            $table->string('delay_name_pas_eng');
            $table->string('checkin_business');

            $table->string('via1_id');
            $table->string('via1_full_name_rus');
            $table->string('via1_full_name_eng');
            $table->string('via1_sta');
            $table->string('via1_eta');
            $table->string('via1_std');
            $table->string('via1_etd');
            $table->string('via1_ata_for_pass');

            $table->string('via2_id');
            $table->string('via2_full_name_rus');
            $table->string('via2_full_name_eng');
            $table->string('via2_sta');
            $table->string('via2_eta');
            $table->string('via2_std');
            $table->string('via2_etd');
            $table->string('via2_ata_for_pass');

            $table->string('via3_id');
            $table->string('via3_full_name_rus');
            $table->string('via3_full_name_eng');
            $table->string('via3_sta');
            $table->string('via3_eta');
            $table->string('via3_std');
            $table->string('via3_etd');
            $table->string('via3_ata_for_pass');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('flights');
	}

}
