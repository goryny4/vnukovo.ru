<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('logs', function(Blueprint  $table) {
            $table->increments('id')->unsigned();
            $table->enum('operation',['create', 'delete', 'update']);
            $table->integer('moder_id');
            $table->string('moder_name');
            $table->string('request_ip');
            $table->enum('moder_group',['content', 'analyst', 'admin', 'superadmin']);
            $table->string('category');
            $table->integer('entity_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('logs');
	}

}
