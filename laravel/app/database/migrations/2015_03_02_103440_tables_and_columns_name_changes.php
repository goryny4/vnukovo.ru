<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablesAndColumnsNameChanges extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->dropForeign('commercial_objects_object_type_id_foreign');
        });
        DB::table('commercial_objects')->delete();
		Schema::table('commercial_objects', function(Blueprint $table) {
            $table->dropColumn('object_type_id');
        });
        Schema::rename('object_types', 'commercial_object_types');
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->integer('commercial_object_type_id')->unsigned()->after('id');
            $table->foreign('commercial_object_type_id')->references('id')->on('commercial_object_types');
        });
        DB::table('ad')->truncate();
        Schema::rename('ad', 'ads');



	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->dropForeign('commercial_objects_commercial_object_type_id_foreign');
        });

        DB::table('commercial_objects')->delete();
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->dropColumn('commercial_object_type_id');
        });
        Schema::rename('commercial_object_types', 'object_types');
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->integer('object_type_id')->unsigned()->after('id');
            $table->foreign('object_type_id')->references('id')->on('object_types');
        });
        Schema::rename('ads', 'ad');
    }

}
