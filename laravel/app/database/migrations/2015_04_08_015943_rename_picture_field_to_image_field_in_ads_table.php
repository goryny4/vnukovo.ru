<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamePictureFieldToImageFieldInAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('ads', function(Blueprint $table) {
            $table->dropColumn('picture_url_ru');
            $table->dropColumn('picture_url_en');

            $table->string('image_ru')->nullable();
            $table->string('image_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ads', function(Blueprint $table) {
            $table->string('picture_url_ru')->nullable();
            $table->string('picture_url_en')->nullable();

            $table->dropColumn('image_ru');
            $table->dropColumn('image_en');
        });
    }
}
