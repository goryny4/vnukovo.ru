<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('ad', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('location_id')->unsigned();
            $table->foreign('location_id')->references('id')->on('locations');
            $table->enum('type', ['block', 'full']);
            $table->string('image_ru');
            $table->string('image_en');
            $table->string('target_url_ru')->nullable();
            $table->string('target_url_en')->nullable();
            $table->integer('times_shown');
            $table->integer('times_opened');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ad');
    }

}
