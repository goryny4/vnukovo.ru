<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageFieldToNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('news', function(Blueprint $table) {
            $table->string('image_ru')->nullable();
            $table->string('image_en')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('news', function(Blueprint $table) {
            $table->dropColumn('image_ru');
            $table->dropColumn('image_en');
        });
	}

}
