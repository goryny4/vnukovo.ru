<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNameAndSoftDeletionToAdsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('ads', function(Blueprint $table) {
            $table->dropColumn('type');
        });

		Schema::table('ads', function(Blueprint $table) {
            $table->string('name')->after('id');
            $table->softDeletes();
            $table->enum('type',['block', 'full', 'link'])->after('name');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('ads', function(Blueprint $table) {
            $table->dropColumn('type');
        });

        Schema::table('ads', function(Blueprint $table) {
            $table->dropColumn('name');
            $table->dropSoftDeletes();
            $table->enum('type',['1', '2', '3'])->after('id');
        });
	}

}
