<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news',function(Blueprint $table) {
			$table->increments('id');
			$table->integer('creator_id');
			$table->string('title_en');
			$table->string('title_ru');
			$table->text('text_en');
			$table->text('text_ru');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news');
	}

}
