<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialObjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('commercial_objects', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('object_type_id')->unsigned();
            $table->foreign('object_type_id')->references('id')->on('object_types');
            $table->string('name_ru');
            $table->string('name_en');
            $table->text('description_ru');
            $table->text('description_en');
            $table->enum('terminal', ['A', 'B', 'D']);
            $table->integer('floor');
            $table->time('time_start');
            $table->time('time_end');
            $table->double('centre_lat');
            $table->double('centre_lon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commercial_objects');
    }

}
