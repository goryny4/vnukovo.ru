<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlinesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airlines', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('airline_vnt');
            $table->string('airline_name_rus');
            $table->string('image_ru')->nullable();
            $table->string('image_en')->nullable();
            $table->integer('version');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('airlines');
    }

}
