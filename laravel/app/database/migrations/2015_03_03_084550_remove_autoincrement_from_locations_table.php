<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAutoincrementFromLocationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('locations')->delete();
        DB::table('ads')->delete();
        Schema::table('ads', function(Blueprint $table) {
            $table->dropForeign('ad_location_id_foreign');
            $table->dropColumn('location_id');
        });
        Schema::drop('locations');
        Schema::create('locations', function(Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->integer('commercial_object_type_id')->unsigned();
            $table->string('name');
            $table->string('entity_name');
        });
        Schema::table('ads', function(Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('type');
            $table->foreign('location_id')->references('id')->on('locations');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('locations')->delete();
        DB::table('ads')->delete();
        Schema::table('ads', function(Blueprint $table) {
            $table->dropForeign('ads_location_id_foreign');
            $table->dropColumn('location_id');
        });
        Schema::drop('locations');
        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('commercial_object_type_id')->unsigned();
            $table->string('name');
            $table->string('entity_name');
        });
        Schema::rename('ads', 'ad');
        Schema::table('ad', function(Blueprint $table) {
            $table->integer('location_id')->unsigned()->after('type');
            $table->foreign('location_id')->references('id')->on('locations');
        });
        Schema::rename('ad', 'ads');
    }

}
