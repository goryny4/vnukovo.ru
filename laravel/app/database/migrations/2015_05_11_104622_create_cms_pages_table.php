<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cms_pages', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('section');
            $table->string('is_primary');
            $table->string('title_ru');
            $table->string('title_en');
            $table->text('bbcode_ru');
            $table->text('bbcode_en');
            $table->text('text_ru');
            $table->text('text_en');
            $table->string('image_ru');
            $table->string('image_en');
            $table->integer('creator_id');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cms_pages');
	}

}
