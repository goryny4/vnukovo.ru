<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteForeignKeyFromLocationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('locations', function(Blueprint $table) {
            $table->dropForeign('locations_object_type_id_foreign');
            $table->dropColumn('object_type_id');
            $table->integer('commercial_object_type_id')->nullable()->after('id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('locations', function(Blueprint $table) {
            $table->dropColumn('commercial_object_type_id');
            $table->integer('object_type_id')->unsigned();
            $table->foreign('object_type_id')->references('id')->on('object_types');
        });

    }

}
