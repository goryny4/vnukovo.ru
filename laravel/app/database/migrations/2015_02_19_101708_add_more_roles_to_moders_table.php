<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreRolesToModersTable extends Migration {

    public function up()
    {
        Schema::table('moders', function(Blueprint $table) {
            $table->dropColumn('group');
        });

        Schema::table('moders', function(Blueprint $table) {
            $table->enum('group',['content', 'analyst', 'admin', 'superadmin'])->after('password');
        });
    }

    public function down()
    {
        Schema::table('moders', function(Blueprint $table) {
            $table->dropColumn('group');
        });

        Schema::table('moders', function(Blueprint $table) {
            $table->enum('group',['content', 'analyst'])->after('password');
        });
    }


}
