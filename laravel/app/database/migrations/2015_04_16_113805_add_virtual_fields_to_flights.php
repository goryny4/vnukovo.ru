<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVirtualFieldsToFlights extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('flights', function(Blueprint $table)
		{
            $table->string('city_ru')->nullable();
            $table->string('city_en')->nullable();
            $table->string('airport_ru')->nullable();
            $table->string('airport_en')->nullable();
            $table->string('ac_type_name_eng')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('flights', function(Blueprint $table)
		{
            $table->dropColumn('city_ru');
            $table->dropColumn('city_en');
            $table->dropColumn('airport_ru');
            $table->dropColumn('airport_en');
            $table->dropColumn('ac_type_name_eng');
        });
	}

}
