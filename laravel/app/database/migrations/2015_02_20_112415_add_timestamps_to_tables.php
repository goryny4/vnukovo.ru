<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampsToTables extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('commercial_objects', function(Blueprint $table) {
          $table->timestamps();
        });
        Schema::table('ad', function(Blueprint $table) {
            $table->timestamps();
        });
        Schema::table('special_proposals', function(Blueprint $table) {
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('commercial_objects', function(Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('ad', function(Blueprint $table) {
            $table->dropTimestamps();
        });
        Schema::table('special_proposals', function(Blueprint $table) {
            $table->dropTimestamps();
        });
    }

}
