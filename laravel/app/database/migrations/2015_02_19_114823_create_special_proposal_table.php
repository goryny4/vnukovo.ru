<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialProposalTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('special_proposals', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('commercial_object_id')->unsigned();
            $table->foreign('commercial_object_id')->references('id')->on('commercial_objects');
            $table->string('picture_url_en');
            $table->string('picture_url_ru');
            $table->string('title_en');
            $table->string('title_ru');
            $table->text('description_en');
            $table->text('description_ru');
            $table->integer('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special_proposals');
    }

}
