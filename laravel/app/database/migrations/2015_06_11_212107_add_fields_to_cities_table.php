<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('cities', function(Blueprint $table)
        {
            $table->integer('weather_id')->nullable();
            $table->text('weather_t')->nullable();
            $table->text('weather_text')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('cities', function(Blueprint $table)
        {
            $table->dropColumn('weather_id');
            $table->dropColumn('weather_t');
            $table->dropColumn('weather_text');
        });
	}

}
