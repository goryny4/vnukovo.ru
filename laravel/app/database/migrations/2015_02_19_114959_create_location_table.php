<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::create('locations', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('object_type_id')->unsigned();
            $table->foreign('object_type_id')->references('id')->on('object_types');
            $table->string('name');
            $table->string('entity_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }

}
