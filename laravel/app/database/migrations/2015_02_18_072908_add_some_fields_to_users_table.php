<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldsToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
            $table->string('token')->after('social_network_id');
            $table->boolean('is_spam_accepted')->after('social_network_id');
            $table->date('birthday')->after('last_name')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('token');
            $table->dropColumn('is_spam_accepted');
            $table->dropColumn('birthday');
        });
	}

}
