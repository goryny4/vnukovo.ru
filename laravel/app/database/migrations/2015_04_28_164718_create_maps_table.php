<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('maps', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('terminal');
            $table->integer('floor');
            $table->integer('version');
            $table->string('image_ru')->nullable();
            $table->string('image_en')->nullable();
            $table->integer('moder_id');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('maps');
	}

}
