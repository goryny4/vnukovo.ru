<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBbcodeFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('news', function(Blueprint $table)
        {
            $table->text('bbcode_ru')->nullable();
            $table->text('bbcode_en')->nullable();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('news', function(Blueprint $table)
        {
            $table->dropColumn('bbcode_ru');
            $table->dropColumn('bbcode_en');
        });
	}

}
