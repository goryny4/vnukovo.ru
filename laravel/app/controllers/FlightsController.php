<?php

class FlightsController extends BaseController {


    /**
     * @api {get} flights List
     * @apiSampleRequest /api/v1/flights?page=:page&bound=:bound&lang=:lang&search=:search&fromTime=:fromTime&toTime=:toTime
     * @apiVersion 1.0.0
     * @apiName GetFlights
     * @apiGroup Flights
     *
     * @apiParam {Number} page
     * @apiParam {Integer} bound={NULL|0|1}
     * @apiParam {String} lang={ru|en}
     * @apiParam {String} search Поиск (ищет по полям flight_number_for_pass и fls_full_name_{rus|eng})
     * @apiParam {Date} fromTime Поиск по времени => со скольки (всегда в паре с toTime)
     * @apiParam {Date} toTime Поиск по времени < до скольки
     *
     * @apiSuccess {String} FLT_ID Id рейса
     * @apiSuccess {String} FLIGHT_NUMBER_FOR_PASS Номер рейса
     * @apiSuccess {String} FLS_FULL_NAME Пункт назначения
     * @apiSuccess {Date} ST Время прилёта/отлёта по расписанию
     * @apiSuccess {Date} ET_FOR_PASS Ожидаемое время прилёта/отлёта
     * @apiSuccess {String} VNUKOVO_STATUS_SHORT Статус рейса
     */

    public function index() {

        $input = Input::all();

        if (isset($input['alive'])) return json_encode(['secondsTillNextUpdate'=>Cache::get('lastUpdateTime')+119-time()]);

        $lang = self::getLang($input);

        $predicate = Flight::lang($lang)->orderBy('st', 'ASC');
//        $predicate = $predicate->whereBetween('st', array(Carbon::now()->subHours(2), Carbon::now()->addDay())); # уже не нужно, в базе только необходимые рейсы
        $timePaginationMode = (!empty($input['fromTime']) && !empty($input['toTime']));

        if (isset($input['bound']))     $predicate = $predicate->where('bound', $input['bound']);
        if (isset($input['search']))    $predicate = $predicate->where(function($query) use ($input) {
            $query->where('flight_number_for_pass', 'LIKE', "%$input[search]%")->orWhere('fls_full_name_rus', 'LIKE', "%$input[search]%")->orWhere('fls_full_name_eng', 'LIKE', "%$input[search]%");
        });
        if ($timePaginationMode) {
            $validation = Validator::make($input,['fromTime' => 'date','toTime' => 'date']);
            if ($validation->fails()) return self::reply('fromTime and toTime should be in datetime format - 15.04.2015 12:30',502);
            $predicate = $predicate->whereBetween('st', array($fromTime = date("Y-m-d H:i:s",strtotime($input['fromTime'])), $toTime = date("Y-m-d H:i:s",strtotime($input['toTime']))));
            $obj = $predicate->get();
        } else {
            if (!$obj = $predicate->paginate(10)) {
                return self::reply('Рейсы не найдены', 404);
            }
        }
    //    $queries    = DB::getQueryLog();


        $flights = $obj->toArray();
        $response = [
            'result' => $timePaginationMode?$flights:$flights['data'],
        ];

        if ($timePaginationMode) {
            $response['paginationData'] = [
                'fromTime' => $fromTime,
                'toTime' => $toTime,
                'minTime'=> Cache::rememberForever('minTime', function () { return Flight::select('st')->min('st');}),
                'maxTime'=> Cache::rememberForever('maxTime', function () { return Flight::select('st')->max('st');}),
                'count' => $obj->count(),
                'total' => Cache::get('lastUpdateCount'),
                'secondsTillNextUpdate'=>Cache::get('lastUpdateTime')+119-time()
            ];
        } else {
            $response['paginationData'] = [
                'currentPage' => $obj->getCurrentPage(),
                'lastPage' => $obj->getLastPage(),
                'perPage' => $obj->getPerPage(),
                'total' => $obj->getTotal(),
                'from' => $obj->getFrom(),
                'to' => $obj->getTo(),
                'count' => $obj->count()
            ];
        }

        return Response::json($response,202);
    }


    /**
     * @api {get} flights/:flt_id Item
     * @apiSampleRequest /api/v1/flights/:flt_id/?lang=:lang&allFields=:allFields
     * @apiVersion 1.0.0
     * @apiName GetFlightById
     * @apiGroup Flights
     *
     * @apiParam {Integer} flt_id Id рейса (например, 2740645)
     * @apiParam {String} lang Язык
     * @apiParam {Boolean} allFields Показать все поля
     *
     * @apiSuccess {String} FLT_ID Id рейса
     * @apiSuccess {String} FLIGHT_NUMBER_FOR_PASS Номер рейса
     * @apiSuccess {String} FLS_FULL_NAME Пункт назначения
     * @apiSuccess {Date} ST Время прилёта/отлёта по расписанию
     * @apiSuccess {Date} ET_FOR_PASS Ожидаемое время прилёта/отлёта
     * @apiSuccess {String} TERM Терминал
     * @apiSuccess {String} VNUKOVO_STATUS_SHORT Статус рейса
     * @apiSuccess {String} AC_TYPE_NAME Воздушное судно (почему-то отсутствует поле с переводом на английский)
     * @apiSuccess {String} AIRLINE_NAME Авиакомпания
     * @apiSuccess {String} GATES_LIST_FOR_PASS Выход(ы)
     */

    public function show($fltId) {
        $lang = self::getLang(Input::all());
        $fields = (Input::get('allFields')?'full':'detailed');
        $flight = Flight::lang($lang,$fields)->where('flt_id', $fltId)->first();
        if (!$flight) {
            return self::reply('Рейс не найден',404);
        }
        $flight->airline_logo = $flight->airline->image_ru;
        $flight->city_photo = $flight->cityRel->image_ru;
        $flight->weather_t = $flight->cityRel->weather_t;
        $flight->weather_text = $flight->cityRel->weather_text;
        unset($flight->airline);
        unset($flight->cityRel);
        return Response::json(['result' => $flight],202);
    }

    /**
     * @api1 {get} fields Посмотреть описания
     * @apiSampleRequest1 /api/v1/fields
     * @apiVersion1 1.0.0
     * @apiName1 GetFields Посмотреть описания
     * @apiGroup1 Fields
     */

    public function fields() {
        $lang = self::getLang(Input::all());
        $fields = 'full';
        $flight = Flight::lang($lang,$fields)->orderByRaw("RAND()")->first();

        $descriptions = DB::table('fields')->lists('description', 'field');
      /*  $descriptions = [
            'st' => 'Время прилёта/отлёта по расписанию',
            'vnukovo_status_short' => 'Статус рейса коротко'
        ];
*/
        if (!$flight) {
            return self::reply('Рейс не найден',404);
        }
        $result = [];
        foreach ($flight->toArray() as $key=>$value) {
            $result[] = [
                'field' => strtoupper($key),
                'value' => $value,
                'description' => @$descriptions[$key]
            ];
        /*    if (!DB::table('fields')->where('field',$key)->count()) {
                DB::table('fields')->insert(['field'=>$key]);
            }
*/
        }

        return Response::json($result,202);
    }

    /**
     * @api {post} fields Добавить описание
     * @apiDescription Добавить описание поля Flights (для внутреннего использования)
     * @apiSampleRequest /api/v1/fields
     * @apiVersion 1.0.0
     * @apiName setField Добавить описание
     * @apiGroup Fields
     * @apiParam {String} field Поле
     * @apiParam {String} description Описание
     */

    public function setFieldDescription() {
        $field = strtolower(Input::get('field'));
        $description = Input::get('description');
        if (!DB::table('fields')->where('field',$field)->count()) return self::reply('No such field!',404);

        DB::table('fields')->where('field',$field)->update(['description'=>$description]);

        return Response::json(['Saved.'],202);
    }

}