<?php

class BaseController extends Controller {

    /**
     * Заголовок страницы для админ-части
     * @var string
     */
    public $pageTitle;

    public function __construct() {
        Log::info(Request::getClientIp().' '.Request::getMethod().' '.Request::url(),[http_build_query(Request::except('password'))]);
    }

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout,[]);
		}
	}

	protected function convertFieldsToEmptyResponse($fields) {
		$result = new stdClass();
		foreach ($fields as $field) {
			$result->$field = '';
		}
		return $result;
	}

    protected function getLang($lang) {
        if (is_array($lang)) $lang = ((!empty($lang['lang']) && $lang['lang']) ? $lang['lang'] : 'ru');
        $lang = ($lang ? : 'ru');
        $validation = Validator::make(
            ['lang' => $lang],
            ['lang' => 'in:ru,en']
        );

        if ($validation->fails()) throw new UserException('Wrong language');

        return $lang;
    }

    protected function auth($token) {

        $user = User::where('token', $token)->first();
        // 3 days login
        if ( $user && !empty($token) && (time() - strtotime($user->updated_at)) < 3 * 24 *3600 ) {
            return [
                'status' => 1,
                'user' => $user,
            ];

        } else {
            if ($user) {
                $user->token = '';
                $user->save([], true);
            }
            return [
                'status' => 0
            ];
        }

    }

    function reply($messages = [], $code = 200, $data = [], $validationMessages = []) {
        if ($data){
            if (is_array($data)) {
                $result = $data;
            } else {
                $result['content'] = $data;
            }
        }
        $result['status'] = $code<400?'success':'error';
        if ($messages) {
            if (!is_array($messages)) {
                $messages = [$messages];
            }
            $result['messages'] = $messages;
        }
        if (count($validationMessages)) {
            if (!isset($result['messages']) || !count($result['messages'])) $result['messages'] = ['Request data validation failed'];
            $result['validationMessages'] = $validationMessages;
        }
        Log::warning(Request::getClientIp().' '.Request::getMethod().' '.Request::url(),$result);

        return Response::json($result,200);//$code); // I always return 200!!!
    }

}
