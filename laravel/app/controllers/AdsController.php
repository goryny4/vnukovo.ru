<?php

class AdsController extends BaseController {

    /// ---------------- External API
    /**
     * @api {get} ads-full Заставка
     * @apiDescription Получить рекламную заставку (пока что random'ом)
     * @apiSampleRequest /api/v1/ads-full?lang=:lang
     * @apiVersion 1.0.0
     * @apiName AdsFull
     * @apiGroup Ads
     *
     * @apiParam {String} lang Язык
     * @apiParam {String} token Token пользователя (optional)
     *
     * @apiSuccess {Integer} id Id рекламной заставки
     * @apiSuccess {Integer} image Картинка
     * @apiSuccess {String} target_url Рекламная ссылка на внешний ресурс
     *
     */


    public function getApiFullAd() {
        $input = Input::all();

        $lang = self::getLang($input);

        $token = isset($input['token']) ? $input['token'] : false;
        $ad = Ads::orderBy(DB::raw('RAND()'))->lang($lang)->where('type','full')->first();

        if (!$ad) {
            return self::reply('No such ad', 404);
        }

        $result =[
            'result' => $ad,
        ];
   //     $token = '0H6EJrKmRJEtrRw2XNRYvaRwNDhbCs0w';

        $ad->increment('times_shown');
        if ($token) {
            $auth = $this->auth($token);
            if ($auth['status']) {
                $ad = ['ad_id'=>$ad->id];
                if (!$auth['user']->adsViews()->where($ad)->first()) {
                    $adView = new adsView($ad);
                    $auth['user']->adsViews()->save($adView);
                }
           } else {
                $result['messages'] = ['This token is not valid anymore.'];
            }
        }

        return Response::json($result,202);
    }

    /**
     * @api {post} ads/click Счётчик кликов
     * @apiDescription Увеличить счётчик кликов на рекламную заставку или блок
     * @apiSampleRequest /api/v1/ads/click
     * @apiVersion 1.0.0
     * @apiName AdsClick
     * @apiGroup Ads
     *
     * @apiParam {Integer} id Id
     * @apiParam {String} link Link
     */

    public function postApiClick() {
        $input = Input::all();

        $mode = (isset($input['link'])?'link':'id');

        switch ($mode) {
            case 'link':
                $ad = Ads::where('name',$input['link'])->first();
                if (!$ad) {
                    return self::reply('No such link', 404);
                }
                break;
            case 'id':
                $id = (int)$input['id'];
                $ad = Ads::where('id',$id)->first();
                if (!$ad) {
                    return self::reply('No such ad', 404);
                }
                break;
        }

    //    $lang = self::getLang($input);
    //    $token = isset($input['token']) ? $input['token'] : false;


        $ad->increment('times_opened');
        return self::reply('Link visit counter incremented');
    }
/*
    public function getApiBlockAdDeprecated() {

        $input = Input::all();

        $lang = self::getLang($input);
        $location = isset($input['location']) ? $input['location'] : null;
        $token = isset($input['token']) ? $input['token'] : false;

        $validation = Validator::make(
            ['location' => $location],
            ['location' => 'required|in:news,duty-free,cafes-and-restaurants,car-rent']
        );

        if ($validation->fails()) {
            return json_encode([
                'status' => 'error',
                'messages' => $validation->messages(),
                'code' => 406
            ]);
        }

        $locationId = Location::where('name', $location)->first()->id;

        $ad = Ads::orderBy(DB::raw('RAND()'))->lang($lang)->where('type','block')->where('location_id', $locationId)->first();

        if ($ad) {
            $id = $ad->id;

            $item = Ads::find($id);
            $item->times_shown++;
            $item->save([], true);

            if ($token) {
                $auth = $this->auth($token);
                if ($auth['status']) {
                    $view = AdsView::where('user_id', $auth['user']->id)->where('ad_id', $id)->first();
                    if (!$view) {
                        $view = new AdsView;
                        $view->user_id = $auth['user']->id;
                        $view->ad_id = $id;
                        $view->save([], true);
                    }
                } else {
                    //здесь и нужно будет расписать действия, если запрос приходит от незалогиненого пользователя
                }
            }

            return json_encode([
                'status' => 'success',
                'result' => $ad,
                'code' => 202,
            ]);
        }

        return json_encode([
            'status' => 'error',
            'messages' => ['Объявлений нет'],
            'code' => 404
        ]);

    }
*/




    // ------------------------ Angular ----------------------------------------------

    public function getAngAdsList() {

        return Ads::orderBy('updated_at', 'DESC')->where('type',Input::get('type'))->paginate(10);

    }

    public function getEditAngAd($id) {

        if ($id == 0) { // for Angular
            return json_encode(Ads::$outputable);

        } else {
            $ad =Ads::select(Ads::$list)->find($id);
            if (!$ad) {
                return null;
            }
        }

        return $ad;

    }

    public function postCreateAngAd() {
        return Ads::create(array_merge(
            Input::all(),
            ['type' => $_GET['type']]
        ));
    }


    public function putEditAngAd($id) {
        return (($obj = Ads::find($id))?$obj->update(Input::all()):0);
    }

    public function destroy() {
        return (!empty($_GET['ids'])?Ads::destroy($_GET['ids']):0);
    }





}