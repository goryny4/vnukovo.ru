<?php

class ImagesController extends BaseController {

    function postImage($entity,$id,$lang) {
        if (!Auth::check())  die('no way! authorize');

        $lang = array_slice(explode('_',$lang),-1,1)[0];
        $lang = self::getLang($lang);
        $userId = Auth::id();
        $tmpDir = storage_path("images/tmp/$userId/$entity/$id/$lang/");

        File::makeDirectory($tmpDir, $mode = 0777, true, true); // создаём папки
        File::deleteDirectory($tmpDir,true); // да, именно в такой последовательности :) (удаляли содержимое папки)
        Input::file('file')->move($tmpDir, Input::file('file')->getClientOriginalName());

        // resize
        $sizes = ['news'=>725,'cities'=>750];
        $path = $tmpDir.'/'.Input::file('file')->getClientOriginalName();
        $image = new SimpleImage();
        $image->load($path);
        $image->resizeToWidth(isset($sizes[$entity])?$sizes[$entity]:750);
        $image->save($path);

    }

}