<?php

class AirlinesController extends BaseController {

    // External API

    /**
     * @api {get} airlines List
     * @apiSampleRequest /api/v1/airlines
     * @apiVersion 1.0.0
     * @apiName GetAirlines
     * @apiGroup Airlines
     *
     * @apiSuccess {String} airline_vnt Код
     * @apiSuccess {String} airline_name Название
     * @apiSuccess {String} image Картинка
     * @apiSuccess {Integer} version Версия
     */

    public function index() {
        $lang = self::getLang(Input::get('lang'));

        $obj = Airlines::lang($lang)->get();

        if (!count($obj)) {
            return self::reply('No airlines found',404);
        }

        $airlines = $obj->toArray();

        return Response::json([
            'result' => $airlines,
        ],202);
    }


    // Angular

    public function getAngAirlinesList() {

        return Airlines::paginate(10);

    }

    public function getEditAngAirlines($id) {

        if ($id == 0) { // for Angular
            return json_encode(Airlines::$outputable);

        } else {
            $airlines = Airlines::select(Airlines::$list)->find($id);
            if (!$airlines) {
                return null;
            }
        }

        return $airlines;

    }
/*
    public function postCreateAngAirlines() {

        return Airlines::create(Input::all());
    }
*/
    public function putEditAngAirlines($id) {
        return (($obj = Airlines::find($id))?$obj->update(Input::all()):0);
    }
    /*
        public function destroy() {

            if(empty($_GET['ids'])) {
                return 0;
            }

            Airlines::destroy($_GET['ids']);

            return 1;
        }
    */
}