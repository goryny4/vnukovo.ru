<?php

class CmsController extends BaseController {

    // External API

    /**
     * @api {get} cms-sections Sections
     * @apiSampleRequest /api/v1/cms-sections?root=:root&lang=:lang
     * @apiVersion 1.0.0
     * @apiName Sections
     * @apiGroup Pages
     *
     * @apiParam {String} lang={ru|en}
     * @apiParam {String} root
     *
     * @apiSuccess {Integer} id Id страницы
     * @apiSuccess {String} title Заголовок новости
     */

    public function sections() {
        $validation = Validator::make(
            ['root'=>Input::get('root')],
            ['root' => 'required|in:how-to-get,services']
        );
        if ($validation->fails()) throw new UserException('Wrong root! Should be in {how-to-get, services}');

        $lang = self::getLang(Input::get('lang'));

        $obj = CmsSections::where('root',Input::get('root'))->lang($lang)->get();

        if (!count($obj)) {
            return self::reply('No sections for this root', 404);
        }

        $result = $obj->toArray();

        return Response::json([
            'result' => $result,
        ],202);
    }

    /**
     * @api {get} cms-pages Pages
     * @apiSampleRequest /api/v1/cms-pages?section=:section&lang=:lang
     * @apiVersion 1.0.0
     * @apiName Pages
     * @apiGroup Pages
     *
     * @apiParam {String} lang={ru|en}
     * @apiParam {String} section
     *
     * @apiSuccess {Integer} id Id страницы
     * @apiSuccess {String} image Картинка
     * @apiSuccess {String} title Заголовок новости
     * @apiSuccess {String} text Текст новости
     */

    public function pages() {
        $sections = array_column(CmsSections::all(['section'])->toArray(),'section');
    //    $sections = ['car','aeroexpress','bus','minibus','order-taxi'];
        $validation = Validator::make(Input::all(),
            ['section' => 'required|in:'.implode(',',$sections)]
        );
        if ($validation->fails()) throw new UserException('Wrong section! Should be in {'.implode(', ',$sections).'}');

        $lang = self::getLang(Input::get('lang'));

        $section  = CmsSections::where('section',Input::get('section'))->first();
        if ($section) {
            $obj = $section->pages()->lang($lang)->orderBy('is_primary','DESC')->get();
        } else $obj = [];

        if (!count($obj)) {
            return self::reply('No pages for this section', 404);
        }

        $result = $obj->toArray();

        return Response::json([
            'result' => $result,
        ],202);
    }

    // Angular

    public function getAngServicesCmsPagesList() {
        return self::_getByRoot('services');
    }

    public function getAngHowToGetCmsPagesList() {
        return self::_getByRoot('how-to-get');
    }

    private function _getByRoot($root) {
        $pagesize = 10;

        $sections = array_column(CmsSections::where('root',$root)->get()->toArray(),'section');
        if (count($sections))
            return CmsPages::admin()->whereIn('section',$sections)->paginate($pagesize)->toArray();
        else
            return ['data'=>[]];
    }

    public function getEditAngCms($id) {
        if ($id == 0) { // for Angular
            return json_encode(CmsPages::$outputable);

        } else {
            $result = CmsPages::select(CmsPages::$list)->find($id);
            if (!$result) {
                return null;
            }
        }

        return $result;
    }

    public function postCreateAngCms() {
        return CmsPages::create(Input::all());
    }

    public function putEditAngCms($id) {
        return (($obj = CmsPages::find($id))?$obj->update(Input::all()):0);
    }

    public function destroy() {

        if(empty($_GET['ids'])) {
            return 0;
        }

        CmsPages::destroy($_GET['ids']);

        return 1;
    }

}