<?php

class SpecialProposalsController extends BaseController {

    // Angular

    public function getAngSpecialProposalList() {
        if ($coid = Input::get('commercial_object_id')) {
            return SpecialProposal::where('commercial_object_id', $coid)->paginate(10);
        } else {
            return SpecialProposal::paginate(25);
        }
    }

    public function getEditAngSpecialProposal($id) {

        if ($id == 0) { // for Angular
            return json_encode(SpecialProposal::$outputable);

        } else {
            $news = CommercialObject::find($id)->specialProposal;
            if (!$news) {
                return null;
            }
        }

        return $news;

    }

    public function postCreateAngSpecialProposal() {
        // todo relative
        if (!Input::get('commercial_object_id')) return SpecialProposal::paginate(25);

        $model = new SpecialProposal;
        $input = Input::all();

        foreach ($input as $key =>$value) {
            $model->$key = $value;
        }

        return (int)$model->save();
    }


    public function putEditAngSpecialProposal($id) {
        return (($obj = SpecialProposal::find($id))?$obj->update(Input::all()):0);

    }

    public function destroy() {

        if(empty($_GET['ids'])) {
            return 0;
        }

        News::destroy($_GET['ids']);

        return 1;
    }


    // External API

    public function index() {

        $lang = self::getLang(Input::all());

        $model = SpecialProposal::lang($lang)->paginate(10);

        if (!count($model)) {
            return 'No items found';
        }

        $model = $model->toArray();

        return json_encode($model['data'], JSON_NUMERIC_CHECK);


    }

    public function show($id) {

        $lang = self::getLang(Input::all());

    //    $news = News::select('text_'.$lang , 'title_'.$lang)->find($id);
        $news = SpecialProposal::lang($lang)->find($id);
        if (!$news) {
            return 'Новость не найдена';
        }

        return $news;

    }

}