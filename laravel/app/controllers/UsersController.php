<?php

class UsersController extends BaseController {

    /**
     * @api {post} user/register Register
     * @apiDescription for registration with EMAIL only!
     * @apiSampleRequest /api/v1/user/register
     * @apiVersion 1.0.0
     * @apiName Register
     * @apiGroup User
     *
     * @apiParam {String} email Email
     * @apiParam {String} password Password
     *
     * @apiSuccess {Integer} token Token
     */


    public function apiRegisterWithEmail() {

        $input = Input::all();

        if (isset($input['social_network']) || isset($input['social_network_id'])) {
            return self::reply('Wrong hole!',400);
        }

        //check whether credentials are valid
        $validation = Validator::make($input,[
            'email' => 'required|email|max:100',
            'password' => 'required|min:5|max:100',
            'is_spam_accepted' => 'in:0,1',
        ]);

        if ($validation->fails()) {
            return self::reply('',406,[],$validation->messages());
        }

        // if email is already used
        if (User::where('email', $input['email'])->first()) {
            return self::reply('This email is already registered',409);
        } else {
            $user = User::create($input);
            $user->password = Hash::make($input['password']);
            $user->confirmation_code = str_random(32);

            $user->token = $this->_generateUserToken();
            $user->save();
            self::_email($user);
            return self::reply('User is successfully registered and logged in!',201,['token' => $user->token]);
        }
    }

    private static function _email($user) {

        // if () а дальше забыл что я хотел
        return mail('egoryny4@gmail.com',
            'Подтверждение email '.$user->email.'- мобильное приложение Аэропорт Внуково',
            "Чтобы подтвердить Email, кликните по <a href='" .URL::to("/api/v1/auth/confirm?email=$user->email&confirmation_code=$user->confirmation_code") . "'>ссылке</a>",
            "Content-type: text/html; charset=utf-8\n"
        );
    }


    // ------ ANON ---------

    /**
     * @api {post} user/anonymous Anonymous Login
     * @apiDescription Anonymous Login
     * @apiSampleRequest /api/v1/user/anonymous
     * @apiVersion 1.0.0
     * @apiName Anonymous
     * @apiGroup User
     *
     * @apiParam (Anonymous) {String="android","ios"} device_type Device Type
     * @apiParam (Anonymous) {String} device_id Device Id
     * @apiParam (Anonymous) {String} registration_id Registration Id
     *
     * @apiSuccess {Integer} token Token
     */
    public function apiAnonymous() { // both for email login and FB/VK login (includes registration on first login)
        $messages = array();

        $input = Input::all();


        $validation = Validator::make($input,[
            'device_type' => 'required',
            'device_id' => 'required',
            'registration_id' => 'required',
        ]);

        if ($validation->fails()) {
            return self::reply('',401,[],$validation->messages());
        }
        $anonCount = User::where('confirmation_code','')->count();

        $user = new User();
        $user->email = $anonCount.'@anonymous.gov';
        $user->token = $this->_generateUserToken();
        $user->save();

        $deviceData = array_intersect_key($input, array_flip(['device_type','device_id','registration_id']));
        $deviceData['user_id'] = $user->id;
        $device = new UserDevice($deviceData);
        $user->devices()->save($device);

        return self::reply([array_merge($messages,['Successfully logged in!'])], 202, ['token' => $user->token]);
    }


    // -------------------





    /**
     * @api {post} user/login Login
     * @apiDescription both for email login and FB/VK login (includes registration on first login)
     * @apiSampleRequest /api/v1/user/login
     * @apiVersion 1.0.0
     * @apiName Login
     * @apiGroup User
     *
     * @apiParam (Email Login) {String} email Email
     * @apiParam (Email Login) {String} password Password
     * @apiParam (Social Network Login) {String="fb","vk"} social_network Social Network
     * @apiParam (Social Network Login) {String} social_network_id Social Network Id
     * @apiParam (Social Network Login) {String} code Secret Code
     * @apiParam (Social Network Register) {String="fb","vk"} social_network Social Network
     * @apiParam (Social Network Register) {String} social_network_id Social Network Id
     * @apiParam (Social Network Register) {String} code Secret Code
     * @apiParam (Social Network Register) {String} email Email
     * @apiParam (Social Network Merge) {String="fb","vk"} social_network Social Network
     * @apiParam (Social Network Merge) {String} social_network_id Social Network Id
     * @apiParam (Social Network Merge) {String} code Secret Code
     * @apiParam (Social Network Merge) {String} email Email
     * @apiParam (Social Network Merge) {String} password Password
     *
     * @apiParam (Email Login) {String="android","ios"} device_type Device Type
     * @apiParam (Email Login) {String} device_id Device Id
     * @apiParam (Email Login) {String} registration_id Registration Id
     *
     * @apiParam (Social Network Login) {String="android","ios"} device_type Device Type
     * @apiParam (Social Network Login) {String} device_id Device Id
     * @apiParam (Social Network Login) {String} registration_id Registration Id
     *
     * @apiParam (Social Network Register) {String="android","ios"} device_type Device Type
     * @apiParam (Social Network Register) {String} device_id Device Id
     * @apiParam (Social Network Register) {String} registration_id Registration Id
     *
     * @apiParam (Social Network Merge) {String="android","ios"} device_type Device Type
     * @apiParam (Social Network Merge) {String} device_id Device Id
     * @apiParam (Social Network Merge) {String} registration_id Registration Id
     *
     * @apiSuccess {Integer} token Token
     */
    public function apiLogin() { // both for email login and FB/VK login (includes registration on first login)
        $messages = array();

        $input = Input::all();

        if (!empty($input['social_network']) && !empty($input['social_network_id'])) {
            if ($input['code'] != md5("vnukovo$input[social_network]$input[social_network_id]"))
            return self::reply('Wrong code',401);
        }

        $validation = Validator::make($input,[
            'social_network' => 'required_without:email|in:fb,vk',
            'social_network_id' => 'required_with:social_network|numeric',
            'code' => 'required_with:social_network',

            'email' => 'sometimes|required|email|max:100',
            'password' => 'sometimes|required_with:email|min:5|max:100',
            'is_spam_accepted' => 'sometimes|in:0,1',

            'device_type' => 'required',
            'device_id' => 'required',
            'registration_id' => 'required',
        ]);

        if ($validation->fails()) {
            return self::reply('',401,[],$validation->messages());
        }

        if (isset($input['social_network'])) {
            $user = User::where('social_network', $input['social_network'])->where('social_network_id', $input['social_network_id'])->first();
            if ($user) {
                if (isset($input['email'])) {
                    // обновляем мыло (мало ли)
                    if ($input['email'] != $user->email) {
                        $user->update(['email' => $input['email']]);
                        $messages[] = 'Email successfully updated';
                    } else {
                        $messages[] = 'Why do you send me an email? This social network user has been already registered earlier!';
                    }
                }
            } else {
                if (isset($input['email'])) {
                    $user = User::where('email', $input['email'])->first();
                    if ($user) {
                        if (isset($input['password'])) {
                            if (Hash::check($input['password'], $user->password)) {
                                $user->update(array_intersect_key($input,array_flip(['social_network','social_network_id','first_name','last_name'])));
                                if (isset($input['birthday'])) {
                                    $user->birthday = date('Y-m-d', strtotime(str_replace('-', '/', $input['birthday'])));
                                    $user->save();
                                }
                                $messages[] = 'Successfully linked to existing account';
                            } else {
                                return self::reply('The password is incorrect',401);
                            }
                        } else {
                            // если пользователь с таким мылом уже есть в системе, то мы запрашиваем email и пароль
                            return self::reply('This email is already taken. If its yours, please provide your password as well. Your existing account will be linked to your social network account.',401);
                        }
                    } else {
                        $user = User::create($input);
                        if (isset($input['birthday'])) $user->birthday = date('Y-m-d', strtotime(str_replace('-', '/', $input['birthday'])));
                        $user->confirmation_code =  str_random(32);
                        self::_email($user);
                    }
                } else {
                    return self::reply('User is not registered. Please include an email to register a new user',400);
                }
            }
        } elseif (isset($input['email'])) {
            $user = User::where('email', $input['email'])->first();
            if ($user) {
                if (!Hash::check($input['password'], $user->password)) {
                    return self::reply('Incorrect credentials',401);
                }
            } else {
                return self::reply('Incorrect credentials',401);
            }
        }

        $user->token = $this->_generateUserToken();
        // $user->setUpdatedAt(time());
        $user->save();
        $deviceData = array_intersect_key($input, array_flip(['device_type','device_id','registration_id']));
        $deviceData['user_id'] = $user->id;
        if (!UserDevice::where($deviceData)->first()) {
            $device = new UserDevice($deviceData);
            $user->devices()->save($device);
        }

        return self::reply([array_merge($messages,['Successfully logged in!'])], 202, ['token' => $user->token]);
    }

    public function apiLogout() {

        $input = Input::all();
        //check whether credentials are valid
        $validation = Validator::make($input,[
            'token' => 'required'
        ]);

        if ($validation->fails()) {
            return self::reply('No fate',406,[],$validation->messages());
        }

        $user = User::where('token', Input::get('token'))->first();

        if ($user) {
            // если это не аноним, то просто очищаем
            if ($user->confirmationCode) {
                $user->token = '';
                $user->save();
            } else {
                // если это был аноним, то удаляем юзера
                // но вообще такого быть как бы не может
                $user->delete();
                return self::reply('Successfully logged out an anonymous user... Strange, but true',202);
            }
            return self::reply('Successfully logged out',202);
        } else {
            return self::reply('Such user is not logged in',202);
        }


    }

    public function apiForgot(){
        return self::reply("We couldn't remind your password either");
    }

    public function apiDelete(){
        return self::reply('Bye',202);
    }

    public function apiConfirm(){
        $email = Input::get('email');
        $confirmationCode = Input::get('confirmation_code');

        $user = User::where('email',$email)->and('confirmation_code',$confirmationCode)->first();
        $user->email_checked = true;
        $user->save();

        return self::reply('Thank you. Email is confirmed.');
    }



    private function _generateUserToken() {
        do {
            $random = str_random(32);
        } while (User::where('token', $random)->first());
        return $random;
    }

    public function apiAnonymousLogin() {

    }


    // --------------- ANGULAR API -------------------------------------------------------------


    public function getAngUsersList() {

        return DB::table('users')->select('id', 'email', 'first_name', 'last_name', 'social_network', 'social_network_id', 'created_at', 'updated_at')->paginate(10);

    }

    public function getEditAngUser($id) {

        if ($id == 0) {

            return json_encode(['email' => '', 'password' => '', 'first_name' => '', 'last_name' => '', 'social_network' => '', 'social_network_id' => '']);

        } else {

            $user = User::select('id', 'email', 'first_name', 'last_name', 'social_network', 'social_network_id', 'created_at', 'updated_at')->find($id);
            if (!$user) {
                return null;
            }
        }

        return $user;

    }

    public function postCreateAngUser() {
        return User::create(array_merge(
            Input::except('password'),
            ['password' =>Hash::make(Input::get('password'))]
        ));
    }

    public function putEditAngUser($id) {
        $user = User::find($id);
        if (!$user) {
            return 0;
        }

        $input = Input::all();

        foreach ($input as $key =>$value) {
            if ($key == 'password') {
                if ($value == '') continue;
                else $value = Hash::make($value);
            }
            $user->$key = $value;
        }

        return (int)$user->save();
    }

    public function destroy() {

        if (empty($_GET['ids'])) {
            return 0;
        }

        User::destroy($_GET['ids']);

        return 1;
    }

}