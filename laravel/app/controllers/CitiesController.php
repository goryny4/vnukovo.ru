<?php

class CitiesController extends BaseController {

    // External API

    /**
     * @api {get} cities List
     * @apiSampleRequest /api/v1/cities
     * @apiVersion 1.0.0
     * @apiName GetCities
     * @apiGroup Cities
     *
     * @apiSuccess {String} city_en Город
     * @apiSuccess {String} image Картинка
     * @apiSuccess {Integer} version Версия
     */

    public function index() {
        $lang = self::getLang(Input::get('lang'));

        $obj = Cities::lang($lang)->get();

        if (!count($obj)) {
            return self::reply('No cities found',404);
        }

        $cities = $obj->toArray();

        // -- check if img really exist ----------
        $cities = array_filter(array_map(function($city) {
            if (is_null($city['image']) || !file_exists(storage_path($city['image']))) {
                $city['image'] = null;
                if (Input::get('null')) echo $city['city_en'].'<br/>';
            }
            return $city;
        }, $cities));

        if (Input::get('null')) return;
        // ---------------------------


        return Response::json([
            'result' => $cities,
        ],202);
    }


    // Angular

    public function getAngCitiesList() {

        return Cities::paginate(10);

    }

    public function getEditAngCities($id) {

        if ($id == 0) { // for Angular
            return json_encode(Cities::$outputable);

        } else {
            $cities = Cities::select(Cities::$list)->find($id);
            if (!$cities) {
                return null;
            }
        }

        return $cities;

    }
/*
    public function postCreateAngCities() {

        return Cities::create(Input::all());
    }
*/
    public function putEditAngCities($id) {
        return (($obj = Cities::find($id))?$obj->update(Input::all()):0);
    }
    /*
        public function destroy() {

            if(empty($_GET['ids'])) {
                return 0;
            }

            Cities::destroy($_GET['ids']);

            return 1;
        }
    */
}