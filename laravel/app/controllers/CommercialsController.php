<?php

class CommercialsController extends BaseController {

    // Angular

    public function getAngCommercialObjectList() {

        $typeId = (($obj = ObjectType::where('name', Input::get('type'))->first())?$obj->id:0);

        return CommercialObject::where('commercial_object_type_id', $typeId)->paginate(10);

    }

    public function getEditAngCommercialObject($id) {

        if ($id == 0) { // for Angular
            return json_encode(CommercialObject::$outputable);

        } else {
            $commercialObjects = CommercialObject::with('SpecialProposals')->select(CommercialObject::$list)->find($id);
            if (!$commercialObjects) {
                return null;
            }
        }

        return $commercialObjects;

    }

    public function postCreateAngCommercialObject() {
        return CommercialObject::create(array_merge(
            Input::except('type'),
            ['commercial_object_type_id'=>ObjectType::where('name', Input::get('type'))->first()->id]
        ));
    }

    public function putEditAngCommercialObject($id) {
        return (($obj = CommercialObject::find($id))?$obj->update(Input::all()):0);
    }

    public function destroy() {

        if(empty($_GET['ids'])) {
            return 0;
        }

        CommercialObject::destroy($_GET['ids']);

        return 1;
    }

    public function index($commercial) {
        $lang = self::getLang(Input::all());

        $type = ObjectType::where('name', $commercial)->first()->id;
        $objects = CommercialObject::lang($lang)->where('object_type_id', $type)->paginate(10);

        if(count($objects)) {
            $objects = $objects->toArray();
            return json_encode([
                'status' => 'success',
                'result' => $objects,
                'code' => 202
            ]);
        } else {
            return json_encode([
                'status' => 'error',
                'messages' => [CommercialObject::$typeNamesPl[$commercial].' не найдены'],
                'code' => 404
            ]);
        }
    }

    public function show($commercial, $id) {
        $lang = self::getLang(Input::all());

        $type = ObjectType::where('name', $commercial)->first()->id;
        $object = CommercialObject::lang($lang)->where('object_type_id', $type)->find($id);
        if ($object) {
            $object = $object->toArray();
            return json_encode([
                'status' => 'success',
                'result' => $object,
                'code' => 202
            ]);
        } else {
            return json_encode([
                'status' => 'error',
                'messages' => [CommercialObject::$typeNamesSing[$commercial].' не найден'],
                'code' => 404
            ]);
        }
    }


}