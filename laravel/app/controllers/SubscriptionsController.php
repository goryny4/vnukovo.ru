<?php

class SubscriptionsController extends BaseController {

    private $user;

    public function __construct() {

        $token =  Input::get('token');
    //    $token = 'Oc4Jn5VYWsWUShQ5TZXjHDAScaJgpcDz';
        $auth = $this->auth($token);
        if ($auth['status']) $this->user = $auth['user'];
        return parent::__construct();
    }

    /**
     * @api {get} subscriptions List
     * @apiSampleRequest /api/v1/subscriptions?lang=:lang
     * @apiVersion 1.0.0
     * @apiName GetSubscriptions
     * @apiGroup Subscriptions
     *
     * @apiParam {String} lang Язык
     * @apiParam {String} token Token
     *
     */

    public function index() {
        $validation = Validator::make([
            'token' => Input::get('token'),
         ] , [
            'token' => 'required',
        ]);

        if ($validation->fails()) {
            return self::reply($validation->messages(),406);
        }
        $lang = self::getLang(Input::get('lang'));
        if (!$this->user) return self::reply('You are not authorized to view personalized data',400);

        $subscriptions = $this->user->subscriptions()->with(array('flight' => function($query) use ($lang)
        {
            $query->select('flt_id','bound',
                'flight_number_for_pass',
                'st',
                "city_$lang as city", // "airport_$lang as airport",
            //    "vnukovo_${lang}_status_short as vnukovo_status_short",
                "vnukovo_${lang}_status as vnukovo_status_short",
                'term'
            );
        }))->get()->toArray();

        return Response::json([
            'result' => $subscriptions
        ],202);
    }


    /**
     * @api {POST} subscriptions Add
     * @apiSampleRequest /api/v1/subscriptions
     * @apiVersion 1.0.0
     * @apiName AddSubscriptions
     * @apiGroup Subscriptions
     *
     * @apiParam {Integer} flt_id FltId
     * @apiParam {String} token Token
     *
     */

    public function store() {
        if (!$this->user) return self::reply('You are not authorized to view personalized data',400);
        $input = Input::all();
        $fltId = isset($input['flt_id']) ? $input['flt_id'] : false;
        $validation = Validator::make([
            'token' => $input['token'],
            'flt_id' => $fltId,
        ] , [
            'token' => 'required',
            'flt_id' => 'required|numeric',
        ]);

        if ($validation->fails()) {
            return self::reply($validation->messages(),406);
        }

        $flightObj = Flight::where('flt_id',$fltId)->first();
        if (!$flightObj) {
            return self::reply('Неверный номер рейса',406);
        }
        $flight = [
            'flt_id'=>$fltId,
        ];
        if ($this->user->subscriptions()->where($flight)->first()) {
            return self::reply('Оповещение уже в базе');
        }
        $this->user->subscriptions()->save(new Subscription($flight));
        return self::reply('Оповещение добавлено');
    }

    /**
     * @api {DELETE} subscriptions Remove
     * @apiSampleRequest /api/v1/subscriptions/:flt_id/
     * @apiVersion 1.0.0
     * @apiName RemoveSubscriptions
     * @apiGroup Subscriptions
     *
     * @apiParam {Integer} flt_id Id рейса
     * @apiParam {String} token Token
     *
     */

    public function destroy($flt_id) {
        if (!$this->user) return self::reply('You are not authorized to view personalized data',400);
        $subscriptions = $this->user->subscriptions()->where('flt_id',(int)$flt_id)->get();
        $result = count($subscriptions);
        foreach ($subscriptions as $sub) {
            $sub->delete();
        }

        return self::reply($result?'Оповещение удалено':'Нет таких оповещений');
    }

}