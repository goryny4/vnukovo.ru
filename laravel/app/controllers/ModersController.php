<?php

class ModersController extends BaseController {

    public function getAngModersList() {

        return DB::table('moders')->paginate(10);

    }

    public function getEditAngModer($id) {

        if ($id == 0) {

            return json_encode(['username' => '', 'password' => '', 'group' => 'content']);

        } else {

            $moder = Moder::select('id', 'username', 'group')->find($id);
            if (!$moder) {
                return null;
            }
        }

        return $moder;

    }

    public function postCreateAngModer() {
        return Moder::create(array_merge(
            Input::all(),
            ['password'=> Hash::make(Input::get('password'))]
        ));
    }


    public function putEditAngModer($id) {
        $moder = Moder::find($id);
        if (!$moder) {
            return 0;
        }

        $input = Input::all();

        foreach ($input as $key =>$value) {
            if ($key == 'password') {
                if ($value == '') continue;
                else $value = Hash::make($value);
            }
            $moder->$key = $value;
        }

        return (int)$moder->save();
    }

    public function destroy() {
        return (!empty($_GET['ids'])?Moder::destroy($_GET['ids']):0);
    }

}
