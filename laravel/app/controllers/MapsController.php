<?php

class MapsController extends BaseController {

    // External API

    /**
     * @api {get} maps List
     * @apiSampleRequest /api/v1/maps?lang=:lang
     * @apiVersion 1.0.0
     * @apiName GetMaps
     * @apiGroup Maps
     *
     * @apiParam {String} lang={ru|en}
     *
     * @apiSuccess {String} terminal Терминал
     * @apiSuccess {Integer} floor Этаж
     * @apiSuccess {String} image Картинка
     * @apiSuccess {Integer} version Версия
     */

    public function index() {
        $lang = self::getLang(Input::get('lang'));

        $obj = Maps::orderBy('terminal', 'ASC')->lang($lang)->get();

        if (!count($obj)) {
            return self::reply('No maps found',404);
        }

        $maps = $obj->toArray();

        return Response::json([
            'result' => $maps,
        ],202);
    }


    // Angular

    public function getAngMapsList() {

        return Maps::paginate(10);

    }

    public function getEditAngMaps($id) {

        if ($id == 0) { // for Angular
            return json_encode(Maps::$outputable);

        } else {
            $maps = Maps::select(Maps::$list)->find($id);
            if (!$maps) {
                return null;
            }
        }

        return $maps;

    }
/*
    public function postCreateAngMaps() {

        return Maps::create(Input::all());
    }
*/
    public function putEditAngMaps($id) {
        return (($obj = Maps::find($id))?$obj->update(Input::all()):0);
    }
    /*
        public function destroy() {

            if(empty($_GET['ids'])) {
                return 0;
            }

            Maps::destroy($_GET['ids']);

            return 1;
        }
    */
}