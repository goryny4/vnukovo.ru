<?php

class LogsController extends BaseController {

    public function getAngLogsList() {
        return OperationLog::orderBy('updated_at', 'DESC')->paginate(10);
    }

}