<?php


class AuthController extends BaseController {

    public function getLogin() {

        return View::make('auth.login');

    }

    public function postLogin() {

        $input = Input::all();

        $auth = Auth::attempt(['username' => $input['username'], 'password' => $input['password']], true, true);

        if ($auth) {
            $user = Auth::user();
            $user->last_ip = Request::getClientIp();
            $user->last_login_date = date('Y-m-d h:i:s');
            $user->save();
        }

        return Redirect::to('/');
    }

    public function logout() {

        if (Auth::check()) {
            Auth::logout();
        }

        return Redirect::to('/');

    }


}