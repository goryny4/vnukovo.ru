<?php

class NewsController extends BaseController {

    // External API

    /**
     * @api {get} news List
     * @apiSampleRequest /api/v1/news?page=:page&lang=:lang
     * @apiVersion 1.0.0
     * @apiName GetNews
     * @apiGroup News
     *
     * @apiParam {String} lang={ru|en}
     * @apiParam {Integer} page
     *
     * @apiSuccess {Integer} id Id новости
     * @apiSuccess {Integer} published_at Дата публикации
     * @apiSuccess {String} image Картинка
     * @apiSuccess {String} title Заголовок новости
     * @apiSuccess {String} text Текст новости
     */

    public function index() {
        $pagesize = 10;

        $lang = self::getLang(Input::get('lang'));

        $obj = News::orderBy('published_at', 'DESC')->lang($lang)->paginate($pagesize);

        if (!count($obj)) {
            return self::reply('No news', 404);
        }

        $news = $obj->toArray();

        if (count($news['data']))
            $result =Ads::fillWithAds('News',$news['data'],$lang);
        else
            $result = array();

        return Response::json([
            'result' => $result,
            'paginationData' => [
                'currentPage' => $obj->getCurrentPage(),
                'lastPage' => $obj->getLastPage(),
                'perPage' => $obj->getPerPage(),
                'total' => $obj->getTotal(),
                'from' => $obj->getFrom(),
                'to' => $obj->getTo(),
                'count' => $obj->count()
            ],
        ],202);
    }

    /**
     * @api {get} news/:id Item
     * @apiSampleRequest /api/v1/news/:id/?lang=:lang
     * @apiVersion 1.0.0
     * @apiName GetNewsById
     * @apiGroup News
     *
     * @apiParam {Number} id News unique ID
     * @apiParam {String} lang Lang
     *
     * @apiSuccess {Integer} id Id новости
     * @apiSuccess {Integer} published_at Дата публикации
     * @apiSuccess {String} title Заголовок новости
     * @apiSuccess {String} text Текст новости
     *
     */
/*
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "id": 2,
     *       "published_at": 2015-02-22 23:00:15,
     *       "title_ru": "Мы открываем портал в созвездие Ориона",
     *       "text_ru": "Текст Новости. Мы открываем портал в созвездие Ориона"
     *     }
     *
     * @apiError NewsNotFound The id of the News was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "error": "UserNotFound"
     *     }*/
    public function show($id) {

        $lang = self::getLang(Input::all());

        $news = News::lang($lang)->find($id);
        if ($news) {
             return Response::json([
                'result' => $news,
             ],202);
        }

        return self::reply('No news',404);

    }

    // Angular

    public function getAngNewsList() {

        return News::orderBy('published_at', 'DESC')->paginate(10);

    }

    public function getEditAngNews($id) {

        if ($id == 0) { // for Angular
            return json_encode(News::$outputable);

        } else {
            $news = News::select(News::$list)->find($id);
            if (!$news) {
                return null;
            }
        }

        return $news;

    }

    public function postCreateAngNews() {
        return News::create(Input::all());
    }

    public function putEditAngNews($id) {
        return (($obj = News::find($id))?$obj->update(Input::all()):0);
    }

    public function destroy() {

        if(empty($_GET['ids'])) {
            return 0;
        }

        News::destroy($_GET['ids']);

        return 1;
    }

}