<?php namespace App\Events;

use Illuminate\Support\ServiceProvider;
use Illuminate\Events\Dispatcher;
use LogEventSubscriber;

class EventServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->events->subscribe(new LogEventSubscriber);
    }



}