<?php
use Illuminate\Events\Dispatcher;

class LogEventSubscriber {

    public function subscribe($event) {

        $event->listen('log.register', 'LogEventSubscriber@registerLog');

    }


    public function registerLog($data) {

        $ip = Request::getClientIp();

        if (!$moder = Auth::user()) return;

        OperationLog::create([
            'operation' => $data['operation'],
            'moder_id' => $moder->id,
            'moder_name' => $moder->username,
            'request_ip' => $ip,
            'moder_group' => $moder->group,
            'category' => $data['category'],
            'entity_id' => $data['entity_id'],
        ]);

    }

}