//var module = angular.module('module', ['ngResource', 'ngRoute', 'LocalStorageModule','mgcrea.ngStrap', 'ngAnimate', 'ngSanitize']);
var App2 = angular.module('CjClubUserApp', ['module', 'templates', 'ngDragDrop', 'toaster']);
App2.run(function (singlePlayer, $rootScope, $window) {
	$rootScope.$on('$viewContentLoaded', function(){
		$window.scrollTo(0, 0);
	});
	'use strict';
	singlePlayer.run();
});
App2.config(function ($routeProvider) {
	'use strict';

	$routeProvider.
		when('/news', {
			templateUrl: 'main/partials/articles.html',
			controller: ['$scope', 'Articles', function ($scope, Articles) {
				$scope.articles = Articles;
			}],
			resolve: {
				Articles: function (Blogs) {
					return Blogs.query();
				}
			}
		}).
		when('/news/:id', {
			templateUrl: 'main/partials/article.html',
			controller: ['$scope', 'Article', function ($scope, Article) {
				$scope.article = Article;
			}],
			resolve: {
				Article: function (Blogs, $route) {
					return Blogs.get({id: $route.current.params.id});
				}
			}
		}).
		when('/tracks', {
			templateUrl: 'main/partials/tracks.html',
			controller: 'TracksCtrl',
			resolve: {
				Styles: function (StylesResource) {
					return StylesResource.query();
				}
			}
		}).
		when('/tracks/:id', {
			templateUrl: 'main/partials/track.html',
			controller: 'TrackCtrl',
			resolve: {
				Track: function (TracksResource, $route, $q) {
					return TracksResource.getWithMetaData({id: $route.current.params.id},function(track){
						$("meta[name='keywords']").attr('content',track.meta_keywords);
						$("meta[name='description']").attr('content',track.meta_description);
						$("title").html(track.meta_title);
					}).$promise;
				}
			}
		}).
		when('/projects/:projectId/tracks/', {
			templateUrl: 'main/partials/project-tracks.html',
			controller: 'ProjectTracksCtrl',
			resolve: {
				Project: function (Projects, $route) {
					return Projects.get({id: $route.current.params.projectId}).$promise;
				}
			}
		}).
		when('/albums', {
			templateUrl: 'main/partials/albums.html',
			controller: 'AlbumsCtrl'
		}).
		when('/projects/:projectId/albums', {
			templateUrl: 'main/partials/project-albums.html',
			controller: 'ProjectAlbumsCtrl',
			resolve: {
				Project: function (Projects, $route) {
					return Projects.get({id: $route.current.params.projectId}).$promise;;
				},
				Albums: function (Albums, $route) {
					return Albums.resource.query({projectId: $route.current.params.projectId});
				}
			}
		}).
        when('/projects/:projectId/members', {
            templateUrl: 'main/partials/project-albums.html',
            controller: 'ProjectAlbumsCtrl',
            resolve: {
                Project: function (Projects, $route) {
                    return Projects.get({id: $route.current.params.projectId}).$promise;;
                },
                Albums: function (Albums, $route) {
                    return Albums.resource.query({projectId: $route.current.params.projectId});
                }
            }
        }).
		when('/projects/:projectId/albums/new', {
			templateUrl: 'main/partials/album.html',
			controller: 'AlbumCtrl',
			resolve: {
				Project: function (Projects, $route) {
					return Projects.get({id: $route.current.params.projectId}).$promise;
				},
				Album: function (Albums, $route) {
					var album = Albums.getDefaults();
					album.projectId = $route.current.params.projectId;
					return album;
				},
				Tracks: function (TracksResource, $route) {
					var albumId = $route.current.params.id;
					if(albumId){
						return TracksResource.queryByAlbum({albumId: $route.current.params.id,
							ordering: 'albumOrder,public_date'}).$promise;
					}
					else{
						return [];
					}
				}
			}
		}).
		when('/projects/:projectId/albums/:id', {
			templateUrl: 'main/partials/album.html',
			controller: 'AlbumCtrl',
			resolve: {
				Project: function (Projects, $route) {
					return Projects.get({id: $route.current.params.projectId}).$promise;
				},
				Album: function (Albums, $route, $q) {
					return Albums.resource.getWithMetaData({id: $route.current.params.id}, function (album) {
						$("meta[name='keywords']").attr('content',album.meta_keywords);
						$("meta[name='description']").attr('content',album.meta_description);
						$("title").html(album.meta_title);
					}, function () {
						alert('Альбом не найден');
					}).$promise;
				},
				Tracks: function (TracksResource, $route) {
					return TracksResource.queryByAlbum({albumId: $route.current.params.id,
						ordering: 'albumOrder,-public_date'}).$promise;
				}
			}
		}).
		when('/about', {
			templateUrl: 'main/partials/textpage.html',
			controller: ['$scope', 'Content', function ($scope, Content) {
				$scope.content = Content;
			}],
			resolve: {
				Content: function () {
					return {
						label: 'О проекте',
						content: 'some text about'
					};
				}
			}
		}).
		when('/projects', {
			templateUrl: 'main/partials/projects.html',
			controller: 'ProjectsCtrl',
			resolve: {
				Projects: function (Projects) {
					return Projects.query();
				}
			}
		}).
		when('/projects/:id', {
			templateUrl: 'main/partials/project.html',
			controller: 'ProjectCtrl',
			resolve: {
				Project: function (Projects, $route) {
					return Projects.get({id: $route.current.params.id},function(project) {
						$("meta[name='keywords']").attr('content',project.meta_keywords);
						$("meta[name='description']").attr('content',project.meta_description);
						$("title").html(project.meta_title);
					}).$promise;
				},
				Blogs: function (Blogs, $route) {
				//	return Blogs.query({projectId: $route.current.params.id}).$promise;
				}
			}
		}).
		when('/articles/new', {
			templateUrl: 'main/partials/articlenew.html',
			controller: 'ArticleEditCtrl'
		}).
		when('/accounts/:id', {
			templateUrl: 'main/partials/account.html',
			controller: 'AccountCtrl',
			resolve: {
				User: function (UsersResource, $route) {
					return UsersResource.get({id: $route.current.params.id});
				}
			}
		}).
		when('/pages/:id', {
			templateUrl: 'main/partials/textpage.html',
			controller: ['$scope', 'PageCtrl', function ($scope, page) {
				$scope.content = page;
			}],
			resolve: {
				PageCtrl: function (PagesResource, $route) {
					return PagesResource.getByLabel({id: $route.current.params.id},function(page){
						$("meta[name='keywords']").attr('content',page.meta_keywords);
						$("meta[name='description']").attr('content',page.meta_description);
						if (page.meta_title) $("head>title").html(page.meta_title);
						if (page.meta_head) $("head").append(page.meta_head);
					}).$promise;
			   }
			}
		}).
		when('/demo', {
			templateUrl: 'main/partials/demopage.html',
			controller: 'DemoCtrl'
		}).
		when('/controlcenter', {
			templateUrl: 'main/partials/controlcenter.html',
			controller: 'ControlCenterCtrl'
		}).
		when('/controlcenter/subscribers', {
			templateUrl: 'main/partials/controlcenter-subscribers.html',
			controller: 'ControlCenterSubscribersCtrl'
		}).
		when('/controlcenter/subscription', {
			templateUrl: 'main/partials/controlcenter-subscription.html',
			controller: 'ControlCenterSubscriptionCtrl'
		}).
		when('/settings', {
			templateUrl: 'main/partials/settings.html',
			controller: 'SettingsCtrl'
		}).
		when('/promostats/personal', {
			templateUrl: 'main/partials/promo-statistic-personal.html',
			controller: 'PromoStatisticCtrl'
		}).
		when('/promostats/global', {
			templateUrl: 'main/partials/promo-statistic-global.html',
			controller: 'PromoStatisticCtrl'
		}).
		when('/tariffs', {
			templateUrl: 'main/partials/tariffs.html',
			controller: 'TariffsCtrl'
		}).
		when('/tariffs/:id', {
			templateUrl: 'main/partials/tariff.html',
			controller: 'TariffCtrl'
		}).
		when('/services', {
			templateUrl: 'main/partials/services.html',
			controller:  'ServicesCtrl'
		}).
		when('/services/:id/:param?', {
			templateUrl: 'main/partials/service.html',
			controller:  'ServiceCtrl'
		}).
		when('/addMoney', {
			templateUrl: 'main/partials/addMoney.html',
			controller:  'AddMoneyCtrl'
		}).
		when('/history', {
			templateUrl: 'main/partials/history.html',
			controller:  'HistoryOperationsCtrl',
			resolve: {
				history: function($resource){
					return $resource('api/moneylog', null, {});
				}
			}
		}).
		when('/transfer', {
			templateUrl: 'main/partials/transfer.html',
			controller:  'TransferCtrl'
		}).
		when('/musiccollection', {
			templateUrl: 'main/partials/musiccollection.html',
			controller: 'MusicCollectionCtrl'
		}).
		when('/users/:id', {
			templateUrl: 'main/partials/userpage.html'
		}).
		when('/report', {
			templateUrl: 'main/partials/report.html',
			controller: 'ReportCtrl'
		}).
        when('/signup', {
            templateUrl: 'main/partials/registration.html',
            controller: 'RegistrationCtrl',
            resolve: {
                AccountTypes: function(AccountTypesResource) {
                    return AccountTypesResource.query();
                },
                Tops: function(MysicstylesResource) {
                    return MysicstylesResource.queryTop();
                },
                styles: function(MysicstylesResource) {
                    return MysicstylesResource.queryTop();
                }
            }
        }).
        when('/addproject', {
            templateUrl: 'main/partials/add_project.html',
            controller: 'AddProjectCtrl',
            resolve: {
                AccountTypes: function(AccountTypesResource) {
                    return AccountTypesResource.query();
                },
                Tops: function(MysicstylesResource) {
                    return MysicstylesResource.queryTop();
                },
                styles: function(MysicstylesResource) {
                    return MysicstylesResource.queryTop();
                }
            }
        }).
        when('/end_registration', {
            templateUrl: 'main/partials/end_registration.html',
            controller: 'EndRegistration'
        }).
		otherwise({
			redirectTo:  '/',
			templateUrl: 'main/partials/index.html'
		});
});

App2.config(['$compileProvider', '$provide', function ($compileProvider, $provide) {
	'use strict';
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|skype):/);
	$provide.decorator('$rootScope', function ($delegate) {
		$delegate.safeApply = function (callback) {
			if (!angular.isFunction(callback)) {
				callback = angular.noop;
			}

			if ($delegate.$$phase) {
				$delegate.$apply(callback);
			} else {
				callback();
			}
		};

		return $delegate;
	});
}]);
