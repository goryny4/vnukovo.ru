<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Вход в админ-панель</title>

    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                        <div class="container">
                            <div class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2" id="loginbox" style="margin-top: 50px;">
                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <div class="panel-title">Вход</div>
                                    </div>

                                    <div class="panel-body" style="padding-top: 30px;">

                                        <div class="alert alert-danger col-sm-12" id="login-alert" style="display: none;"></div>

                                        <form method="POST" action="{{URL::to('/auth/login')}}" class="form-horizontal" id="loginform" role="form">

                                            <div class="input-group" style="margin-bottom: 25px;">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                <input name="username" class="form-control" id="login-username" type="text" placeholder="имя пользователя" value="" required>
                                            </div>

                                            <div class="input-group" style="margin-bottom: 25px;">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                                <input name="password" class="form-control" id="login-password" type="password" placeholder="пароль" required>
                                            </div>


                                            <div class="form-group" style="margin-top: 10px;">
                                                <!-- Button -->

                                                <div class="col-sm-12 controls">
                                                    <button class="btn btn-success" id="btn-login" type="submit">Вход  </button>

                                                </div>
                                            </div>

                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>