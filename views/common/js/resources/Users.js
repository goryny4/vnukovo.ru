module.factory('UsersResource', function($resource) {
	return $resource('users/:id', {}, {
		queryOnline: {
			url: 'users/online',
			method: 'GET',
			isArray: true
		},

        queryFriends: {
            url: 'api/users/friends',
            method: 'GET',
            isArray: false
        },

        querySearch: {
            url: 'api/users',
            method: 'GET',
            isArray: false
        },

        queryTransfer: {
            url: 'api/users/transfer',
            method: 'POST'
        },

        queryPing: {
            url: 'api/users/ping',
            method: 'POST',
			isArray: false
        },

        queryOnlineCount: {
            url: 'api/users/onlineCount',
            method: 'POST',
			isArray: false
        }
	});
});