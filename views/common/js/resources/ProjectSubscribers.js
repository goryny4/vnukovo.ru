module.factory('ProjectSubscribersResource', ['$resource', 'SignedUser', function($resource, SignedUser) {
    var service = {
        resource: $resource(null, null,
            {
                'postSubscribe': {
                    method:'POST',
                    url: '/api/projects/subscribe'
                },
                'getSubscription': {
                    method:'GET',
                    url: '/api/projects/subscription',
                    isArray: false
                },
                'getSubscribers': {
                    method: 'GET',
                    url: '/api/projects/subscribers',
                    isArray: false
                }
            }),
        getTitle: function() {
            if (!SignedUser.isSigned())
                return 'Авторизируйтесь, чтобы подсписаться на проект';
            return '';//''Лайкнуть трек';
        },
        subscribe: function(project) {
            if (SignedUser.isSigned()) {
                var query = this.resource.postSubscribe(
                    {'project_id': project.id}
                );
                return query;
            }
        },
        update_user: function(project) {
            SignedUser.user.subscription_count = parseInt(SignedUser.user.subscription_count, 10);
            SignedUser.user.subscription_count += (project.subscribe) ? 1 : -1;
        }
    };
    return service;
}]);