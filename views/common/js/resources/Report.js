module.factory('ReportResource', function ($resource) {
    'use strict';
    return $resource('api/report/',{},
    	{
    		send:{
    			method: 'POST'
    		}
    	}
    );
});