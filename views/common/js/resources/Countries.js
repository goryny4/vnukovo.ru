module.factory('CountriesResource', function($resource) {

    return $resource('/api/countries/:id', {}, {

        queryPage: {
            method: 'GET',
            url: 'api/countries',
            isArray: true
        }
    });
});