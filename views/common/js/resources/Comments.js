module.factory('Comments', ['$resource', function ($resource) {
	'use strict';
	return $resource('/api/comments/:objectType/:objectId', {}, {
		add: {
			method: 'POST',
			url: '/api/comments/add/:objectType/:objectId',
			params: {objectType: '@objectType', objectId: '@objectId'}
		},
        getComment: {
            method: 'GET',
            url: '/api/comments/:id'
        }
	});
}]);