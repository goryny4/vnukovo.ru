
module.factory('ServicesResource', function($resource){
    'use strict';
    return $resource('api/services/:id', {}, {

        queryPage: {
            method: 'GET',
            url:    'api/services'
        },

        queryActive: {
            method: 'GET',
            url: 'api/services/active',
            isArray: true
        },

        subscribeTo: {
            method: 'GET',
            url:    'api/service/subscribe'
        },

        extend: {
            method: 'POST',
            url: 'api/service/extend'
        }
    })
});