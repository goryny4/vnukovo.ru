module.factory('PagesResource', function($resource) {
    return $resource('/api/pages/:id', {}, {
        getByLabel: {
            method: 'GET',
            url: 'api/pages/:id'
        }
    });
});

