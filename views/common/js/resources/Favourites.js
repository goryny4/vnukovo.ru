module.factory('FavouritesResource', ['$resource', 'SignedUser', function($resource, SignedUser) {
    var service = {
        resource: $resource(null, null,
            {
                'postFavourite': {
                    method:'POST',
                    url: '/api/trackfavourites/favourite'
                }
            }),
        getTitle: function(track) {
            if (!SignedUser.isSigned())
                return 'Авторизируйтесь, чтобы добавить в избранные треки';

            if (track && track.favourite) {
                return 'Убрать из избранного'
            } else {
                return 'Добавить в избранное'
            }
        },
        favouriteTrack: function(track) {
            if (SignedUser.isSigned()) {
                this.resource.postFavourite(
                    {'track_id': track.id}
                ).
                $promise.then(function(data) {
                    track.count_favourites = parseInt(data.count_favourites, 10);
                    track.favourite        = parseInt(data.favourite, 10);
                });
            }
        }
    };
    return service;
}]);