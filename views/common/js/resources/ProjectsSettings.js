module.factory('ProjectsSettings', function($resource, $q, Projects) {
    var service = {
        DEFAULT_PROJECTS_SETTINGS_ID: -1,
        resource: $resource('api/projects_settings/:projectId', {}, {
            getDefault: {
                method: 'GET',
                url: 'api/projects_settings/default'
            },
            save: {
                method: 'PUT',
                url: 'api/projects_settings/:projectId'
            },
            create: {
                method: 'POST',
                url: 'api/projects_settings/0'
            }
        }),
        getUserProjectSettings: function(userId){
            var deferred = $q.defer();
            // Тупо берем первый его проект, пока нельзя определить, в каком он сейчас проекте
            Projects.query({user: userId}, function(response){
                if(response.length){
                    var project = response[0];
                    // Грузим настройки для проекта
                    service.resource.get({projectId: project.id}, function(response){
                        deferred.resolve([project, response]);
                    }, deferred.reject);
                }
                else{
                    deferred.reject();
                }
            });
            return deferred.promise;
        }
    }
	return service;
});