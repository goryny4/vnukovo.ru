module.factory('Playlists', function ($resource) {
	'use strict';
	return $resource('api/playlists/:id', {}, {
		update: { method:'PUT' },
		getPage: {
			url: 'api/playlists?user=:userId&page=:pageNumber&order_field=:orderField&order_dir=:orderDir',
			params: {objectType: '@userId', objectId: '@pageNumber', 'orderField': '@orderField', 'orderDir': '@orderDir'},
			method: 'GET'
		},
		getTorrent: {
			url: 'api/playlists/download/:id',
			params: {id: '@id'}
		}
	});
});