module.factory('LikesResource', ['$resource', 'SignedUser', function($resource, SignedUser) {
    var service = {
        resource: $resource(null, null,
            {
                'postLike': {
                    method:'POST',
                    url: '/api/tracklikes/like'
                },
                'getLikes': {
                    method:'GET',
                    url: '/api/tracklikes/likes'
                },
                'getPeopleByTrack': {
                    method:'GET',
                    url: '/api/tracklikes/peoplebytrack',
                    isArray: true
                }
            }),
        getTitle: function(track) {
            if (!SignedUser.isSigned())
                return 'Авторизируйтесь, чтобы лайкнуть трек';
            if (!SignedUser.isEmailChecked()) {
                return 'Сначала нужно пройти верификацию емейла';
            }


            if (track && track.liked) {
                return 'Убрать лайк'
            } else {
                return 'Лайкнуть'
            }
        },
        likeTrack: function(track, scope, promoId) {
            var self = this;
            if (SignedUser.isEmailChecked() && SignedUser.isSigned()) {
                this.resource.postLike(
                    { 'track_id': track.id, promo_id: promoId }
                ).
                $promise.then(function(data) {
                    track.count_liked = parseInt(data.count_liked, 10);
                    track.liked       = parseInt(data.liked, 10);
                    if (scope) {
                        self.resource.getLikes({track_id: track.id, type_id: 0}).$promise.then(function(likes) {
                            scope.userTypes = likes.userTypes;
                            scope.likes = likes.data;
                        });
                    }

                });
            }
        },
        peopleByTrack: function(track_id) {
            return this.resource.getPeopleByTrack({'track_id': track_id});
        }
    };
    return service;
}]);