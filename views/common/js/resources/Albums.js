module.factory('Albums', ['$resource', '$window', 'toaster', function($resource, $window, toaster) {
    var service = {
        resource: $resource('api/albums/:id', {id: '@id'}, {
            save: {
                method: 'PUT',
                url: 'api/albums/:id',
                params: {id: '@id'}
            },
            create: {
                method: 'POST',
                url: 'api/albums/0'
            },
            delete: {
                method: 'DELETE',
                url: 'api/albums?ids=:ids'
            },
            getInfo: {
                method: 'GET',
                url: 'api/albums/:id/info',
                params: {id: '@id'}
            },
            query: {
                method: 'GET',
                url: 'api/albums',
                isArray: true
            },
            queryPage: {
                method: 'GET',
                url: 'api/albums?page=:page&page_size=:pageSize'
            },
            getWithMetaData: {
                method: 'GET',
                url: 'api/albums/:id/addMeta',
                params: {id: '@id'}
            },
            queryByIds: {
                method: 'GET',
                url: 'api/albums?id=:ids',
                isArray: true
            }
        }),
        getDefaults: function(){
            return new this.resource({
                name: "Новый альбом",
                descr: "Здесь будет описание",
                short_descr: "Здесь будет краткое описание",
                date: moment().format(),
                hidden: 0,
                userOrder: 0,
                tracks: 0,
                size: 0,
                sizeStr: '0.0 Mb',
                timelengthStr: '0.0.0',
                cover: '/main/img/cover2.jpg',
                cover_settings: angular.toJson({
                    "image": {
                        "width": 164,
                        "height": 164
                    },
                    "actualRect": {
                        "x": 0,
                        "y": 0,
                        "width": 164,
                        "height": 164
                    }
                })
            });
        },
        processInfo: function(album, info){
            var maxStylesLength = 80,
                stylesNames = [];
            album.styles = info.styles;
            for(var i=0; i<info.styles.length; i++){
                stylesNames.push(info.styles[i].value);
            }
            stylesNames = stylesNames.join(', ')
            if(stylesNames.length >= maxStylesLength){
                // Переводим стили в строку, обрезаем до макс длины, преобразуем обратно в массив, получаем кол-во элементов
                album.stylesShowCnt = stylesNames.slice(0, maxStylesLength).replace(/,\s[^,]*$/, '').split(', ').length;
            }
            else{
                album.stylesShowCnt = info.styles.length;
            }
            album.sizeStr = parseFloat(info.size / 1048576).toFixed(1) + ' Mb';
            album.timelengthStr = moment().startOf('day').add('s', info.timelength).format("H.m.s");
        },
        toggleHidden: function(album){
            album.hidden = +!+album.hidden;
            var def = album.$save();
            def.then(function () {
                toaster.pop('success', "Действие успешно выполнено", "Видимость альбома успешно изменена");
            }, function () {
                toaster.pop('error', "Произошла ошибка", "Не удалось изменить видимость альбома");
            });
            return def;
        },
        delete: function(album){
            if($window.confirm("Удалить альбом " + album.name + "?")){
                return album.$delete({ids: album.id});
            }
            return false;
        }
    }
	return service;
}]);