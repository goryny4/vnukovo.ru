module.factory('StylesResource', function($resource) {
	'use strict';
	return $resource('api/styles/:id');
});
