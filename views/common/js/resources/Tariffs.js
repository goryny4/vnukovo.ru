
module.factory('TariffsResource', function ($resource) {
    'use strict';
    return $resource('api/tariffs/:id', {}, {

        queryPage: {
            method: 'GET',
            url:    'api/tariffs'
        },
        subscribeTo: {
            method: 'POST',
            url:    'api/tariff/subscribe'
        },
        extend: {
            method: 'POST',
            url: 'api/tariff/extend'
        },
        change: {
            method: 'POST',
            url: 'api/tariff/change'
        },
        unSubscribe: {
            method: 'POST',
            url: 'api/tariff/unSubscribe'
        }
    });
})
