module.factory('TracksResource', function ($resource) {
	'use strict';

	return $resource('api/tracks/:id', {}, {
        getWithMetaData: {
            method: 'GET',
            url: 'api/tracks/:id/addMeta'
        },
		queryBest: {
			method: 'GET',
			url: 'api/tracks/best',
			isArray: true
		},
		queryLatest: {
			method: 'GET',
			url: 'api/tracks/latest',
			isArray: true
		},
		queryPromo: {
			method: 'GET',
			url: 'api/tracks/promo',
			isArray: true
		},
		queryCount: {
			method: 'POST',
			url: 'api/tracks/count',
			isArray: false
		},
        addPersonalPromoTrack: {
            method: 'POST',
            url: 'api/personal_promo/add/:id'
        },
        removePersonalPromoTrack: {
            method: 'DELETE',
            url: 'api/personal_promo/remove'
        },
        swapPersonalPromoTracks: {
            method: 'POST',
            url: 'api/personal_promo/swap'
        },
        getPromoStatistic: {
            method: 'GET',
            url: 'api/tracks/promoStatistic',
            isArray: true
        },
        query: {
            method: 'GET',
            url: 'api/tracks',
            isArray: true
        },
        queryUsersBest: {
            method: 'GET',
            url: 'api/tracks/usersbest',
            isArray: true
        },
        queryWithMetaData: {
            method: 'GET',
            url: 'api/tracks/withMeta'
        },
        queryByIds: {
			method: 'GET',
			url: 'api/tracks?id=:ids',
			isArray: true
		},
		queryByAlbum: {
			method: 'GET',
			url: 'api/tracks?album_id=:albumId',
			isArray: true
		},
		getWave: {
			method: 'GET',
			url: '_files/waves/:id.json',
			responseType: 'json',
			cache: true,
			transformResponse: function (data) {
				return {points: angular.fromJson(data)};
			}
		},
		getRadio: {
			method: 'GET',
			url: 'radio/0',
			isArray: true
		}
	});
});

module.factory('TracksResourceL', function ($resource) {
    'use strict';
    return $resource('api/tracks/:id', {}, {
        queryByAlbum: {
			method: 'GET',
			url: 'api/tracks?album_id=:albumId',
			isArray: true
        }
    });
});

module.constant('CountForm', {
    0: '{} раз',
    one: '{} раз',
    few: '{} раза',
    many: '{} раз',
    other: '{} раз'
});