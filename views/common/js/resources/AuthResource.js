
module.factory('AuthResource', function ($resource) {
    'use strict';
    return $resource('', {}, {
        check_email: {
            method: 'GET',
            url:    'api/auth/check_email'
        },
        check_login: {
            method: 'GET',
            url:    'api/auth/check_login'
        },
        register: {
            method: 'POST',
            url:    'api/auth/register'
        },
        is_user_in_cjclub: {
            method: 'GET',
            url: 'api/auth/isCjclub'
        },

        sendMailRequest: {
            method: 'GET',
            url: 'api/auth/sendMailRequest'
        }
    });
});