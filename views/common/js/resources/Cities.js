module.factory('CitiesResource', function($resource) {

    return $resource('/api/cities/:id', {}, {

        queryPage: {
            method: 'GET',
            url: 'api/cities',
            isArray: true
        }
    });
});

