module.factory('Projects', function($resource) {

    return $resource('/api/projects/:id', {}, {
        getStyles: {
            method: 'GET',
            url: 'api/projects/:id/styles',
            params: {'id': '@id'},
            isArray: true
        },

        queryPage: {
            method: 'GET',
            url: 'api/projects',
            isArray: true
        },

        queryPromo: {
            method: 'GET',
            url: 'api/projects/promo',
            isArray: true
        },

        getUserProjectsByName: {
            method: 'GET',
            url: 'api/projects/:name',
            isArray: true
        },

        postCheckName: {
            method: 'POST',
            url: 'api/projects/checkName'
        },

        postCreateProject: {
            method: 'POST',
            url: 'api/projects/create'
        },

        queryCount: {
            method: 'POST',
            url: 'api/projects/count',
            isArray: false
        }
    });
});

