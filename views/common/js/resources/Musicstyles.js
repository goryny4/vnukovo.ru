module.factory('MysicstylesResource', function($resource) {
	'use strict';
	return $resource('api/musicstyles/:id', {}, {
        queryTop: {
            method: 'GET',
            url: 'api/musicstyles/top',
            isArray: true
        }
    });
});
