
module.controller('ReportCtrl', function(ReportResource, $scope, $timeout, SignedUser){
    $scope.report_data = {
    	category: null,
    	email: SignedUser.user.email,
    	text: null,
    	captcha: null
    };

    $scope.status = false;
    $scope.rnd = Math.random(); // Для каптчи
    $scope.submit_disabled = false;

    $scope.refreshCaptcha = function(report_data){
    	$scope.rnd = Math.random();
    	report_data.captcha = '';
    };

    $scope.regenerateCaptcha = function() {
        $scope.rnd = Math.random();
    };

    $scope.sendReport = function(report_data){
    	var valid = $scope.report_form.$valid;
    	if (valid){
            $scope.submit_disabled = true;

    		ReportResource.send(report_data).$promise.then(function(data) {
                var status = data.status;
                $scope.status = status;

                if(status){
                    $scope.regenerateCaptcha();
                }

                if(status == 'success') {
                    $scope.report_data = {
                        category: null,
                        email: null,
                        text: null,
                        captcha: null
                    };
                    $scope.report_form.$setPristine(true);
                }

                $timeout(function() {
                    $scope.status = false;                    
                }, 2000);

                $scope.submit_disabled = false;
            });
    	}
    };
  

});