module.controller('RootCtrl', function($scope, $rootScope, AuthResource, SignedUser, $location, $timeout, jplayerInterface) {
	'use strict';
	$rootScope.player = jplayerInterface;
    $rootScope.$watch(function(){ return SignedUser.user }, function(user){
        $scope.user = user;
    });

    $rootScope.sendMailRequest = function(){
        AuthResource.sendMailRequest().$promise
            .then(function(){
                $rootScope.__success = true;

                $timeout(function(){
                    $rootScope.__success = false;
                }, 3000);

            });
    };

    $rootScope.littleNotification = function(){
        return $location.path() == '/addproject' || $location.path() == '/signup' || $location.path() == '/end_registration';
    };

});
/*
 * установкой свойств этого сервиса выставляются статусные и ошибочные сообщения
 */
module.constant('Notifications', {
	error:  '',
	status: ''
});
/*
 * контроллер создает $scope где видны свойства сервиса Notifications
 */
module.controller('NotificationCtrl', function ($scope, Notifications, $timeout) {
	'use strict';
	$scope.Notifications = Notifications;
	$scope.$watch('Notifications.status', function () {
		$timeout(function () {
			$scope.Notifications.status = '';
		}, 3000);
	});
	$scope.$watch('Notifications.error', function () {
		$timeout(function () {
			$scope.Notifications.error = '';
		}, 3000);
	});
});