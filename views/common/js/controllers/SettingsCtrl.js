module.controller('SettingsCtrl',
    ['$rootScope', '$scope', '$location', 'ProjectsSettings', 'SignedUser','Projects', 'toaster', 
    function ($rootScope, $scope, $location, ProjectsSettings, SignedUser, Projects, toaster) {
        'use strict';
        $scope.loading = true;
        $scope.projectsSettings = null;
        $scope.project = {};
        // Если юзер не залогинился, редиректим
        if(!SignedUser.isSigned()){
            $location.url('/');
        }
        else{
            ProjectsSettings.getUserProjectSettings(SignedUser.user.id).then(function(args){
                $scope.project = args[0];
                $scope.projectsSettings = args[1];
            }).finally(function(){
                $scope.loading = false;
            });
        }
        $scope.save = function(){
            var def = null;
            if(! $scope.projectsSettings){
                return;
            }
            if(! $scope.projectsSettings.project){
                $scope.projectsSettings.project_id = $scope.project.id;
                def = $scope.projectsSettings.$create();
            }
            else{
                def = $scope.projectsSettings.$save({projectId: $scope.projectsSettings.project_id});
            }
            def.then(function () {
                toaster.pop('success', "Действие успешно выполнено", "Настройки успешно сохранены");
                $scope.projectsSettings.isDefault = false;
				$rootScope.$broadcast('updateProjectsSettingsEvent');
            }, function () {
                toaster.pop('error', "Произошла ошибка", "Не удалось сохранить настройки");
            })
        }
    }]
);