module.controller('AlbumsCtrl',
    ['$scope', 'Albums', function ($scope, Albums) {
        'use strict';
        function getAlbums(reload){
            $scope.loading = true;
            Albums.resource.queryPage({
                page: $scope.page,
                pageSize: 5,
                filtering: 'with_tracks',
                ordering: $scope.sortDirection + $scope.sortType
            }, function(response){
                if(angular.isArray(response.data) && response.data.length){
                    if(reload){
                        $scope.albums = response.data;
                    }
                    else{
                        $scope.albums = $scope.albums.concat(response.data);
                    }
                    $scope.loading = false;
                }
            });
        }
        var sortTypes = {
                name: 'name',
                date: 'date'
            },
            sortDirections = {
                asc: '',
                desc: '-'
            },
            sorting = {
                'name': sortDirections.desc,
                'date': sortDirections.desc
            };
        $scope.loading = false;
        $scope.page = 1;
        $scope.albums = [];
        $scope.sortTypes = sortTypes;
        $scope.sortType = sortTypes.date;
        $scope.sortDirection = sortDirections.desc;
        getAlbums();
        $scope.sort = function (type){
            $scope.sortType = type;
            sorting[type] = (sorting[type] == sortDirections.asc)
                ? sortDirections.desc : sortDirections.asc;
            $scope.sortDirection = sorting[type];
            getAlbums(true);
        }
        $scope.isAsc = function() {
            return $scope.sortDirection === sortDirections.asc;
        }
        $scope.loadPage = function(){
            if ($scope.loading) {
                return;
            }
            getAlbums();
            $scope.page++;
        }
    }]
);