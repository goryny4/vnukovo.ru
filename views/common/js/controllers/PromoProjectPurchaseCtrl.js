module.controller('PromoProjectPurchaseCtrl', function($scope, ServicesResource, Projects, SignedUser, $routeParams, RubForm){
    'use strict';

    $scope.canAddToPromo = false;
    $scope.daysEntered = false;
    $scope.now = new Date();
    $scope.RubForm = RubForm;

    ServicesResource.get({ id: $routeParams.id }).$promise
        .then(function( service){
            $scope.service = service;
            $scope.service.price = $scope.service.prices[0].price;
            $scope.period = Math.floor( SignedUser.user.balance / $scope.service.price );
        });

    $scope.$watch('period', function(nw){
        $scope.daysEntered = !isNaN(nw);
    });

    $scope.getCalc = function(){
        return $scope.calc;
    };

    $scope.projectIdChanged = function(nw){
        $scope.getproject(nw);
        $scope.data = false;
    };
    $scope.projectId = $routeParams.param;
    $scope.getproject = function( id){
        if(id == '') return;
        if(isNaN(id))
            Projects.getUserProjectsByName({ name: id }).$promise
                .then(function(res){
                    $scope.project = res;
                },function(){
                    $scope.project = {};
                });
        else
            Projects.get({ id: id }).$promise
                .then(function(res){
                    $scope.project = res;
                },function(){
                    $scope.project = {};
                });
    };

    $scope.pick = function(project){
        $scope.projectId = project.id;
        $scope.projectIdChanged(project.id);
    };

    $scope.loading = false;

    $scope.submit = function( period){
        $scope.loading = true;
        ServicesResource.subscribeTo({ id: $routeParams.id, period: 1, periodMoment: period, "additionalParams[]": [ $scope.project.id, period ] }).$promise
            .then(function( data){
                $scope.loading = false;
                $scope.data = data;
                if($scope.data.to){
                    $scope.data.to = new Date($scope.data.to);
                }
                if($scope.data.from){
                    $scope.data.from = new Date($scope.data.from);
                }
                if(!$scope.data.msg){
                    SignedUser.user.balance -= $scope.service.price * period;
                }
            });
    };

});