module.controller('TariffCtrl', function( $scope, TariffsResource, Projects, $location, $routeParams, $http, RubForm, MonthForm, ProjectForm, SignedUser, $sce, $timeout, $filter){

    $scope.loaded = false;
    $scope.rubForm = RubForm;
    $scope.MonthForm = MonthForm;
    $scope.ProjectForm = ProjectForm;
    $scope.user = SignedUser.user;
    $scope.purchasedPrice = 0;
    $scope.period = 1;

    //get user projects
    Projects.queryPage({ user: true }).$promise
        .then(function( response){
            $scope.projects = response;

            TariffsResource.queryPage({}).$promise
                .then(function(response){
                    angular.forEach(response.data, function(tariff){
                        if(tariff.users[0] && tariff.users[0].pivot)
                            $scope.subscribedOn = tariff;
                        if(tariff.id == $routeParams.id)
                            $scope.tariff = tariff;

                        if(tariff.projectCount > $scope.projects.length && !$scope.fit){
                            $scope.fit = tariff;
                        }
                    });

                });
        });

    $scope.redirect = function( path){
        $location.path( path);
    };

    // Подписан на текущий тариф
    $scope.isSubscribed = function(){
        if(!$scope.subscribedOn || !$scope.tariff) return false;
        return ($scope.subscribedOn.id == $scope.tariff.id) || $scope.subscribed;
    };

    // Подписан на большее кол-во проектов
    $scope.isLessThenOnSubscribed = function(){
        return $scope.subscribedOn && $scope.tariff && $scope.subscribedOn.projectCount > $scope.tariff.projectCount;
    };

    // У вас много проектов
    $scope.isLessThenUserHasProjects = function(){
        return $scope.tariff && $scope.projects && $scope.tariff.projectCount < $scope.projects.length;
    };

    $scope.couldChange = function(){
        return !$scope.isSubscribed() && $scope.tariff.id && !$scope.isLessThenOnSubscribed() && !$scope.isLessThenUserHasProjects() && $scope.subscribedOn;
    };
    $scope.couldTry = function(){
        return !$scope.isSubscribed() && $scope.tariff.id  && !$scope.couldChange() && !$scope.justPurchase;
    };

    $scope.periodChanged = function(){
        $scope.period = this.period;
    };

    $scope.renderHtml = function( html){
        return $sce.trustAsHtml($filter('wikitext')( html));
    };

    $scope.purchase = function(tariff, period){
        $scope.loading = true;
        TariffsResource.subscribeTo({ id: tariff.id, period: (period? $scope.period : 0) }).$promise
            .then(function(response){
                $scope.response = response;
                $scope.loading = false;
                if(!response.error){
                    $scope.subscribed = true;
                    $scope.justPurchase = true;
                    $scope.tariff.users = [{pivot: {}}];
                    $scope.tariff.users[0].pivot.paidTill = response.paidTill.date;
                    $scope.user.balance -= response.moneyAmount;
                    $scope.purchasedPrice = response.moneyAmount;
                }
            }, function(err){
                $scope.response = { error: true, msg: 'Произошла ошибка, попробуйте позже.'};
                $scope.loading = false;
            });
    };

    $scope.unSubscribe = function(tariff){
        $scope.loading = true;
        TariffsResource.unSubscribe({ id: tariff.id, price: $scope.purchasedPrice }).$promise
            .then(function(response){
                $scope.response = { msg: 'Вы успешно отписались' };
                $scope.loading = false;
                $scope.subscribed = false;
                $scope.tariff.users = [];
                $scope.justPurchase = false;
                $scope.user.balance += $scope.purchasedPrice;
                $scope.purchasedPrice = 0;
            }, function(err){
                $scope.response = { error: true, msg: 'Произошла ошибка, попробуйте позже.'};
                $scope.loading = false;
            });
    };

    $scope.extend = function(tariff){
        $scope.loading = true;
        TariffsResource.extend({ id: tariff.id, period: $scope.period }).$promise
            .then(function(response){
                $scope.response = response;
                $scope.loading = false;
                if(!response.error){
                    $scope.purchasedPrice += response.moneyAmount;
                    $scope.user.balance -= response.moneyAmount;
                    $scope.tariff.users = [{pivot: {}}];
                    $scope.tariff.users[0].pivot.paidTill = response.paidTill.date;
                }
            }, function(err){
                $scope.response = { error: true, msg: 'Произошла ошибка, попробуйте позже.' };
                $scope.loading = false;
            });
    };

    $scope.change = function(tariff){
        $scope.loading = true;
        TariffsResource.change({ id: tariff.id, period: $scope.period }).$promise
            .then(function(response){
                $scope.response = response;
                $scope.loading = false;
                if(!response.error){
                    $scope.subscribed = true;
                    $scope.changed = true;
                    $scope.tariff.users = [{pivot: {}}];
                    $scope.tariff.users[0].pivot.paidTill = response.paidTill.date;
                    $scope.user.balance -= response.moneyAmount;
                    $scope.purchasedPrice = response.moneyAmount;
                }
            }, function(err){
                $scope.response = { error: true, msg: 'Произошла ошибка, попробуйте позже.'};
                $scope.loading = false;
            });
    };

    $scope.$watch('response', function(){
        $timeout(function(){
            $scope.response = false;
        }, 6000);
    });

});