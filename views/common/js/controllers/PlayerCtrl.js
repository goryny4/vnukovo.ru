module.controller('PlayerCtrl', function ($scope, $interval, jplayerInterface) {
	'use strict';

	$scope.playInfo = jplayerInterface.info;

	$scope.toggleMute = function () {
		jplayerInterface.toggleMute();
	};
	$scope.togglePlay = function () {
		jplayerInterface.togglePlay();
	};
	$scope.isPlaying = function () {
		return jplayerInterface.isPlaying();
	};
	$scope.seekAndPlay = function (e, width) {
		if (jplayerInterface.isRadio()) {
			return;
		}

		var x, percents, seekTo;

		if (e.offsetX === undefined)	// this works for Firefox
		{
			x = e.pageX - $(e.target).offset().left;
		}
		else													// works in Google Chrome
		{
			x = e.offsetX;
		}

		percents = x / width;
		seekTo = Math.round($scope.track.timelength * percents);
		jplayerInterface.play(seekTo);
	};
});
