module.controller('TrackCtrl',
	['$scope', 'Track', 'jplayerInterface', '$window', 'headerState', 'FavouritesResource', 'LikesResource', 'TracksResource', 'promoTracks', 'SignedUser',
		function ($scope, Track, jplayerInterface, $window, headerState, FavouritesResource, LikesResource, TracksResource, promoTracks, SignedUser) {
			'use strict';

			var
				track = $('div.track-wave'),
				setWavePosition = function(){
					var
						isOpen = headerState.isOpen(),
						top = document.documentElement.scrollTop || document.body.scrollTop,
						scroll = isOpen ? 352 : 388;
					$scope.scrollOffset = (isOpen ? 160 : 55) + 180;
					if (top > scroll) {
						if (!isOpen) {
							track.addClass('closed-header');
						} else {
							track.removeClass('closed-header');
						}
						track.addClass('fixed-track');
					} else {
						track.removeClass('fixed-track');
						track.removeClass('closed-header');
					}
				};

            $scope.user = SignedUser.user;
            $scope.show_message = "Показать всех";

			$scope.track = Track;
			$scope.isBannerCodeShown = false;
			$scope.showBannerCode = function () {
				$scope.isBannerCodeShown = true;
			};
			$scope.player = jplayerInterface;
			$scope.playInfo = jplayerInterface.info;

            //--в избранное
            $scope.FavouritesResource = FavouritesResource;


            //избранное

            //--лайки

            $scope.LikesResource = LikesResource;

            $scope.getLikes = function(type) {
                var type = type || 0;
                LikesResource.resource.getLikes({track_id: Track.id, type_id: type}).$promise.then(function(likes) {
                    $scope.userTypes = likes.userTypes;
                    $scope.likes = likes.data;
                });
            };

            $scope.$watch(function(){ return promoTracks.urPromo }, function(v){
                $scope.urPromo = v;
            });

            $scope.likes_limit = 12;
            $scope.changeLimit = function() {
                if ($scope.likes_limit == 12)
                {
                    $scope.likes_limit = 30;
                    $scope.show_message = "Скрыть всех";
                    return true;
                }
                $scope.likes_limit = 12;
                $scope.show_message = "Показать всех";
                return true;
            };

            $scope.likesCount = function(type) {
                if (!$scope.userTypes || !$scope.userTypes[type]) {
                    return 0;
                }
                return $scope.userTypes[type].count;
            };

            $scope.tabLikes = function(type) {
                $scope.getLikes(type);
            };

            $scope.getLikes();

            $scope.like = function(track) {
                $scope.LikesResource.likeTrack(track, $scope);
            }

			$($window).on('scroll', function () {
				$scope.safeApply(function () {
					setWavePosition();
				});
			});

			$scope.$on('header.stateChanged', setWavePosition);
		}]
);