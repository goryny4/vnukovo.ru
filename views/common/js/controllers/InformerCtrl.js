module.controller('InformerCtrl', function($scope, Informer) {
	$scope.data = Informer;
	$scope.isInProgress = function() {
		return Informer.busy;
	};
});