module.controller('ProjectTracksCtrl', function ($scope, Project, Projects, StylesResource, TracksResource, FavouritesResource, LikesResource, SignedUser, promoTracks, $animate, $timeout, headerState) {
	'use strict';
	var
		sortTypes = {
			pit: 'pit',
			date: 'public_date'
		},
		sortDirections = {
			asc: '',
			desc: '-'
		},
		sorting = {
			'pit': sortDirections.desc,
			'public_date': sortDirections.desc
		},
		tags = $('#tags'),
		uninterpolate = function (a, b) {
			b = b - (a = +a) ? 1 / (b - a) : 0;
			return function (x) {
				return (x - a) * b;
			};
		},
		interpolate = function (a, b) {
			b -= a = +a;
			return function (t) {
				return a + b * t;
			};
		},
		getFiltering = function(){
			var filtering = [];
			// Без альбома
			if($scope.isOwner && $scope.filter.noAlbum){
				filtering.push('no_album');
			}
			// Период
			if ($scope.filter.period2weeks) {
				filtering.push('period_2_weeks');
			}
			return filtering.join(',');
		};

	$scope.LikesResource = LikesResource;
	$scope.project = Project;
	$scope.user = SignedUser.user;
	$scope.isOwner = SignedUser.isSigned() && SignedUser.user.id == Project.creatorId;

	$scope.template = 'track_normal.html';
	$scope.page = 1;
	$scope.tracks = [];
	$scope.total = 0;
	$scope.infiniteScrollDisabled = true;
	$scope.filter = {
		noAlbum: false,
		period2weeks: false	
	}
	$scope.loading = false;
	// Сортировка
	$scope.sortTypes = sortTypes;
	$scope.sortType = '';
	$scope.sortDirection = '';
	// Вынесена в отдельную функцию, чтобы менять напавление сортировки каждый раз
	$scope.sort = function (type){
		$scope.sortType = type;
		sorting[type] = (sorting[type] == sortDirections.asc)
			? sortDirections.desc : sortDirections.asc;
		$scope.sortDirection = sorting[type];
		$scope.loadPage(true);
	}
	$scope.isSortAsc = function(){
		return $scope.sortDirection === sortDirections.asc;
	};
	// Стили
	$scope.styles = [];
	Projects.getStyles({id: $scope.project.id}, function(styles){
		var
			minFont = 12,
			maxFont = 30,
			minCount, maxCount, u, i;
		$scope.styles = styles;
		if(styles.length > 1){
			minCount = _.min($scope.styles, function (style) {
				return +style.tracks;
			}).tracks;
			maxCount = _.max($scope.styles, function (style) {
				return +style.tracks;
			}).tracks;
			u = uninterpolate(minCount, maxCount);
			i = interpolate(minFont, maxFont);
			$scope.styleFont = function (count) {
				return Math.floor(i(u(+count)));
			};
		}
	});

	$scope.selectStyle = function (style) {
		$scope.selectedStyle = style;
		$scope.toggleStyles();
		$scope.loadPage(true);
	};

	$scope.toggleStyles = function () {
		$scope.isStylesOpened = !$scope.isStylesOpened;
		tags.slideToggle(300);
	};
	$scope.isSortable = function(){
		// Если включена какая то сортировка или фильтр, то менять порядок нельзя
		return $scope.isOwner && !($scope.sortType || getFiltering());
	};
	if ($scope.isOwner) {
		$scope.sortableOptions = {
			stop: function (e, ui) {
				if(!$scope.isSortable()){
					return;
				}
				for (var i = 0; i < $scope.tracks.length; i++) {
					if ($scope.tracks[i].userOrder != i) {
						$scope.tracks[i].userOrder = i;

						var tagTrackUpdateError = false;
						$scope.tracks[i].$save(function () {}, function () {
							if (!tagTrackUpdateError) {
								toaster.pop('error', "Произошла ошибка", "Не удалось изменить порядок треков");
								tagTrackUpdateError = true;
							}

						});
					}
				}
			}
		};
	} else {
		$scope.sortableOptions = {
			disabled: true
		};
	}
	// Загрузка страницы
	$scope.loadPage = function(reload){
		if ($scope.loading) {
			return;
		}
		// Если нужно заменить данные, то это 1 страница
		if(reload){
			$scope.page = 1;
		}
		// Параметры запроса
		var params = {
				page: $scope.page,
				page_size: 10,
				projectId: $scope.project.id,
				ordering: $scope.sortDirection + $scope.sortType,
				filtering: getFiltering()
			};
		// Стиль
		if ($scope.selectedStyle) {
			params.style = $scope.selectedStyle.id;
		}
		$scope.loading = true;
		$scope.infiniteScrollDisabled = true;
		// Берем треки с метаданными
		TracksResource.queryWithMetaData(params,function(response){
			if(response.tracks && angular.isArray(response.tracks.data)){
				$("meta[name='keywords']").attr('content',response.meta.keywords);
				$("meta[name='description']").attr('content',response.meta.description);
				$("title").html(response.meta.title);
				var tracks = response.tracks.data;
				angular.forEach(tracks, function(track, i){
					tracks[i] = new TracksResource(track);
				});
				// Если надо заменить треки
				if(reload){
					$scope.tracks = tracks;
				}
				else{
					$scope.tracks = $scope.tracks.concat(tracks);
				}
				$scope.total = response.tracks.total;
				$scope.loading = false;
				$scope.infiniteScrollDisabled = !$scope.total || (response.tracks.current_page == response.tracks.last_page);
			}
		});
		$scope.page++;
	}

	$scope.$watchCollection('filter', function () {
		$scope.loadPage(true);
	});
});

