module.controller('PromoPurchaseCtrl', function($scope, ServicesResource, TracksResource, SignedUser, $routeParams, RubForm){
    'use strict';

    $scope.canAddToPromo = false;
    $scope.daysEntered = false;
    $scope.now = new Date();
    $scope.RubForm = RubForm;

    $scope.service = ServicesResource.get({ id: $routeParams.id }).$promise
        .then(function( service){
            $scope.service = service;
            $scope.service.price = $scope.service.prices[0].price;
            $scope.period = Math.floor( SignedUser.user.balance / $scope.service.price );
        });

    $scope.$watch('period', function(nw){
        $scope.daysEntered = !isNaN(nw);
    });

    $scope.getCalc = function(){
        return $scope.calc;
    };

    $scope.trackIdChanged = function(nw){
        if(!isNaN(+nw)){
            $scope.getTrack(nw);
            $scope.data = false;
        }
        else{
            if(!nw) return;
            var sp = nw.split('/');
            if(sp[sp.length - 2] == 'tracks' && typeof +sp[sp.length - 1] == 'number'){
                $scope.getTrack( sp[sp.length - 1]);
                $scope.data = false;
            }
        }
    };
    $scope.getTrack = function( id){
        TracksResource.get({ id: id }).$promise
            .then(function(res){
                $scope.track = res;
                $scope.track.label = $scope.track.label == 'true';
            },function(){
                $scope.track = {};
            });
    };

    $scope.trackId = $routeParams.param;
    if($scope.trackId) $scope.getTrack($scope.trackId);

    $scope.loading = false;

    $scope.submit = function( period){
        $scope.loading = true;
        ServicesResource.subscribeTo({ id: $routeParams.id, period: 1, periodMoment: period, "additionalParams[]": [ $scope.track.id, period ] }).$promise
            .then(function( data){
                $scope.loading = false;
                $scope.data = data;
                if($scope.data.to){
                    $scope.data.to = new Date($scope.data.to);
                }
                if($scope.data.from){
                    $scope.data.from = new Date($scope.data.from);
                }
                if(!$scope.data.msg){
                    SignedUser.user.balance -= $scope.service.price * period;
                }
            });
    };

});