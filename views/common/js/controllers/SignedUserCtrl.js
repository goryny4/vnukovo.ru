module.controller('SignedUserCtrl', function($scope, SignedUser, $http, headerState, $window, $route) {
	$scope.isSigned = SignedUser.isSigned;

    $scope.$watch(function(){ return SignedUser.user; }, function(user){
        $scope.user = user;
    });

	$scope.model = {
		login: '',
		password: '',
		remember: false
	};
	$scope.sendForm = function() {
		$http.put('api/auth', $scope.model)
			.success(function(userData) {
				$scope.user = SignedUser.user = userData;
                $route.reload();
			})
			.error(function(msg) {
				alert('Ошибка: ' + msg);
			});
	};
	$scope.logout = function() {
		$http.delete('api/auth')
			.success(function() {
                var states = headerState.states;
                SignedUser.user = null;
                console.log(SignedUser.user);
                $route.reload();
                headerState.setState(states.opened);
            });
	};
});