module.controller('ControlCenterCtrl', ['$scope', 'SignedUser', 'ServicesResource', 'TracksResource', 'RubForm', 'CountForm', '$location', function ($scope, SignedUser, ServicesResource, TracksResource, RubForm, CountForm, $location) {
    'use strict';
    $scope.user = SignedUser.user;
    $scope.rubForm = RubForm;

    if(!$scope.user.login) $location.path('/');

    ServicesResource.queryPage({})
        .$promise.then(function( response){
            $scope.services = response.data;
            $scope.columns = [];
            var columns = Math.ceil($scope.services.length/4);
            for(var i = 0; i<columns;i++){
                $scope.columns.push(i*4);
            }
        });
}]);