module.controller('DemoCtrl', function($scope) {
	$scope.log = [];
	$scope.onStart = function() {
		$scope.log.push('Загрузка началась');
	};
	$scope.onProgress = function(progress) {
		$scope.log.push('Загружено ' + progress + '%');
	};
	$scope.onComplete = function() {
		$scope.log.push('Загрузка завершена');
	};
	$scope.onError = function(error) {
		$scope.log.push('Ошибка загрузки: ' + error);
	};

	$scope.checkFile = function(file, info) {
		$scope.log.push('Проверка файла перед отправкой: ');
		return file;
	};
});