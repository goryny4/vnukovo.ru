
module.controller('ServicesCtrl', function($scope, ServicesResource, $location, $http, TariffsResource, RubForm, SignedUser){

    $scope.page         = 1;
    $scope.services     = [];
    $scope.complete     = false;
    $scope.loading      = false;
    $scope.rubForm      = RubForm;
    $scope.user = SignedUser.user;
    $scope.User = SignedUser;
    $scope.activeServices = [];

    ServicesResource.queryActive()
        .$promise.then(function( response){
            angular.forEach(response, function(elem){
                if(elem.projects && elem.projects[0] && elem.projects[0].pivot)
                    elem.projects[0].pivot.paidTill = new Date(elem.projects[0].pivot.paidTill);
                $scope.activeServices.push(elem);
            });
            $scope.activeColumns = [];
            var activeColumns = Math.ceil($scope.activeServices.length/4);
            for(var i = 0; i<activeColumns;i++){
                $scope.activeColumns.push(i*4);
            }
        });

    $scope.loadPage = function(){

        if ($scope.loading) return;
        $scope.loading = true;
        $scope.complete = false;

        ServicesResource.queryPage({ page: $scope.page }).$promise
            .then(function( response){
                if(angular.isArray(response.data) && response.data.length){

                    $scope.services = $scope.services.concat(response.data);
                    $scope.columns = [];
                    var columns = Math.ceil($scope.services.length/4);
                    for(var i = 0; i<columns;i++){
                        $scope.columns.push(i*4);
                    }
                    if(response.data.length < 15){
                        $scope.complete = true;
                    }else{
                        $scope.loading = false;
                    }
                }
                else{
                    $scope.complete = true;
                }

            },
            function(error){
                console.log(error);
            });

        $scope.page++;
    };
});