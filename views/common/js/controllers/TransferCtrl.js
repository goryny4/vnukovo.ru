

module.controller('TransferCtrl', function($scope, UsersResource, $timeout, $sce, RubForm, SignedUser){

    // Текст поиска
    $scope.text  = '';
    $scope.field = 'name';
    // Номер страницы
    $scope.page  = 1;
    $scope.rubForm = RubForm;
    $scope.user = SignedUser.user;

    var timeouts = [];
    $scope.changeSearch   = function(){
        if($scope.text.length < 2){
            $scope.users = $scope.friends;
            return;
        }
        $scope.scrollDisable = false;
        $scope.dirty   = true;
        $scope.loading = true;
        $scope.page    = 1;

        // удалить все проки ng-change до этого момента.
        if(timeouts.length) angular.forEach(timeouts, function(to, i){ $timeout.cancel(to); timeouts.splice(i, 1); });
        timeouts.push($timeout(function(){

            // загрузить данные, если последнюю секунду запрос не изменился.
            UsersResource.querySearch({ page: $scope.page, "wheres[]": [$scope.field, "LIKE", $scope.text] })
                .$promise.then(function( users){
                    angular.forEach( users.data, function(user){
                        user.amount = 100;
                    });
                    $scope.users = users.data;
                    $scope.loading = false;
                });

            $scope.page++;
        }, 1000));
    };

    $scope.loadPage = function(){

        UsersResource.querySearch({ page: $scope.page, "wheres[]": [$scope.field, "LIKE", $scope.text]})
            .$promise.then(function( users){
                angular.forEach( users.data, function(user){
                    user.amount = 100;
                });
                $scope.users = $scope.users.concat(users.data);
                $scope.loading = false;
                $scope.scrollDisable = true;
            });
        $scope.page++;
    };

    $scope.setCurrent = function( user){
        $scope.current = user;
    };

    $scope.transfer = function( selected){
        // todo предупреждение, или алерт
        UsersResource.queryTransfer({ to: selected.id, amount: selected.amount, comment: selected.comment }).$promise
            .then(function(data){
                if(data.status){
                    $scope.balance -= selected.amount;
                }
                console.log(data)
            });
    };

    //todo доделать
    $scope.searchAndShow = function(sin, text){
        return $sce.trustAsHtml(sin.replace(new RegExp(text, "ig"), function(a){
            return "<div style='color: #ff9607 !important; display: inline;'>"+a+" </div>";
        }));
    };

});