module.controller('EndRegistration', function($scope, SignedUser, $routeParams, $timeout, $location, TracksResource){

    $scope.step = 3;
    $scope.user = SignedUser.user;

    $scope.routeParams = $routeParams;

    if($routeParams.project){
        $scope.project_id = $routeParams.project;
    }

    if(!$scope.user || !$scope.user.name){
        $location.path('/signup');
    }

    $scope.current_step = function(step) {
        return ($scope.step == step) ? 'reg_step_a' : ''
    };

    $scope.tracks = TracksResource.queryUsersBest({ limit: 10 });

    function moveRightBlock(){
        var top = 357 - window.scrollY;
        if(window.scrollY >= 209)
            top = 148;

        $('.right_aside.move')
            .css('right', ((window.innerWidth - 1000) / 2 - 9) +'px')
            .css('top', top + 'px');
    }
    moveRightBlock();
    $timeout(moveRightBlock, 500);
    $(window).on('resize', moveRightBlock);
    $(window).on('scroll', moveRightBlock);

});