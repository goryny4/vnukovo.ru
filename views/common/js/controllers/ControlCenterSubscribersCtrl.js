module.controller('ControlCenterSubscribersCtrl', function($scope, ProjectSubscribersResource) {
    $scope.loading = true;
    $scope.page = 0;
    $scope.complete = false;
    $scope.showZero = false;
    $scope.users = {
        data: []
    };
    $scope.last_page = 1000;


    $scope.nextPage = function() {
        if ($scope.page < $scope.last_page)
        {
            $scope.page += 1;
            $scope.loading = true;
        }
        else
        {
            $scope.complete = true;
            return;
        }
        var users_new = ProjectSubscribersResource.resource.getSubscribers({page: $scope.page});
        users_new.$promise.then(function(result) {
            for (var i = 0; i < result.data.length; i++) {
                $scope.users.data.push(result.data[i]);
            }
            $scope.last_page = result.last_page;
            $scope.total = result.total;
            $scope.loading = false;
        }, function(result) {
            $scope.loading = false;
            $scope.complete = true;
            $scope.showZero = true;
        });
    }
});
