module.controller('ControlCenterSubscriptionCtrl', function($scope, ProjectSubscribersResource) {
    $scope.loading = true;
    $scope.showZero = false;
    $scope.page = 0;
    $scope.complete = false;
    $scope.projects = {
        data: []
    };
    $scope.last_page = 1000;


    $scope.nextPage = function() {
        if ($scope.page < $scope.last_page)
        {
            $scope.page += 1;
            $scope.loading = true;
        }
        else
        {
            $scope.complete = true;
            return;
        }
        var projects_new = ProjectSubscribersResource.resource.getSubscription({page: $scope.page});
        projects_new.$promise.then(function(result) {
            for (var i = 0; i < result.data.length; i++) {
                $scope.projects.data.push(result.data[i]);
            }
            $scope.last_page = result.last_page;
            $scope.total = result.total;
            $scope.loading = false;
            console.log($scope.projects.data.length);
        }, function(result) {
            $scope.loading = false;
            $scope.complete = true;
            $scope.showZero = true;
        });
    }
});
