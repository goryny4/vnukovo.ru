module.controller('AddProjectCtrl', function($scope, Credentials, AccountTypes, Tops, AuthResource, SignedUser, $route, CitiesResource, TracksResource, CountriesResource, UsersResource, $sce, $interval, $timeout, Projects, $location){

    $scope.account_types = AccountTypes;
    $scope.tops          = Tops;
    $scope.credentials   = Credentials;
    $scope.auth          = AuthResource;
    $scope.user          = SignedUser.user;

    $scope.step          = SignedUser.user && SignedUser.user.name ? 2 : null;

    if(!SignedUser.user || !SignedUser.user.name)
        $location.path('/signup');

    $scope.search_text = '';
    $scope.error = '';

    $scope.states = {
        projectCreated: false,
        migrationCreated: false
    };

    $scope.countries = CountriesResource.queryPage();
    $scope.cities    = CitiesResource.queryPage();

    $scope.changeCity = function(){
        angular.forEach($scope.cities, function( city){
            if(city.id == $scope.credentials.project.city_id){
                $scope.credentials.project.country_id = +city.country_id;
            }
        });
    };

    $scope.changeCountry = function(){
        $scope.credentials.project.city_id = 0;
        $scope.cities    = CitiesResource.queryPage({ country_id: $scope.credentials.project.country_id });
    };

    $scope.credentials.project.coverSettings = $scope.coverSettings = {
        "image": {
            "width": 190,
            "height": 190
        },
        "actualRect": {
            "x": 0,
            "y": 0,
            "width": 200,
            "height": 200
        }
    };
    $scope.viewPort = {
        width: 164,
        height: 164,
        getStyle: function(){
            var ratioWidth = $scope.coverSettings.image.width / $scope.coverSettings.actualRect.width,
                ratioHeight = $scope.coverSettings.image.height / $scope.coverSettings.actualRect.height;
            return {
                'margin-left': - Math.round($scope.coverSettings.actualRect.x * ratioWidth),
                'margin-top': - Math.round($scope.coverSettings.actualRect.y * ratioHeight),
                'width':  Math.round($scope.coverSettings.image.width * ($scope.viewPort.width / $scope.coverSettings.actualRect.width)),
                'height': Math.round($scope.coverSettings.image.height * ($scope.viewPort.height / $scope.coverSettings.actualRect.height))
            };
        }
    };

    var timeout = [];
    $scope.changeSearchText = function(){
        if(timeout.length)
            angular.forEach(timeout, function(t, i){
                $timeout.cancel(t);
                timeout.splice(i, 1);
            });

        timeout.push($timeout(function(){
            $scope.users = UsersResource.querySearch({ login: $scope.search_text, limit: 10 });
        }, 200));
    };

    $scope.light = function(sin){
        return $sce.trustAsHtml(sin.replace(new RegExp($scope.search_text, "ig"), function(a){
            return "<span class='high'>"+a+" </span>";
        }));
    };

    $scope.current_step = function(step) {
        return ($scope.step == step) ? 'reg_step_a' : ''
    };

    $scope.check_project_name = function(){

        if($scope.credentials.project.name.length < 3){
            $scope.credentials.project.name_checked = false;
            return;
        }

        Projects.postCheckName({ name: $scope.credentials.project.name }).$promise
            .then(function(result){
                $scope.credentials.project.name_checked = result != 0;
            });
    };

    $scope.cleanImg = function(){
        $scope.credentials.project.imgSrc = '/main/img/cover2.jpg';
        return false;
    };

    $scope.showMessage = function(msg){

        $scope.msg = msg;

        $timeout(function(){
            $scope.msg = '';
        }, 3000);

    };

    $scope.$watch('credentials.project.imgSrc', function(){
        $scope.loading = true;
    });
    $scope.onImgLoad = function(){
        $scope.loading = false;
    };

    $scope.register_project = function(){

        $scope.error = '';
        $scope.loadingRegResponse = true;

        var project = angular.copy($scope.credentials.project);
        project.coverSettings = JSON.stringify(project.coverSettings);
        project.add_all_mysic_styles = null;
        project.addUser = null;
        project.add_music_style = null;
        project.removeUser = null;
        project.has_music_style = null;
        project.getNotInProject = null;
        project.addTel = null;
        project.changeState = null;
        project.check_project_data = null;
        project.label = project.imgSrc != '/main/img/cover2.jpg';

        if(!project.info)
            project.info = 'Здесь будет описание';
        if(!project.infoShort)
            project.infoShort = 'Здесь будет краткое описание';

        if($scope.credentials.project.check_project_data()){

            Projects.postCreateProject(project).$promise
                .then(function( project){
                    $scope.loadingRegResponse = false;

                    if(!project.error){
                        $location.search('project', project.id);
                        $scope.clear();
                        $location.path('/end_registration');
                    }
                    else{
                        $scope.error = project.msg;
                        console.log(project);
                    }
                }, function(){
                    $scope.loadingRegResponse = false;
                    console.log('create failed');
                });
        }
        else{
            $scope.loadingRegResponse = false;
        }

        return false;
    };

    $scope.clear = function(){
        $scope.credentials.project.name = '';
        $scope.credentials.project.name_checked = null;
        $scope.credentials.project.email = '';
        $scope.credentials.project.info = 'Здесь будет описание';
        $scope.credentials.project.infoShort = 'Здесь будет краткое описание';
        $scope.credentials.project.positions = [{country_id: 0, city_id: 0}];

        $scope.credentials.project.tels = [{value: '', permission: 1}];
        $scope.credentials.project.skype = {value: '', permission: 1};
        $scope.credentials.project.vk = {value: '', permission: 1};
        $scope.credentials.project.icq = {value: '', permission: 1};
        $scope.credentials.project.site = {value: '', permission: 1};

        var dateString = '',
            date = new Date();
        dateString += date.getUTCDate() < 10? '0' + date.getUTCDate() : date.getUTCDate();
        dateString += '/' + (date.getUTCMonth() < 10? '0' + date.getUTCMonth() : date.getUTCMonth());
        dateString += '/' + date.getFullYear();
        $scope.credentials.project.createDate = dateString;
        $scope.credentials.project.imgSrc = '/main/img/cover2.jpg';
        $scope.credentials.project.label = false;
        $scope.credentials.project.users = [];
        $scope.credentials.project.work_styles = [];
        $scope.credentials.project.like_styles = [];

    };

    $scope.title = function(permission){
        return permission ? 'Скрыть от всех, кроме друзей' : 'Показать всем';
    };

    $scope.cutInfoShort = function( maxlength){

        var str = $scope.credentials.project.infoShort;

        if (str.length > maxlength) {
            str = str.slice(0, maxlength);
        }

        $scope.credentials.project.infoShort = str;

    };

    $(":file").change(function () {
        console.log(this, arguments);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e){
                $scope.credentials.project.imgSrc = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        }
    });

    $(function() {
        $( ".datepicker" ).datepicker();
    });

    $scope.clickCalc = function(){
        $('input.f_text2.date').focus().click();
    };

    function moveRightBlock(){
        var top = 357 - window.scrollY;
        if(window.scrollY >= 209)
            top = 148;

        $('.right_aside.move')
            .css('right', ((window.innerWidth - 1000) / 2 - 9) +'px')
            .css('top', top + 'px');
    }
    moveRightBlock();
    $timeout(moveRightBlock, 500);
    $(window).on('resize', moveRightBlock);
    $(window).on('scroll', moveRightBlock);

});