
module.controller('HistoryOperationsCtrl', function($scope, history, RubForm, SignedUser){

    $scope.page         = 1;
    $scope.history      = [];
    $scope.complete     = false;
    $scope.loading      = false;
    $scope.rubForm      = RubForm;
    $scope.user         = SignedUser.user;

    //сортировка
    $scope.sortField    = 'date';
    $scope.revs         = true;
    $scope.sortChange   = function(v){
        $scope.history = [];
        $scope.page    = 1;
        $scope.loading = false;
        if($scope.sortField == v){
            $scope.revs = !$scope.revs;
        }
        else{
            $scope.revs = false;
        }
        $scope.sortField = v;
        $scope.loadPage();
    };
    $scope.find         = []; //['type', '=', 'Покупка']
    $scope.$watch('find', function(){
        $scope.loading = false;
        $scope.page = 1;
        $scope.history = [];
        $scope.loadPage();
    });

    $scope.loadPage = function(){

        if ($scope.loading) return;
        $scope.loading = true;
        $scope.complete = false;

        history.get({ page: $scope.page, "wheres[]": $scope.find, order_field: $scope.sortField, order_dir: ($scope.revs? 'ASC': 'DESC') })
            .$promise.then(function( response){
                if(angular.isArray(response.data) && response.data.length){

                    angular.forEach(response.data, function( h){
                        h.paidTill = new Date(h.paidTill);
                        h.date = new Date(h.date);
                    });

                    $scope.history = $scope.history.concat(response.data);
                    if(response.data.length < 15){
                        $scope.complete = true;
                    }else{
                        $scope.loading = false;
                    }
                }
                else{
                    $scope.complete = true;
                }

            },
            function(error){
                console.log(error);
            });

        $scope.page++;
    };

});