module.controller('MusicCollectionCtrl',
	['$scope', '$routeParams', 'SignedUser', 'Playlists', function ($scope, $routeParams, SignedUser, Playlists) {
		'use strict';

		var
			pageNumber = 1,
			userId = $routeParams.userId || SignedUser.user.id,
			sortTypes = {
				date: 'modified',
				name: 'name'
			},
			sortDirections = {
				asc: 'asc',
				desc: 'desc'
			};

		$scope.playlists = [];
		$scope.loading = false;
		$scope.sortTypes = sortTypes;
		$scope.sortDirections = sortDirections;
		$scope.sortType = sortTypes.date;
		$scope.sortDirection = sortDirections.asc;

		$scope.nextPage = function () {
			if ($scope.loading) {
				return;
			}

			getPage();

			pageNumber += 1;
		};

		$scope.sort = function (type) {
			pageNumber = 1;

			$scope.playlists.length = 0;
			if ($scope.sortType === type) {
				$scope.sortDirection = isAsc() ? sortDirections.desc : sortDirections.asc;
			} else {
				$scope.sortDirection = sortDirections.asc;
			}
			$scope.sortType = type;

			getPage();
		};

		$scope.isAsc = isAsc;

		function isAsc() {
			return $scope.sortDirection === sortDirections.asc;
		}

		function getPage() {
			$scope.loading = true;
			$scope.complete = false;

			Playlists.getPage({
				userId: userId,
				pageNumber: pageNumber,
				orderField: $scope.sortType,
				orderDir: $scope.sortDirection
			})
				.$promise.then(function (result) {
					if (result.data.length >= result.per_page) {
						$scope.loading = false;
					}
					else {
						$scope.complete = true;
					}
					result.data.forEach(function (list) {
						$scope.playlists.push(list);
					});
				});
		}

	}]);