module.controller('PromoStatisticCtrl', function($scope, SignedUser, RubForm, CountForm, DayForm, TracksResource){
    'use strict';
    $scope.user = SignedUser.user;
    $scope.CountForm = CountForm;
    $scope.DayForm = DayForm;
    $scope.now = new Date();
    $scope.personalPromo = [];
    $scope.globalPromo = [];

    $scope.promoStats = TracksResource.getPromoStatistic({}).$promise
        .then(function(data){
            angular.forEach(data, function(e, i){
                if(e.from)
                    $scope.globalPromo.push(e);
                else
                    $scope.personalPromo.push(e);
            });
            $scope.loaded = true;
        });

    $scope.getPeriod = function(from, to){
        return Math.floor((new Date(to).getTime() - new Date(from).getTime()) / 24 / 60 / 60 / 1000)
    };
});