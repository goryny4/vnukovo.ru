module.controller('ProjectAlbumsCtrl',
    ['$scope', 'Project', 'Albums', 'SignedUser', 'jplayerInterface', 'toaster', function ($scope, Project, Albums, SignedUser, jplayerInterface, toaster) {
        'use strict';
        $scope.project = Project;
        $scope.albums = Albums;
        $scope.isOwner = SignedUser.isSigned() && SignedUser.user.id == Project.creatorId;

        if ($scope.isOwner) {
            $scope.sortableOptions = {
                stop: function (e, ui) {
                    for (var i = 0; i < $scope.albums.length; i++) {
                        if ($scope.albums[i].userOrder != i) {
                            $scope.albums[i].userOrder = i;

                            $scope.albums[i].$save(function() {}, function () {
                                toaster.pop('error', "Произошла ошибка", "Не удалось изменить порядок альбомов");
                            });
                        }
                    }
                }
            };
        } else {
            $scope.sortableOptions = {
                disabled: true
            };
        }
    }]
);