module.controller('StatInfoCtrl', function($scope, $interval, UsersResource, TracksResource, Projects, statInfoReloadTime) {
	$scope.stat = {
		users: 50600,
		tracks: 43125,
		projects: 98031
	}

	statInfoReload();
	
	$interval(statInfoReload, statInfoReloadTime);

	function statInfoReload() {
		UsersResource.queryOnlineCount(
			function (data) {
				$scope.stat.projects = data.count;
			}
		);

		TracksResource.queryCount(
			function (data) {
				$scope.stat.tracks = data.count;
			}
		);

		Projects.queryCount(
			function (data) {
				$scope.stat.users = data.count;
			}
		);
	}
});
