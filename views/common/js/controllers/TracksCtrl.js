module.controller('TracksCtrl', function ($scope, Styles, TracksResource, FavouritesResource, LikesResource, SignedUser, promoTracks, $animate, $timeout, headerState) {
	'use strict';
	var
		sortTypes = {
			pit: 'pit',
			date: 'public_date'
		},
		sortDirections = {
			asc: '',
			desc: '-'
		},
		sorting = {
			'pit': sortDirections.desc,
			'public_date': sortDirections.desc
		},
		tags = $('#tags'),
		uninterpolate = function (a, b) {
			b = b - (a = +a) ? 1 / (b - a) : 0;
			return function (x) {
				return (x - a) * b;
			};
		},
		interpolate = function (a, b) {
			b -= a = +a;
			return function (t) {
				return a + b * t;
			};
		};

	$scope.LikesResource = LikesResource;
	$scope.user = SignedUser.user;

	$scope.template = 'track_normal.html';
	$scope.page = 1;
	$scope.tracks = [];
	$scope.total = 0;
	$scope.infiniteScrollDisabled = true;
	$scope.filter = {
		period2weeks: false	
	}
	$scope.loading = false;
	// Сортировка
	$scope.sortTypes = sortTypes;
	$scope.sortType = sortTypes.date;
	$scope.sortDirection = sortDirections.desc;
	// Вынесена в отдельную функцию, чтобы менять напавление сортировки каждый раз
	$scope.sort = function (type){
		$scope.sortType = type;
		sorting[type] = (sorting[type] == sortDirections.asc)
			? sortDirections.desc : sortDirections.asc;
		$scope.sortDirection = sorting[type];
		$scope.loadPage(true);
	}
	$scope.isAsc = function() {
		return $scope.sortDirection === sortDirections.asc;
	}
	// Стили
	$scope.styles = Styles;
	$scope.styleFont = (function () {
		var
			promise = Styles.$promise,
			minFont = 12,
			maxFont = 30,
			minCount, maxCount, u, i;

		promise.then(function (styles) {
			minCount = _.min(styles, function (style) {
				return +style.tracks;
			}).tracks;
			maxCount = _.max(styles, function (style) {
				return +style.tracks;
			}).tracks;
			u = uninterpolate(minCount, maxCount);
			i = interpolate(minFont, maxFont);
		});
		return function (count) {
			return Math.floor(i(u(+count)));
		};
	})();

	$scope.selectStyle = function (style) {
		$scope.selectedStyle = style;
		$scope.toggleStyles();
		$scope.loadPage(true);
	};

	$scope.toggleStyles = function () {
		$scope.isStylesOpened = !$scope.isStylesOpened;
		tags.slideToggle(300);
	};
	// Загрузка страницы
	$scope.loadPage = function(reload){
		if ($scope.loading) {
			return;
		}
		// Если нужно заменить данные, то это 1 страница
		if(reload){
			$scope.page = 1;
		}
		// Параметры запроса
		var params = {
				page: $scope.page,
				page_size: 10,
				ordering: $scope.sortDirection + $scope.sortType,
			},
			filtering = [];
		// Стиль
		if ($scope.selectedStyle) {
			params.style = $scope.selectedStyle.id;
		}
		// Период
		if ($scope.filter.period2weeks) {
			filtering.push('period_2_weeks');
		}
		if(filtering){
			params.filtering = filtering.join(',');
		}
		$scope.loading = true;
		$scope.infiniteScrollDisabled = true;
		// Берем треки с метаданными
		TracksResource.queryWithMetaData(params,function(response){
			if(response.tracks && angular.isArray(response.tracks.data)){
				$("meta[name='keywords']").attr('content',response.meta.keywords);
				$("meta[name='description']").attr('content',response.meta.description);
				$("title").html(response.meta.title);
				// Если надо заменить треки
				if(reload){
					$scope.tracks = response.tracks.data;
				}
				else{
					$scope.tracks = $scope.tracks.concat(response.tracks.data);
				}
				$scope.total = response.tracks.total;
				$scope.loading = false;
				$scope.infiniteScrollDisabled = !$scope.total || (response.tracks.current_page == response.tracks.last_page);
			}
		});
		$scope.page++;
	}

	$scope.$watchCollection('filter', function () {
		$scope.loadPage(true);
	});
});

