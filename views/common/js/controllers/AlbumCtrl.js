module.controller('AlbumCtrl',
    ['$scope', '$route', '$location', 'Project', 'Album', 'Tracks', 'Albums', 'SignedUser', 'jplayerInterface', 'toaster', 
    function ($scope, $route, $location, Project, Album, Tracks, Albums, SignedUser, jplayerInterface, toaster) {
        'use strict';
        moment.lang('ru');
        $scope.project = Project;
        $scope.album = Album;      
        $scope.tracks = Tracks;
        $scope.isHidden = false;
        $scope.shortDescrMaxLength = 200;
        $scope.show = {};

        $scope.isOwner = SignedUser.isSigned() && SignedUser.user.id == Project.creatorId;
        // Если какой то левый чувак пытается создать альбом - редиректим проходимца
        if(!$scope.album.id && !$scope.isOwner){
            $location.url('/projects/' + $scope.album.projectId + '/albums/');
        }
        $scope.isEditMode = $scope.isOwner && (!$scope.album.id || $route.current.params.edit);
        $scope.albumDate = moment($scope.album.date).format("DD-MM-YYYY");
        $scope.$watch('album.hidden', function(value) {
            $scope.isHidden = !!+$scope.album.hidden;
        });
        $scope.$watch('album.sizeStr', function(value) {
            // Проверяем, если sizeStr не запонен и это не новый альбом
            if(!value && $scope.album.id){
                // Получаем инфу с сервера
                Albums.resource.getInfo({id: $scope.album.id}, function(info){
                    Albums.processInfo($scope.album, info);
                });
            }
        });

        // Настройки кропа обложки
        $scope.coverSettings = angular.fromJson($scope.album.cover_settings  || Albums.getDefaults().cover_settings);
        // Обложка
        $scope.viewPort = {
            width: 164,
            height: 164,
            getStyle: function(){
                var ratioWidth = $scope.coverSettings.image.width / $scope.coverSettings.actualRect.width,
                    ratioHeight = $scope.coverSettings.image.height / $scope.coverSettings.actualRect.height;
                return {
                    'margin-left': - Math.round($scope.coverSettings.actualRect.x * ratioWidth),
                    'margin-top': - Math.round($scope.coverSettings.actualRect.y * ratioHeight),
                    'width':  Math.round($scope.coverSettings.image.width * ($scope.viewPort.width / $scope.coverSettings.actualRect.width)),
                    'height': Math.round($scope.coverSettings.image.height * ($scope.viewPort.height / $scope.coverSettings.actualRect.height))
                };
            }
        }

        if ($scope.isOwner) {
            $scope.sortableOptions = {
                stop: function (e, ui) {
                    for (var i = 0; i < $scope.tracks.length; i++) {
                        if ($scope.tracks[i].albumOrder != i) {
                            $scope.tracks[i].albumOrder = i;

                            var tagTrackUpdateError = false;
                            $scope.tracks[i].$save(function () {}, function () {
                                if (!tagTrackUpdateError) {
                                    toaster.pop('error', "Произошла ошибка", "Не удалось изменить порядок треков");
                                    tagTrackUpdateError = true;
                                }

                            });
                        }
                    }
                }
            };
        } else {
            $scope.sortableOptions = {
                disabled: true
            };
        }
        $scope.toggleHidden = function(){
            if($scope.album.id){
                Albums.toggleHidden($scope.album);
            }
            else{
                $scope.album.hidden = +!+$scope.album.hidden;
            }
        };
        if($scope.album.id){
            $scope.delete = function(){
                if(Albums.delete($scope.album)){
                    toaster.pop('success', "Действие успешно выполнено", "Альбом удален");
                    $location.url('/projects/' + $scope.album.projectId + '/albums/');
                }
                else{
                    toaster.pop('error', "Произошла ошибка", "Не удалось удалить альбом");
                }
            };
            $scope.toggleEdit = function () {
                $location.url('/projects/' + $scope.album.projectId + '/albums/' + $scope.album.id
                    + (!$scope.isEditMode ? '?edit' : ''));
            };
            $scope.toggleDescr = function(){
                $('#album_descr').slideToggle(300);
                $scope.show['#album_descr'] = !$scope.show['#album_descr'];            
            }
        }
        $scope.save = function () {
            if(! this.album_edit_form.$valid){
                return;
            }
            $scope.album.cover_settings = angular.toJson($scope.coverSettings);
            var def = ($scope.album.id ? $scope.album.$save({id: $scope.album.id}) : $scope.album.$create());
            def.then(function () {
                $location.url('/projects/' + $scope.album.projectId + '/albums/' + $scope.album.id);
            }, function () {
                toaster.pop('error', "Произошла ошибка", "Не удалось " 
                    + ($scope.album.id ? "изменить" : "создать") + " альбом");
            });
        };
    }]
);