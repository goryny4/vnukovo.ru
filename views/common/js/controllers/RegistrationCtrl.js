module.controller('RegistrationCtrl', function($scope, Credentials, AccountTypes, Tops, AuthResource, SignedUser, $route, CitiesResource, TracksResource, CountriesResource, UsersResource, $sce, $interval, $timeout, Projects, $location){
    $scope.account_types = AccountTypes;
    $scope.tops          = Tops;
    $scope.credentials   = Credentials;
    $scope.auth          = AuthResource;

    $scope.step = 1;
    if(SignedUser.user && SignedUser.user.name){
        $location.path('/addproject');
    }

    $scope.search_text   = '';

    $scope.states = {
        projectCreated: false,
        migrationCreated: false
    };

    var timeout = [];
    $scope.changeSearchText = function(){
        if(timeout.length)
            angular.forEach(timeout, function(t, i){
                $timeout.cancel(t);
                timeout.splice(i, 1);
            });

        timeout.push($timeout(function(){
            $scope.users = UsersResource.querySearch({ login: $scope.search_text, limit: 10 });
        }, 200));
    };

    $scope.light = function(sin){
        return $sce.trustAsHtml(sin.replace(new RegExp($scope.search_text, "ig"), function(a){
            return "<span class='high'>"+a+" </span>";
        }));
    };

    $scope.check_email = function(email) {
        if (email.length > 0) {
            var promise = $scope.auth.check_email({email: email});
            promise.$promise.then(function(data) {
                $scope.credentials.email_checked = !!data.status;
                $scope.check_user_in_cjclub($scope.credentials.login, email);
            });
        }
    };

    $scope.check_login = function(login) {
        if (login.length > 0) {
            var promise = $scope.auth.check_login({login: login});
            promise.$promise.then(function(data) {
                $scope.credentials.login_checked = !!data.status;
                $scope.check_user_in_cjclub(login, $scope.credentials.email);
            });
        }
    };

    $scope.migrate = function(){
        $scope.showMigrateForm = true;
    };

    $scope.link = function(){

    };

    $scope.hideMsg = function(){
        $scope.credentials.migrate = false;
    };

    $scope.check_user_in_cjclub = function(login, email){

        if($scope.credentials.email_checked && $scope.credentials.login_checked){
            $scope.auth.is_user_in_cjclub({ login: login, email: email }).$promise
                .then(function(data){
                    $scope.credentials.migrate = data.id;
                },
                function(){
                    $scope.credentials.migrate = null;
                });
        }
    };

    $scope.check_password = function() {
        var password = $scope.credentials.password,
            password_repeat = $scope.credentials.password_repeat;
        if (password.length > 3 && password_repeat.length > 3) {
            var success = password == password_repeat;
            $scope.credentials.password_checked = success;
            return success;
        }
        else return false;
    };

    $scope.current_step = function(step) {
        return ($scope.step == step) ? 'reg_step_a' : ''
    };

    $scope.to_step = function(step ) {
        var credentials = $scope.credentials;
        var data = {
            email: credentials.email,
            login: credentials.login,
            password: credentials.password,
            password_repeat: credentials.password_repeat,
            name: credentials.name,
            gender: credentials.gender,
            account_types: credentials.account_types,
            music_styles: credentials.music_styles
        };

        if (credentials.check_registration_data()) {
            var promise = $scope.auth.register(data);
            promise.$promise.then
            (function(user) {
                SignedUser.user = user;
                console.log(step);
                if(step == 2)
                    $location.path('/addproject');
                else
                    $location.path('/end_registration');

                $scope.$emit('user:registered');
            }, function(data) {
                console.log('fail register');
                $route.reload();
            });
        }
    };

    $scope.register_project = function(){

        var project = angular.copy($scope.credentials.project);

        project.coverSettings = JSON.stringify(project.coverSettings);

        project.add_all_mysic_styles = null;
        project.addUser = null;
        project.add_music_style = null;
        project.removeUser = null;
        project.has_music_style = null;
        project.getNotInProject = null;
        project.addTel = null;
        project.changeState = null;
        project.label = project.imgSrc != '/main/img/cover2.jpg';

        Projects.postCreateProject(project).$promise
            .then(function( project){
                $scope.credentials.project.id = project;
                $scope.step = 3;
                $scope.tracks = TracksResource.queryUsersBest({ limit: 10 });
            }, function(){
                console.log('create failed');
            });

        return false;
    };

    function moveRightBlock(){
        var top = 357 - window.scrollY;
        if(window.scrollY >= 209)
            top = 148;

        $('.right_aside.move')
            .css('right', ((window.innerWidth - 1000) / 2 - 9) +'px')
            .css('top', top + 'px');
    }
    moveRightBlock();
    $timeout(moveRightBlock, 500);
    $(window).on('resize', moveRightBlock);
    $(window).on('scroll', moveRightBlock);

});