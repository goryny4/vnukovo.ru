module.controller('PromoTracksCtrl', function($scope, SignedUser, TracksResource, LikesResource, promoTracks, $timeout) {
    $scope.LikesResource = LikesResource;

    $scope.user = SignedUser.user;
    $scope.$watch(function(){ return promoTracks.tracks; }, function(nw){
        $scope.promos = nw;
        $scope.urPromo = promoTracks.urPromo;
    });
    $scope.removePersonalPromo = function( track){
        TracksResource.removePersonalPromoTrack({ id: track.id }).$promise.then(function(){
            promoTracks.tracks[promoTracks.tracks.indexOf(track)].id = 0;
        });
    };
    $scope.$watch(function(){ return promoTracks.index }, function(nv){
        $scope.index = nv;
    });

    $scope.zeroTracks = function(){
        var result = false;
        angular.forEach($scope.promos, function(track, i){
            if(track.id)
                result = true
        });
        return !result && $scope.urPromo;
    };

    $scope.$watch(function(){ return $scope.zeroTracks() },
    function(nw){
        $scope.sortableOptions.disabled = nw;
    });

    $scope.sortableOptions = {
        axis: 'x',
        disabled: false,
        update: function(e, ui) {
            promoTracks.tracks = promoTracks.tracks.sort(function(a, b){
                return a.order - b.order;
            });
            var   index1 = ui.item.sortable.index
                , index2 = ui.item.sortable.dropindex

                , order1 = index1 +1
                , order2 = index2 +1

                , elem1 = promoTracks.tracks[index1]
                , elem2 = promoTracks.tracks[index2]
                ;

            if(order1 < 6 && order2 < 6 && order1 > 0 && order2 > 0){
                elem2.order = order1;
                promoTracks.tracks[index1] = elem2;

                elem1.order = order2;
                promoTracks.tracks[index2] = elem1;

                TracksResource.swapPersonalPromoTracks({ order1: order1, order2: order2 });
            }
        }
    };

    $scope.promoslist = {
        accept: function() {
            //return !($scope.promos.length >= 5) && promoTracks.urPromo;
            return typeof promoTracks.index != 'undefined';
        },
        drop: function(event, obj){

            if(obj.draggable.context.className == "gb ng-pristine ng-valid"){
                $timeout(function(){

                    var idInArray = $scope.promos.length - 1,
                        newTrack = $scope.promos[idInArray];

                    TracksResource.addPersonalPromoTrack({ id: newTrack.id, order: promoTracks.index });
                    var order = promoTracks.index;
                    newTrack.order = order;
                    angular.forEach(promoTracks.tracks, function(val, i){
                        if(val.order == order)
                            promoTracks.tracks[i] = newTrack;
                    });
                    delete $scope.promos[idInArray];

                }, 300);
            }

        }
    };

});