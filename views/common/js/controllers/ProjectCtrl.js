module.controller('ProjectCtrl', ['$scope', 'Project', 'Albums', 'TracksResource', 'Blogs', 'SignedUser', 
    'ProjectSubscribersResource', 'ProjectsSettings', '$timeout', 'headerState', '$animate', 'promoTracks', 'toaster', 
    function($scope, Project, Albums, TracksResource, Blogs, SignedUser, ProjectSubscribersResource, ProjectsSettings,
        $timeout, headerState, $animate, promoTracks,  toaster) {
	$scope.project = Project;
	$scope.project.$promise.then(function() {
		$scope.project.styles = [
			{
				id: 2,
				style: 'Trance'
			},
			{
				id: 14,
				style: 'DnB'
			}
		];
	}); // todo mockup
	$scope.blog = Blogs;
	$scope.tracks = [];
	$scope.albums = [];
    ProjectsSettings.resource.get({projectId: $scope.project.id}, function(settings){
        var albumsParams = {projectId: $scope.project.id},
            tracksParams = {projectId: $scope.project.id};
        if(settings.show_albums_count > 0){
            albumsParams.limit = settings.show_albums_count;
        }
        Albums.resource.query(albumsParams, function(albums){
                $scope.albums = albums;
        });
        if(settings.show_tracks_count > 0){
            tracksParams.limit = settings.show_tracks_count;
        }
        TracksResource.query(tracksParams, function(tracks){
                $scope.tracks = tracks;
        });
    });

	$scope.isAccessGranted = SignedUser.isSigned() && SignedUser.user.id == Project.creatorId;

    if ($scope.isAccessGranted) {
        $scope.albumsSortableOptions = {
            stop: function (e, ui) {
                for (var i = 0; i < $scope.albums.length; i++) {
                    if ($scope.albums[i].userOrder != i) {
                        $scope.albums[i].userOrder = i;

                        $scope.albums[i].$save(function() {}, function () {
                            toaster.pop('error', "Произошла ошибка", "Не удалось изменить порядок альбомов");
                        });
                    }
                }
            }
        };
        $scope.tracksSortableOptions = {
            stop: function (e, ui) {
                for (var i = 0; i < $scope.tracks.length; i++) {
                    if ($scope.tracks[i].userOrder != i) {
                        $scope.tracks[i].userOrder = i;

                        var tagTrackUpdateError = false;
                        $scope.tracks[i].$save(function () {}, function () {
                            if (!tagTrackUpdateError) {
                                toaster.pop('error', "Произошла ошибка", "Не удалось изменить порядок треков");
                                tagTrackUpdateError = true;
                            }

                        });
                    }
                }
            }
        };
    } else {
        $scope.albumsSortableOptions = {
            disabled: true
        };
        $scope.tracksSortableOptions = {
            disabled: true
        };
    }

    //подписка на проект
    $scope.project_subscribe = ProjectSubscribersResource;
    $scope.subscribe = function(project) {
        var query = $scope.project_subscribe.subscribe(project);
        query.$promise.then(function(project) {
            $scope.project = project;
            $scope.project_subscribe.update_user($scope.project);

            //TODO mockup
            $scope.project.styles = [
                {
                    id: 2,
                    style: 'Trance'
                },
                {
                    id: 14,
                    style: 'DnB'
                }
            ];

        });
    }
    $scope.user = SignedUser.user;


}]);
