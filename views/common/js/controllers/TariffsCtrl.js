module.controller('TariffsCtrl', function($scope, TariffsResource, $location, RubForm, ProjectForm, SignedUser, Projects){

    $scope.page         = 1;
    $scope.tariffs      = [];
    $scope.complete     = false;
    $scope.loading      = false;
    $scope.prLoaded     = false;
    $scope.rubForm      = RubForm;
    $scope.ProjectForm  = ProjectForm;
    $scope.user = SignedUser.user;

    $scope.projects = Projects.queryPage({ user: true });

    $scope.loadPage = function(){

        if ($scope.loading) return;
        $scope.loading = true;
        $scope.complete = false;

        TariffsResource.queryPage({ page: $scope.page, order_dir: 'asc', order_field: 'projectCount' }).$promise
            .then(function( response){
                if(angular.isArray(response.data) && response.data.length){

                    $scope.tariffs = $scope.tariffs.concat(response.data);
                    $scope.columns = [];
                    var columns = Math.ceil($scope.tariffs.length/3);
                    for(var i = 0; i<columns;i++){
                        $scope.columns.push(i*3);
                    }
                    $scope.prLoaded = true;
                    if(response.data.length < 15){
                        $scope.complete = true;
                    }else{
                        $scope.loading = false;
                    }

                    angular.forEach($scope.tariffs, function(tariff){
                        angular.forEach(tariff.users, function(user){
                            if(user.id == $scope.user.id)
                                $scope.activeTariff = tariff;
                        })
                    });
                }
                else{
                    $scope.complete = true;
                }

            },
            function(error){
                console.log(error);
            });

        $scope.page++;
    };

    $scope.redirect = function( path){
        $location.path( path);
    };

});