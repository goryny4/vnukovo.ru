

module.controller('ServiceCtrl', function($scope, ServicesResource, Projects, $routeParams, RubForm, SignedUser, $timeout, $sce, $filter){

    $scope.loaded = false;
    $scope.rubForm = RubForm;
    $scope.user = SignedUser.user;
    //$scope.customList = [5, 7];

    $scope.getPath = function(){
        return 'main/partials/sub/service-'+$routeParams.id+'.html';
    };

    $scope.renderHtml = function( html){
        return $sce.trustAsHtml($filter('wikitext')( html));
    };

    Projects.queryPage({ user: true }).$promise
        .then(function(response){
            angular.forEach(response, function(p){
                p.checked = true;
            });
            $scope.projects = response;
        });

    ServicesResource.get({ id: $routeParams.id }).$promise
        .then(function( response){
            $scope.service = response;
            $scope.service.period = 1;
            $scope.loaded = true;
        });

    $scope.$watch('response', function(){
        $timeout(function(){
            $scope.response = false;
        }, 4000);
    });

    $scope.purchase = function( service, project){

        var selectedProjects = $scope.getSelectedProjectsIds();
        if(selectedProjects.length < 1){
            $scope.response = { error: true, msg: 'Выберите проект' };
            return;
        }

        ServicesResource.subscribeTo({ id: service.id, period: service.period || 1, projectIds: selectedProjects.join(',') })
            .$promise.then(function( response){
                $scope.response = response;
                if(response.error) return;
                $scope.user.balance -= response.moneyAmount;
                angular.forEach($scope.getSelectedProjects(), function(prj){
                    $scope.service.projects.push(prj);
                });
                $scope.bought = true;
            });
    };

    $scope.extend = function( service){

        ServicesResource.extend({ id: service.id, period: service.period || 1 })
            .$promise.then(function( response){
                $scope.response = response;
                $scope.user.balance -= (service.type == 'once')? service.price : ( service.period * service.price);
            })
    };

    $scope.isSubscribe = function( service, projectId){
        var bought = false;
        angular.forEach(service.projects, function(project){
            if(projectId == project.id && project.creatorId == $scope.user.id){
                bought = true;
            }
        });
        return bought;
    };

    $scope.getSelectedProjectsIds = function(){
        var result = [];
        angular.forEach($scope.getSelectedProjects(), function( project){
            result.push( project.id);
        });
        return result;
    };

    $scope.getSelectedProjects = function(){
        var result = [];
        angular.forEach( $scope.projects, function(pr){
            if(pr.checked && !$scope.isSubscribe($scope.service, pr.id)){
                result.push(pr);
            }
        });
        return result;
    };

    $scope.allSubscribed = function(){
        var result = true;
        angular.forEach($scope.projects, function(pr, i){
            if(result) result = $scope.isSubscribe($scope.service, pr.id) || pr.bought;
        });
        return result;
    };

});