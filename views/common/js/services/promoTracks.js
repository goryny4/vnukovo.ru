
module.factory('promoTracks', function($rootScope, TracksResource, SignedUser, $http, $location){
    var user = SignedUser.user;

    Array.prototype.inArrayKeyValue = function(key, value){
        var result = false;
        angular.forEach(this, function(val, i){
            if(val[key] == value){
                result = true;
            }
        });
        return result;
    };

    var promo = {
        tracks: [],

        userId: -1,
        lastUserId: -1,
        urPromo: false,

        id: undefined,
        index: undefined,

        load: function(){
            if(promo.userId != promo.lastUserId){
                TracksResource.queryPromo({ user: promo.userId }).$promise.then(function( promos){
                    promo.tracks = promos;
                    var i = 1;
                    while(promo.tracks.length < 5){
                        if(!promo.tracks.inArrayKeyValue('order', i)){
                            promo.tracks.push({ order: i });
                        }
                        i++;
                    }
                });
                promo.lastUserId = promo.userId;
            }
        },

        setId: function(val){
            this.id = val;
        },
        setIndex: function(val){
            this.index = val;
        }
    };

    $rootScope.$watch(function(){ return promo.userId }, function( id){
        promo.urPromo = (id == user.id);
        promo.load();
    });

    $rootScope.$on('$routeChangeSuccess', function(){
        $http.get('api/users/page', { params: { path: $location.path() }})
            .success(function(id){
                promo.userId = id;
            });
    });

    return promo;
});