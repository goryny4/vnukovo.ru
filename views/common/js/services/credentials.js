Storage.prototype.setObject = function(key, value) {
    this.setItem(key, JSON.stringify(value));
};

Storage.prototype.getObject = function(key) {
    var value = this.getItem(key);
    return value && JSON.parse(value);
};

module.factory('Credentials', function() {

    var date = new Date(),
        dateString = '';

    dateString += date.getUTCDate() < 10? '0' + date.getUTCDate() : date.getUTCDate();
    dateString += '/' + (date.getUTCMonth() < 10? '0' + date.getUTCMonth() : date.getUTCMonth());
    dateString += '/' + date.getFullYear();

    var credentials_el = {

        email: '',
        login: '',
        password: '',
        password_repeat: '',
        name: '',
        gender: 'male',

        account_types: [],
        music_styles: [],

        email_checked: null,
        login_checked: null,
        password_checked: null,

        migrate: null,

        project: {

            creatorId: null,

            name: '',
            name_checked: null,

            email: '',

            info: 'Здесь будет описание',
            infoShort: 'Здесь будет краткое описание',

            musicRating: '',
            musicPlace: '',
            config: '',

            imgSrc: '/main/img/cover2.jpg',

            city_id: 0,
            country_id: 0,

            tels: [{value: '', permission: 1}],

            skype: {value: '', permission: 1},
            vk: {value: '', permission: 1},
            icq: {value: '', permission: 1},
            site: {value: '', permission: 1},

            createDate: dateString,

            totalTracks: 0,
            totalNews: 0,
            totalSubscribers: 0,
            totalPhoto: 0,

            type: null,
            status: null,

            label: false,

            users: [],
            work_styles: [],
            like_styles: [],

            check_name: false,

            // Methods

            check_project_data: function(){
                return this.name_checked;
            },

            add_music_style: function (key, style) {
                var index = this[key].indexOf(style);
                (index == -1) ? this[key].push(style) : this.music_styles.splice(index, 1);
            },

            add_all_mysic_styles: function (key, top) {
                var self = this;
                var styles = top.styles;
                top[key] = [];
                if (top[key]['styles_added'] == null || top[key]['styles_added'] == 0) {
                    angular.forEach(styles, function (style) {
                        var index = self[key].indexOf(style);
                        if (index == -1) self[key].push(style);
                    });
                    top[key]['styles_added'] = 1;
                } else {
                    angular.forEach(styles, function (style) {
                        var index = self[key].indexOf(style);
                        if (index != -1) self[key].splice(index, 1);
                    });
                    top[key]['styles_added'] = 0;
                }
            },

            has_music_style: function (key, style) {
                return (this[key].indexOf(style) !== -1);
            },

            addUser: function (user) {
                var found = false;
                angular.forEach(this.users, function (u) {
                    if (u.id == user.id)
                        found = true;
                }.bind(this));

                !found && this.users.push(user);
            },
            removeUser: function (index) {
                this.users.splice(index, 1);
            },

            getNotInProject: function (users) {
                var result = [];
                angular.forEach(users, function (user, i) {
                    var found = false;
                    angular.forEach(this.users, function (u) {
                        if (user.id == u.id)
                            found = true;
                    });
                    if (!found) result.push(user);

                }.bind(this));
                return result;
            },

            addTel: function () {
                console.log('add tell');
                this.tels.push({value: '', permission: 1});
            },
            addPos: function () {
                this.positions.push({country_id: 0, city_id: 0});
            },
            changeState: function (obj) {
                obj.permission = obj.permission ? 0 : 1;
            }
        },

        add_music_style: function (style) {
            var index = this.music_styles.indexOf(style);
            (index == -1) ? this.music_styles.push(style) : this.music_styles.splice(index, 1);
        },

        add_account_type: function (type) {
            var index = this.account_types.indexOf(type);
            if (this.account_types.length < 3) {
                if (index == -1)
                    this.account_types.push(type);
                else
                    this.account_types.splice(index, 1);
            } else {
                if (index != -1)
                    this.account_types.splice(index, 1);
            }
        },

        add_all_mysic_styles: function (top) {
            var self = this;
            var styles = top.styles;
            if (top['styles_added'] == null || top['styles_added'] == 0) {
                angular.forEach(styles, function (style) {
                    var index = self.music_styles.indexOf(style);
                    if (index == -1) self.music_styles.push(style);
                });
                top['styles_added'] = 1;
            } else {
                angular.forEach(styles, function (style) {
                    var index = self.music_styles.indexOf(style);
                    if (index != -1) self.music_styles.splice(index, 1);
                });
                top['styles_added'] = 0;
            }
        },

        has_music_style: function (style) {
            return (this.music_styles.indexOf(style) !== -1);
        },

        has_account_type: function (type) {
            return (this.account_types.indexOf(type) !== -1);
        },

        check_registration_data: function () {
            return this.email_checked && this.login_checked && this.password_checked;
        }

    };

    return credentials_el;
});
