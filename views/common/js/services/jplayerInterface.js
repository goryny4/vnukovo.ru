module.factory('jplayerInterface', function ($rootScope, $interval, playlist, radioUrls, TracksResource, radioUpdateTime) {
	'use strict';
	var
		volume = 1,
		volumeMutted,
		el,
		prevTrackId,
		isRadio,
		radioBitrateHigh = true,
		isPlayingState,
		radioUpdateInterval,
		currentTrack,
		updateCurrent = function (id) {
			if (angular.isNumber(id)) {
				TracksResource.get({id: id}).$promise.then(function (track) {
					currentTrack = track;
					$rootScope.$broadcast('jplayerInterface:trackUpdated', currentTrack);
				});
			}
		},
		getRadioInfo = function () {
			TracksResource.getRadio().$promise.then(function (track) {
				currentTrack = track[0]; //TODO узнать почему массивом приходит
				$rootScope.$broadcast('jplayerInterface:trackUpdated', currentTrack);
			});
		},
		updateRadioInfo = function () {
			getRadioInfo();

			if (angular.isUndefined(radioUpdateInterval)) {
				radioUpdateInterval = $interval(
					function () {
						getRadioInfo();
					},
					radioUpdateTime
				);
			}
		},
		stopUpdateRadio = function () {
			$interval.cancel(radioUpdateInterval);
			radioUpdateInterval = undefined;
			updateCurrent(prevTrackId);
		},
		service = {
			info: {
				currentTime: 0,
				position: 0,
				durationEstimate: 100,
				bytesLoaded: 0,
				bytesTotal: 100
			},
			init: function (e, options) {
				var self = this;
				el = e;
				el.jPlayer({
					swfPath: 'libs/jplayer',
					solutions: 'html, flash',
					supplied: 'mp3',
					preload: 'auto',
					timeupdate: function (e) {
						if (service.isRadio()) {
							return;
						}

						service.info.position = e.jPlayer.status.currentPercentAbsolute;
						service.info.currentTime = e.jPlayer.status.currentTime;
						if (!$rootScope.$$phase) {
							$rootScope.$apply();
						}
					},
					progress: function () {
						if (service.isRadio()) {
							return;
						}

						var audio = el.find('audio')[0];
						if (audio.buffered.length > 0) {
							service.info.bytesLoaded = (audio.buffered.end(audio.buffered.length - 1) / audio.duration) * 100;
							if (!$rootScope.$$phase) {
								$rootScope.$apply();
							}
						}
					}
				});

				if (options.autoNext) {
					el.bind($.jPlayer.event.ended, function () {
						var next = playlist.getNext(prevTrackId);
						if (next) {
							self.playId(next);
						}
					});
				}
			},
			destroy: function (el, options) {
				if (options.autoNext) {
					el.unbind($.jPlayer.event.ended);
				}
			},
			setMedia: function (url) {
				el.jPlayer("setMedia", {mp3: url});
			},
			setId: function (trackId) {
				service.info.position = 0;
				service.info.bytesLoaded = 0;
				service.info.currentTime = 0;

				prevTrackId = trackId;
				var url = '_tracks/' + trackId + '.mp3';
				service.setMedia(url);
				updateCurrent(trackId);
			},
			play: function (time) {
				el.jPlayer("play", time);
				isPlayingState = true;
			},
			playId: function (trackId) {
				service.setId(trackId);
				service.play();
			},
			playIdOrPause: function (trackId) {
				if (isRadio) {
					service.stopRadio();
				}

				if (prevTrackId === trackId) {
					service.togglePlay();
				} else {
					service.playId(trackId);
				}
			},
			pause: function () {
				el.jPlayer("pause");
				isPlayingState = false;
			},
			togglePlay: function () {
				if (isPlayingState) {
					service.pause();
				} else {
					service.play();
				}
			},
			isPlaying: function () {
				return isPlayingState;
			},
			isRadio: function () {
				return isRadio;
			},
			isIdbeingPlayed: function (trackId) {
				return (prevTrackId === trackId) && service.isPlaying() && !isRadio;
			},
			toggleMute: function () {
				if (volume === 0) {
					service.setVolume(volumeMutted);
				} else {
					volumeMutted = volume;
					service.setVolume(0);
				}
			},
			getVolume: function () {
				return volume;
			},
			setVolume: function (v) {
				volume = v;
				el.jPlayer("volume", v);
			},
			getTrackId: function () {
				return currentTrack ? currentTrack.id : prevTrackId;
			},
			playRadio: function () {
				var streamUrl = radioBitrateHigh ? radioUrls.high : radioUrls.low;
				service.setMedia(streamUrl);
				service.play();
				isRadio = true;
				updateRadioInfo();
			},
			stopRadio: function () {
				isRadio = false;
				isPlayingState = false;
				stopUpdateRadio();

				service.setId(prevTrackId);
			},
			setRadioHighBitrate: function (value) {
				if (radioBitrateHigh === value) {
					return;
				}
				radioBitrateHigh = value;
				if (isRadio) {
					service.playRadio();
				}
			},
			isBitrateHigh: function () {
				return radioBitrateHigh;
			},
			getCurrentTrack: function () {
				return currentTrack;
			}
		};

	return service;
});
