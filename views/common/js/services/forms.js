
module
    .constant('RubForm', {
        0: '{} рублей',
        one: '{} рубль',
        few: '{} рубля',
        many: '{} рублей',
        other: '{} рублей'
    })
    .constant('MonthForm', {
        0: ' 0 месяцев ',
        one: '{} месяц',
        few: '{} месяцa',
        many: '{} месяцев',
        other: '{} месяцев'
    })
    .constant('DayForm', {
        0: '0 дней',
        one: '{} день',
        few: '{} дня',
        many: '{} дней',
        other: '{} дней'
    })
    .constant('ProjectForm', {
        0: '{} проектов',
        one: '{} проект',
        few: '{} проекта',
        many: '{} проектов',
        other: '{} проектов'
    });