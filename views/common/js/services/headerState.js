module.factory('headerState', ['$rootScope', 'localStorageService', function ($rs, localStorageService) {
	'use strict';

	var
		HEADER_STATE = 'cjclub_header_state',
		states = {
			opened: 0,
			closed: 1
		},
		currentState = Number(localStorageService.get(HEADER_STATE)) || states.opened;

	return {
		getState: function () {
			return currentState;
		},
		setState: function (state) {
			currentState = state;
			localStorageService.set(HEADER_STATE, state);
            $rs.$broadcast('header.stateChanged');
		},
		states: states,
		isOpen: function () {
			return currentState === states.opened;
		}

	};

}]);