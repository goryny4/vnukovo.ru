module.directive('aboutProjectPopup', function($popover, $interval) {
    'use strict';

    var currentPopover = null;

    return {
        restrict: 'A',
        scope: {
            project: '='
        },
        link: function(scope, element, attrs) {
            var interval,
                popover = $popover(element, {
                    trigger: 'manual',
                    container: 'body',
                    placement: 'bottom',
                    template: 'main/partials/sub/aboutProjectPopup.html'
                });

            popover.$scope.mouseIsOver = false;
            popover.$scope.project = scope.project;

            element.on('mouseover', function() {
                if (currentPopover !== null) {
                    currentPopover.$scope.$hide();
                }
                currentPopover = popover;
                popover.$scope.$show();
                popover.$scope.mouseIsOver = true;
            });

            element.on('mouseleave', function() {
                $interval.cancel(interval);
                popover.$scope.mouseIsOver = false;

                interval = $interval(function() {
                    if (!popover.$scope.mouseIsOver) {
                        popover.$scope.$hide();
                        $interval.cancel(interval);
                    }
                }, 150);
            });

            element.on('$destroy', function() {
                $interval.cancel(interval);
                popover.$scope.$hide();
            });
        }
    }
});