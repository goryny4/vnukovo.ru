module.directive('topTracksBlock', function(TracksResource, Projects) {
	return {
		restrict: 'E',
		templateUrl: 'main/partials/sub/topTracks.html',
		link: function(scope, elem, attrs){
			attrs.$observe("topTrackStyle", function(topTrackStyle){
				scope.topTrackStyle = topTrackStyle;
				scope.topTracks = TracksResource.query({
					style: scope.topTrackStyle,
					ordering: "pit,-label",
					limit: 4
				});
			});
			attrs.$observe("topTrackTitle", function(topTrackTitle){
				scope.topTrackTitle = topTrackTitle;
			});
		}
	}
});