module.directive('album', ['$route', 'Albums', 'Projects', 'SignedUser', 'toaster', function ($route, Albums, Projects, SignedUser, toaster) {
	'use strict';
	return {
		restrict: 'EA',
        link: function (scope, element, attrs){
            scope.isOwner = false;
            scope.isHidden = true;
            scope.readOnly = !!attrs.readOnly;
            scope.album.styles = [];
            scope.albumDate = moment(scope.album.date).format("DD-MM-YYYY");
            scope.isOwner = SignedUser.isSigned() && SignedUser.user.id == scope.album.project.creatorId;
            scope.$watch('album.hidden', function() {
                scope.isHidden = !!+scope.album.hidden;
            });
            scope.$watch('album.sizeStr', function(value) {
                if(!value){
                    Albums.resource.getInfo({id: scope.album.id}, function(info){
                        Albums.processInfo(scope.album, info);
                    });
                }
            });
            scope.toggleHidden = function(){
                if(scope.readOnly){
                    return;
                }
                Albums.toggleHidden(scope.album);
            };
            scope.delete = function(){
                if(Albums.delete(scope.album)){
                    toaster.pop('success', "Действие успешно выполнено", "Альбом удален");
                    $route.reload();
                }
                else{
                    toaster.pop('error', "Произошла ошибка", "Не удалось удалить альбом");
                }
            };
        },
		templateUrl: 'main/partials/sub/album.html'
	};
}]);