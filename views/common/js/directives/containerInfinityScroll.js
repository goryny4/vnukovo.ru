
/*
 * бесконечный скролл привязанный к контейнеру
 * @param scroll - function
 *
 */
module.directive('scroll', [function() {
    return {

        link: function(scope, elm, attrs) {

            var raw = elm[0];
            elm.bind('scroll', function() {
                if (raw.scrollTop + raw.offsetHeight+5 >= raw.scrollHeight && !scope.$eval(attrs.scrollDisable, scope)) {
                    if(attrs.scroll) scope.$eval(attrs.scroll, scope);
                    scope.$apply();
                }
            });

        }
    };
}
]);