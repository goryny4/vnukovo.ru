module.directive('headerCloser', ['$rootScope', 'headerState', 'SignedUser', '$document', '$window', 'ProjectsSettings', 
						 function ($rootScope, headerState, SignedUser, $document, $window, ProjectsSettings) {
	'use strict';

	return {
		restrict: 'E',
		template: '<div class="header_closer"></div>',
		replace: true,
		link: function (scope, element) {
            scope.header_auto_folding = null;

            if (SignedUser.isSigned()) {
				ProjectsSettings.getUserProjectSettings(SignedUser.user.id).then(function(args){
					var project = args[0];
					ProjectsSettings.resource.get({projectId: project.id}, function(settings){
						scope.header_auto_folding = Math.round(settings.header_auto_folding);
					});
				});
			}

			scope.$watch(SignedUser.isSigned, function(e){
                if(e && headerState.getState() == 0){
                    element.show();
                }else{
                    element.hide();
                }
            });

			scope.$on('updateProjectsSettingsEvent', function(){
				ProjectsSettings.getUserProjectSettings(SignedUser.user.id).then(function(args){
					var project = args[0];
					ProjectsSettings.resource.get({projectId: project.id}, function(settings){
						scope.header_auto_folding = Math.round(settings.header_auto_folding);
					});
				});
			});

			var states = headerState.states;
			element.on('click', function () {
				headerState.setState(states.closed);
			});

			$document.bind('scroll', function (event) {
				if (scope.header_auto_folding && SignedUser.isSigned()) {
					if (headerState.getState() == headerState.states.opened && $window.pageYOffset > 100) {
						headerState.setState(states.closed);
						element.hide();
					} else if (headerState.getState() == headerState.states.closed && $window.pageYOffset <= 100) {
						headerState.setState(states.opened);
						element.show();
					}
				}				
			});
		}
	};
}]);