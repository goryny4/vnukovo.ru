module.directive('trackTop', function (LikesResource) {
	'use strict';
	return {
		restrict: 'EA',
        scope: {
            track: '='
        },
        link: function(scope) {
            scope.LikesResource = LikesResource;
        },
		templateUrl: 'main/partials/sub/track_top.html'
	};
});