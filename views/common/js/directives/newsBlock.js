module.directive('newsBlock', function() {
	return {
		restrict: 'E',
		templateUrl: 'main/partials/sub/newsBlock.html',
		controller: 'NewsColumnCtrl',
		link: function () {
		}
	}
});