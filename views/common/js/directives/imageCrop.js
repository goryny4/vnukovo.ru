module.directive('imageCrop', function ($timeout) {
    'use strict';
    return {
        restrict: 'E',
        template: '<img ng-src="{{src}}">',
        replace: true,
        scope: {
            src: '=',
            selection: '=?',
            aspect: '=?',
            color: '=?',
            opacity: '=?',
            minSize: '=?',
            maxSize: '=?',
            enabled: '=?',
            allowSelect: '=?',
            allowMove: '=?',
            allowResize : '=?',
            width: '@',
            height: '@',
            onSelect: '&',
            onChange: '&',
            onRelease: '&'
        },
        link: function (scope, element) {
            var isInternalSelectionUpdate = false;
            var jcropInstance;

            function ensureDefaultPropertyValue(property, defaultValue) {
                if (scope[property] === undefined) {
                    scope[property] = defaultValue;
                }
            }

            function onSelect(args) {
                scope.onSelect({args: args});
            }

            function onChange(args) {
                scope.onChange({args: args});
                isInternalSelectionUpdate = true;
                scope.selection = {
                    x: args.x,
                    y: args.y,
                    width: args.w,
                    height: args.h
                };
                $timeout(function() {
                    scope.$apply();
                });
            }

            function onRelease() {
                scope.onRelease();
            }

            function getOptions(initial) {
                var selection;
                if (_.isObject(scope.selection)) {
                    selection = [scope.selection.x,scope.selection.y, scope.selection.x + scope.selection.width, scope.selection.y + scope.selection.height];
                }
                var options = {
                    aspectRatio: scope.aspect,
                    minSize: scope.minSize,
                    maxSize: scope.maxSize,
                    setSelect: selection,
                    bgColor: scope.color,
                    bgOpacity: scope.opacity,
                    allowSelect: scope.allowSelect,
                    allowMove: scope.allowMove,
                    allowResize : scope.allowResize
                };

                if (initial) {
                    options.boxWidth = scope.width;
                    options.boxHeight = scope.height;
                    options.onSelect = onSelect;
                    options.onChange = onChange;
                    options.onRelease = onRelease;
                }
                return options;
            }

            scope.$watch('src', function() {
                if (jcropInstance !== undefined) {
                    jcropInstance.setImage(scope.src);
                }
            });

            scope.$watch('enabled', function() {
                if (jcropInstance === undefined) {
                    return;
                }
                if (scope.enabled) {
                    jcropInstance.enable();
                } else {
                    jcropInstance.disable();
                }
            });

            scope.$watch('selection', function(newValue, oldValue) {
                if (isInternalSelectionUpdate) {
                    isInternalSelectionUpdate = false;
                    return;
                }
                if (jcropInstance === undefined) {
                    return;
                }
                if (_.isObject(newValue)) {
                    jcropInstance.setOptions(getOptions());
                } else if (_.isObject(oldValue)){
                    jcropInstance.release();
                }
            });

            scope.$watchCollection('[width, height, color, opacity, minSize, maxSize, allowSelect, allowMove, allowResize]', function(newValue, oldValue) {
                if (jcropInstance === undefined) {
                    return;
                }
                jcropInstance.setOptions(getOptions());
            });

            ensureDefaultPropertyValue('opacity', 0.6);
            ensureDefaultPropertyValue('color', 'black');
            ensureDefaultPropertyValue('enabled', 'true');
            ensureDefaultPropertyValue('allowSelect', 'true');
            ensureDefaultPropertyValue('allowMove', 'true');
            ensureDefaultPropertyValue('allowResize ', 'true');

            $(element).Jcrop(getOptions(true), function() {
                jcropInstance = this;
            });
        }
    };
});