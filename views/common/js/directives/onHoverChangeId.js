
module.directive('onHoverChangeOrder', function(promoTracks, $timeout){
    return {
        restrict: 'AE',
        link: function(scope, elem, attrs){

            $(elem).hover(function(){
                promoTracks.setIndex(scope.track.order);
            }, function(){})
        }
    }
});