module.directive('trackWave',
	[ 'jplayerInterface', 'TracksResource', 'SignedUser', 'Comments', '$rootScope',
		function (jplayerInterface, TracksResource, SignedUser, Comments, $rootScope) {
			'use strict';

			var
				MIDDLE = 100,
				secToString = function (totalSec, all) {
					totalSec = Math.floor(totalSec);

					var
						hours = parseInt(totalSec / 3600, 10) % 24,
						minutes = parseInt(totalSec / 60, 10) % 60,
						seconds = totalSec % 60,
						result =
							(all || hours ? (hours < 10 ? "0" + hours : hours) + ":" : '') +
							(minutes < 10 ? "0" + minutes : minutes) + ":" +
							(seconds < 10 ? "0" + seconds : seconds);
					return result;
				},
				drawTime = function (position, currentTime, totalTime, canvasWidth) {
					var
						height = 16,
						padding = 3,
						x = ((position * canvasWidth) / 100) + 1,
						totalX,
						draw = function (x, sec) {
							var
								width,
								text = secToString(sec),
								y = MIDDLE - height;

							this.font = '10px';
							width = this.measureText(text).width + padding;

							if (typeof x === 'function') {
								x = x(width);
							}

							if (!totalX) {
								totalX = x;
							} else if (x + width >= totalX) {
								x = totalX - width;
							}

							this.fillStyle = '#000';
							this.fillRect(x, y, width, height);

							this.textAlign = 'center';
							this.textBaseline = 'middle';
							this.fillStyle = "#f60";
							this.fillText(text, x + width / 2, y + height / 2);
						};

					this.save();
					this.setTransform(1, 0, 0, 1, 0, 0);
					this.clearRect(0, 0, canvasWidth, MIDDLE);
					this.restore();

					draw.call(this, function (width) {
						return canvasWidth - width;
					}, totalTime);
					draw.call(this, x, currentTime);

				},
				redraw = function () {
					var d, i, middle, t, _i, _len, _ref, _results, y, bottom;
					this.clear();
					middle = MIDDLE;
					i = 0;
					_ref = this.data;
					_results = [];
					t = this.width / this.data.length;
					bottom = this.height - middle;
					for (_i = 0, _len = _ref.length; _i < _len; _i += 1) {
						d = _ref[_i];

						this.context.fillStyle = this.innerColor(i / this.width, d);
						y = middle - middle * d;

						this.context.save();
						this.context.setTransform(1, 0, 0, 1, 0, 0);
						this.context.clearRect(t * i, 0, t, this.height);
						this.context.restore();

						this.context.fillRect(t * i, y, t, middle - y);
						this.context.fillRect(t * i, middle, t, bottom * d);

						i += 1;
						_results.push(i);
					}
					return _results;
				},
				showComment = function (time, comments) {
					if (!time || !comments) {
						return;
					}

					var showed = false;

					comments.forEach(function (comment) {
						if (!showed && (time === comment.timestamp || (time < comment.timestamp + 2 && time > comment.timestamp - 2))) {
							comment.show = true;
							showed = true;
						} else {
							comment.show = false;
						}
					});
				},
				link = function (scope, element) {
					var
						colors = {
							defaultColor: '#6b6b6c',
							loadedColor: '#444444',
							playedColor: '#bd5f23'
						},
						width = element.parent().width(),
						waveform = new Waveform({
							container: element.find('.wave')[0],
							width: width,
							height: 140
						}),
						streamOptions = waveform.optionsForSyncedStream(colors),
						timeCanvas = waveform.createCanvas(element.find('.wave_time')[0], width, MIDDLE),
						timeContext = timeCanvas.getContext("2d"),
						commentBox = element.find('#cot'),
						commentInput = commentBox.find('#cot_text');

					waveform.redraw = redraw;

					streamOptions.whileloading.call(jplayerInterface.info);

					scope.comments = [];

					scope.seekAndPlay = function (e) {
						var x, percents, seekTo;

						if (e.offsetX === undefined)	// this works for Firefox
						{
							x = e.pageX - $(e.target).offset().left;
						}
						else													// works in Google Chrome
						{
							x = e.offsetX;
						}

						percents = x / width;
						seekTo = Math.round(scope.track.timelength * percents);
						if (jplayerInterface.getTrackId() !== scope.track.id) {
							jplayerInterface.setId(scope.track.id);
						}
						jplayerInterface.play(seekTo);
					};

					scope.$watch(
						function () {
							return jplayerInterface.info;
						},
						function (info) {
							if (jplayerInterface.getTrackId() === scope.track.id) {
								waveform.redraw();
								drawTime.call(timeContext, info.position, info.currentTime, scope.track.timelength, width);
								showComment(info.currentTime, scope.comments);
							}
						},
						true
					);

					scope.$watch('track', function (track) {
						TracksResource.getWave({id: track.id}).$promise.then(function (data) {
							waveform.update({
								data: data.points
							});
							drawTime.call(timeContext, 0, 0, track.timelength, width);
						});

						var time, index, lenght, timeStampLength = 10, maxCommentLength = 30, commentLength;

						Comments.query({
							objectType: 'track',
							objectId: track.id,
							labeledOnly: 1
						}).$promise.then(function (comments) {
                            comments.forEach(function (comment) {
                                if (comment.timelabels) {
                                    index = 0;
                                    time = moment(comment.timelabels, 'HH:mm:ss');
                                    lenght = comment.message.length;
                                    commentLength = lenght - timeStampLength > maxCommentLength ? maxCommentLength : lenght - timeStampLength;
                                    scope.comments.push({
                                        userId: comment.user.id,
                                        userName: comment.user.name,
                                        commentId: comment.id,
                                        comment: comment.message, //.substr(index + timeStampLength, commentLength),
                                        timestamp: time.second() + time.minutes() * 60 + time.hours() * 3600
                                    });
                                }
                            });
                        });
					});
					scope.newComment = {};

					scope.getUser = function () {
						return SignedUser.user || {};
					};

					scope.isSigned = SignedUser.isSigned;

					scope.commentPosition = function (timestamp) {
						return (timestamp / scope.track.timelength) * 100;
					};

					scope.setCommentTime = function ($e) {
						if ($e.isDefaultPrevented() || !SignedUser.isSigned()) {
							return;
						}

						var x = $e.offsetX,
							percents = x / width;

						scope.newComment.timestamp = Math.round(scope.track.timelength * percents);
						commentInput.focus();
					};

					scope.addComment = function () {
                        var timelabel = '[' + secToString(scope.newComment.timestamp, true) + ']',
                            msg  = scope.newComment.text,
							user = scope.getUser();

						Comments
							.add({
								objectType: 'track',
								objectId: scope.track.id,
								message: msg,
								authorId: user.id,
								'object_type': 'track',
								'object_id': scope.track.id,
								parentCommentId: 0,
								quoteText: '',
                                timelabels: timelabel,
								'track_sharing': false
							})
							.$promise.then(function (result) {
								if (result.status !== 'ok') {
									return;
								}
								scope.comments.push({
									userId: user.id,
									userName: user.name,
									comment: scope.newComment.text,
									commentId: result.response.id,
									timestamp: scope.newComment.timestamp
								});

								$rootScope.$broadcast('comments:addComment', result.response);

								commentInput.blur();
								scope.newComment.text = null;
							});
					};

					scope.showComment = function ($e, id) {
						$e.preventDefault();
						$rootScope.$broadcast('comments:showComment', id);
					};

					commentInput.on('focus', function () {
						commentBox.addClass('cot_focus');
						if (scope.newComment.timestamp === null || scope.newComment.timestamp === undefined) {
							scope.newComment.timestamp = jplayerInterface.info.currentTime;
						}
						if (!!scope.$$parse) {
							scope.$apply();
						}
					});

					commentInput.on('blur', function () {
						commentBox.removeClass('cot_focus');
						scope.newComment.timestamp = null;
						if (!!scope.$$parse) {
							scope.$apply();
						}
					});
				};

			return {
				scope: {
					track: '='
				},
				restrict: 'AEC',
				templateUrl: 'main/partials/sub/trackWave.html',
				link: link
			};

		}
	]
);