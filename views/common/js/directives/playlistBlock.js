module.directive('playlistBlock', ['Playlists', 'playlist', function (Playlists, playlist) {
	'use strict';

	var visibility = {
			visible: 'yes',
			hidden: 'no'
		},

		link = function (scope) {
			scope.visibility = visibility;

			scope.toggleVisibility = function () {
				var v = scope.model['public'] === visibility.visible ? visibility.hidden : visibility.visible;
				Playlists.update(
					{id: scope.model.id},
					angular.extend(
						{},
						scope.model,
						{
							public: v,
							tracks: _.map(scope.model.tracks, function (track) {
								return track.id;
							})
						}
					)
				).$promise.then(function () {
						scope.model.public = v;
					});
			};

			scope.remove = function () {
				Playlists.remove({id: scope.model.id}).$promise.then(function () {
					var source = scope.$parent[scope.sourceName];
					if (source && angular.isArray(source) && source.length) {
						var index = source.indexOf(scope.model);
						source.splice(index, 1);
					}
				});
			};

			scope.play = function () {
				playlist.clearPlaylist();
				playlist.addRange(scope.model.tracks);
			};

			scope.download = function () {
				Playlists.getTorrent({id: scope.model.id});
			};

			scope.getSize = function () {
				if(!scope.model.tracks.length) {
					return undefined;
				}

				return _.reduce(scope.model.tracks, function (memo, track) {
					return memo + Number(track.filesize);
				}, 0);
			};
		};

	return {
		restrict: 'EAC',
		scope: {
			model: '=playlistBlock',
			sourceName: '@'
		},
		replace: true,
		templateUrl: 'main/partials/sub/playlistBlock.html',
		link: link
	};
}]);