module.directive('mainHeader', ['$q', 'localStorageService', 'playlist', '$window', 'headerState',
	function ($q, localStorageService, playlist, $window, headerState) {
		'use strict';
		var
			deferred = $q.defer(),
			contentLoaded = deferred.promise,
			scroller = function (element) {
				var
					top = document.documentElement.scrollTop || document.body.scrollTop,
					headerShadow = document.getElementById('header_shadow'),
					maxScroll = 250,
					headerShadowHeight = top ? ((top > maxScroll) ? 20 : 20 * top / maxScroll ) : 0;

				if (top > 0) {
					element.addClass('fixed');
					//TODO убрать из этой директивы
					$('.promo_what').hide();
				} else {
					element.removeClass('fixed');
					$('.promo_what').show();
				}

				headerShadow.style.height = headerShadowHeight + "px";
				document.body.className = top === 0 ? 'onTop' : '';
			},
			setInitState = function () {
				contentLoaded.then(function () {
					var margin = headerState.isOpen() ? 150 : 82;
					$('#main-content').css('margin-top', margin);
				});

				changeState(0);
			},
			changeState = function (duration) {
				if (typeof duration === 'undefined') {
					duration = 300;
				}

				if (headerState.isOpen()) {
					$('#topmini').slideUp(duration);
					$('#top').slideDown();
					$('#header').animate({'height': '113'}, duration);
					$('.header-bg').animate({'height': '150'}, duration);
					$('#main-content').animate({'margin-top': '150'}, duration);
					$('.header_closer').show();
					$('nav.cf').css('position', 'fixed').animate({'top': '113px'}, duration);
				} else {
					$('#top').slideUp(duration);
					$('#topmini').slideDown();
					$('#header').animate({'height': '45'}, duration);
					$('.header-bg').animate({'height': '45'}, duration);
					$('#main-content').animate({'margin-top': '82'}, duration);
					$('.header_closer').hide();
					$('nav.cf').css('position', 'absolute').animate({'top': '45px'}, duration);
				}
			},
			link = function (scope, element) {
				setInitState();

				scope.togglePlaylist = function () {
					playlist.togglePlaylist();
				};

				scope.$on('$viewContentLoaded', function () {
					deferred.resolve();
					scroller(element);
				});

				scope.$on('header.stateChanged', function () {
					playlist.close();
					changeState();
				});

				$($window).on('scroll', function () {
					scope.safeApply(function () {
						scroller(element);
					});
				});
			};

		return {
			restrict: 'AE',
			replace: true,
			scope: true,
			link: link,
			templateUrl: 'main/partials/sub/main-header.html'
		};
	}]);