module.directive('textBlock', ['$http', '$sce', '$filter',  function ($http, $sce, $filter) {
    return {
        restrict: 'E',
        template:  '<div ng-bind-html="content"></div>',
        replace: true,
        link: function (scope, ellement, attrs) {
            $http({method: 'GET', url: '/api/content/'+attrs.label}).
                success(function(data) {

                    if(attrs.filter)
                        data.content = $filter(attrs.filter)(data.content);

                    scope.content = $sce.trustAsHtml(data.content);
                })
        }
    };

}]);