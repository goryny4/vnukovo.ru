module.directive('promo' , function (headerState, $timeout, $animate, $rootScope){
    return {
        restrict: 'EA',
        controller: "PromoTracksCtrl",
        templateUrl: "main/partials/sub/promo.html",

        link: function(scope, elem, attrs){

            var returning = function(){
                if(!headerState.getState()){
                    $timeout(function(){
                        $animate.removeClass( angular.element('promo'), 'promo_fixed');
                        $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch');
                    }, 300);
                }
                else{
                    $timeout(function(){
                        $animate.removeClass( angular.element('promo'), 'promo_fixed2');
                        $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch2');
                    }, 300);
                }
                console.log('returning');
            };

            scope.state = function(){
                return headerState.getState();
            };
            scope.$watch('state()', function(value){
                if(!value){
                    $animate.removeClass( angular.element('promo'), 'promo_fixed2');
                    $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch2');
                }
                else{
                    $animate.removeClass( angular.element('promo'), 'promo_fixed');
                    $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch');
                }
            });

            angular.element(document).on('scroll', function(){
                if(!headerState.getState()){
                    $timeout(function(){
                        $animate.removeClass( angular.element('promo'), 'promo_fixed');
                        $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch');
                    }, 300);
                }
                else{
                    $timeout(function(){
                        $animate.removeClass( angular.element('promo'), 'promo_fixed2');
                        $animate.removeClass( angular.element('#promoclutch'), 'promo_clutch2');
                    }, 300);
                }
            });

        }
    };
});

