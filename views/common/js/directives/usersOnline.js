module.directive('usersOnline', function($interval, UsersResource, userPingTime) {
	return {
		restrict: 'E',
		templateUrl: 'main/partials/sub/usersOnline.html',
		//controller: 'UsersOnlineCtrl',
		scope: true,
		link: function (scope) {
			scope.users = UsersResource.queryOnline();
			updateUserPing();
			$interval(updateUserPing, userPingTime);

			function updateUserPing() {
				UsersResource.queryPing();
			}
		}
	}
});