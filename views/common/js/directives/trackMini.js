module.directive('trackMini', function (playlist, LikesResource, FavouritesResource, promoTracks, headerState, $animate, $compile, SignedUser, $timeout) {
	'use strict';
	return {
		restrict: 'EA',
		link: function (scope, element, attrs) {
            scope.user = SignedUser.user;
            scope.playlist = playlist;
            scope.LikesResource = LikesResource;
            scope.FavouritesResource = FavouritesResource;

            scope.inAlbum = !!attrs.inAlbum;
            scope.heartOpen = false;
            scope.isOwner = scope.track.project.creatorId == scope.user.id;
            attrs.$observe('sortable', function(sortable){
                scope.isTrackSortable = scope.$eval(sortable);
            });
            scope.toogleHeart = function(track_id) {
                if (scope.heartOpen) {
                    scope.people = null;
                    return scope.heartOpen = false;
                } else {
                    var query = LikesResource.peopleByTrack(track_id);
                    query.$promise.then(function(data) {
                        scope.people = data;
                    });
                    return scope.heartOpen = true;
                }
            };
            scope.isHeartOpen = function() {
                if (scope.people)
                    return (scope.heartOpen && scope.people.length > 0) ? true : false;
            };

            scope.$watch(function(){ return promoTracks.tracks }, function(nw){
                scope.promoS = promoTracks;
                scope.urPromo = promoTracks.urPromo;
            });
            scope.urPromo = promoTracks.urPromo;
            scope.hasInPromo = function( id){
                var has = false;
                angular.forEach(promoTracks.tracks, function(track){
                    if(track && track.id == id){
                        has = true;
                    }
                });
                return has;
            };

            scope.onStart = function( event, ui ){

                scope.dontShowRemove = true;
                ui.helper.closest('.gb')
                    .addClass('draggingTrack')
                    .html($compile('<track-mini data-tpl="track_promo.html"></track-mini>')(scope));

                if(!headerState.getState()){
                    $animate.addClass( angular.element('promo'), 'promo_fixed');
                    $animate.addClass( angular.element('#promoclutch'), 'promo_clutch');
                }
                else{
                    $animate.addClass( angular.element('promo'), 'promo_fixed2');
                    $animate.addClass( angular.element('#promoclutch'), 'promo_clutch2');
                }
            };
            scope.onStop = function( event, ui ){
                var elem = ui.helper.closest('.gb')
                    .removeClass('draggingTrack draggable')
                    .replaceWith($compile('<track-mini data-tpl="track_normal.html"></track-mini>')(scope));
                var id = elem[0].id;

                $timeout(function(){
                    $('#'+id+' .gb_label_in')
                        .hover()
                        .mouseenter()
                        .mouseleave();
                }, 100);

            };

		},
		templateUrl: function (el, context) {
			var tpl = context.tpl ? context.tpl : 'track_mini.html';
			return 'main/partials/sub/' + tpl;
		}
	};
});