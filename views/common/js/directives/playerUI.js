module.directive('playerUi', function (jplayerInterface, playlist, LikesResource, SignedUser) {
	'use strict';
	var
		link = function (scope) {
            scope.signedUser = SignedUser;
			scope.playlist = playlist;
            scope.LikesResource = LikesResource;

			scope.next = function (id) {
				if (jplayerInterface.isRadio()) {
					return;
				}
				var next = playlist.getNext(id, true);
				if (next) {
					jplayerInterface.playId(next);
				}
			};

			scope.prev = function (id) {
				if (jplayerInterface.isRadio()) {
					return;
				}
				var prev = playlist.getPrev(id, true);
				if (prev) {
					jplayerInterface.playId(prev);
				}
			};

			scope.isRadio = jplayerInterface.isRadio;
			scope.radioBitrateHigh = jplayerInterface.isBitrateHigh;
			scope.setRadioHighBitrate = jplayerInterface.setRadioHighBitrate;
			scope.playRadio = jplayerInterface.playRadio;
			scope.stopRadio = jplayerInterface.stopRadio;

			scope.$on('jplayerInterface:trackUpdated', function ($e, track) {
				scope.track = track;
			});

			scope.track = jplayerInterface.getCurrentTrack();
			scope.isTrackSet = function () {
				return scope.track !== null && scope.track !== undefined;
			};
		};

	return {
		restrict: 'EA',
		scope: true,
		controller: 'PlayerCtrl', //TODO перенсти в директиву
		templateUrl: function (el, attr) {
			var tpl = attr.tpl ? attr.tpl : 'player.html';
			return 'main/partials/sub/' + tpl;
		},
		link: link
	};
});