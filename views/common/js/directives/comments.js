module.directive('comments', ['$log', 'SignedUser', 'jplayerInterface', 'Comments', function ($log, SignedUser, jplayerInterface, Comments) {

    function CommentModel(data) {
        this.__children = [];
        this.__parent = null;
        this.__quotedText = null;
        this.__canEdit = false;
        this.__isRemoved = false;
        if (angular.isObject(data)) {
            this.setData(data);
        }
    }

    CommentModel.prototype.setData = function (data) {

        this.data = data;

        var signedUserId = SignedUser.isSigned() ? SignedUser.user.id : 0;
        var commentAuthorId = this.data.user ? this.data.user.id : 0;
        var isMine = signedUserId == commentAuthorId;

        this.__visible = this.data.status == "active" && !this.data.complaint;
        this.__removed = this.data.status == "deleted" && !this.data.complaint;

        this.__message = this.data ? (this.data.message || null) : null;

        if (this.data.datewritten) {

            var isModerator = false; //TODO
            var isOwner = false; //TODO

            var datewritten = moment.utc(this.data.datewritten, "YYYY-MM-DD HH:mm:ss");

            var now = moment();
            var dateWrittenLocal = datewritten.local();

            this.data.datewritten = dateWrittenLocal.format("YYYY-MM-DD HH:mm:ss");
            var isNew = (now.unix() - dateWrittenLocal.unix()) < 3600;
            this.__canEdit = (isMine && isNew) || isModerator;
            this.__canDelete = (isMine && isNew) || isModerator || isOwner;

        }

    };

    CommentModel.prototype.isVisible = function () {
        return this.__visible;
    };

    CommentModel.prototype.setVisibility = function (isVisible) {
        this.__visible = !!isVisible;
    };

    CommentModel.prototype.setParent = function (parent) {
        this.__parent = parent;

        var input = this.data.message;

        var quotesRE = /\[quote\]((?!\[\/quote\])[\s\S]*?)\[\/quote\]/g,
            oneQuoteRe = /^\[quote\]([\s\S]*)\[\/quote\]$/;
        var matches = input.match(quotesRE);
        var result = '';
        if (matches != null && matches.length) {
            for (var i = 0; i < matches.length; i++) {
                input = input.replace(matches[i], '');
                var quoteMatches = matches[i].match(oneQuoteRe);
                if (quoteMatches == null || !quoteMatches.length) {
                    $log.error("invalid quote expression:", matches[i]);
                    continue;
                }

                result += quoteMatches[1];
            }
        }
        result = result.replace("[quote]", "").replace("[/quote]", "");
        this.setMessage(input);
        this.__quotedText = result;

    };

    CommentModel.prototype.getParent = function () {
        return this.__parent;
    };

    CommentModel.prototype.getChildren = function () {
        return this.__children;
    };

    CommentModel.prototype.getQuotedText = function () {
        return this.__quotedText;
    };

    CommentModel.prototype.getMessage = function () {
        return this.__message;
    };

    CommentModel.prototype.setMessage = function (msg) {
        this.__message = msg;
    };

    CommentModel.prototype.canEdit = function () {
        return this.__canEdit;
    };

    CommentModel.prototype.isRemoved = function () {
        return this.__removed;
    };

    CommentModel.prototype.canDelete = function () {
        return this.__canDelete;
    };

    function CommentsCollection(ordering) {
        this.__commentsHash = {};
        this.__comments = [];
        this.setOrdering(ordering);
    }

    CommentsCollection.prototype.clear = function () {
        this.__commentsHash = {};
        this.__comments.splice(0);
    };

    CommentsCollection.prototype.getComments = function () {
        return this.__comments;
    };

    CommentsCollection.prototype.setOrdering = function (ordering) {
        if (!ordering) {
            ordering = 'desc'
        }
        this.__ordering = ordering.toLowerCase();
    };

    CommentsCollection.prototype.addComment = function (data) {

        var model = null;
        if (this.__commentsHash[data.id]) {
            model = this.__commentsHash[data.id];
            model.setData(data);
        }
        else {
            model = new CommentModel(data);
            this.__commentsHash[data.id] = model;
            this.__comments.push(model);
        }

        if (data.com_to_com > 0) {
            var parent = null;
            if (this.__commentsHash.hasOwnProperty(data.com_to_com)) {
                parent = this.__commentsHash[data.com_to_com];
            }
            else {
                /*
                quote = [
                    'id':data.com_to_com,
                    ''
                ]
                parent = new CommentModel(quote);
                model.setParent(parent); */
/*
                Comments.getComment({id: data.com_to_com}).$promise.then(function (result) {
                    parent = new CommentModel(result);
                    model.setParent(parent);
                });
*/

              //  this.__comments.push(parent);
                parent = new CommentModel({
                    id: data.com_to_com,
                    quoteAuthorId: data.quoteAuthorId,
                    quoteAuthorName: data.quoteAuthorName,
                    quoteDatewritten: data.quoteDatewritten
                });
            //    this.__commentsHash[data.com_to_com] = parent;
            }
            model.setParent(parent);
        }

        var orderInt = this.__ordering == 'asc' ? 1 : -1;

        this.__comments.sort(function (a, b) {
            var aDate = moment(a.data.datewritten, "YYYY-MM-DD HH:mm:ss"),
                bDate = moment(b.data.datewritten, "YYYY-MM-DD HH:mm:ss");
            return orderInt * (aDate.unix() - bDate.unix());
        });
    };

    CommentsCollection.prototype.removeComment = function (comment) {
        if (this.__commentsHash[comment.data.id]) {
            this.__comments.splice(this.__comments.indexOf(this.__commentsHash[comment.data.id]), 1);
            delete this.__commentsHash[comment.data.id];
            return true;
        }
        return false;
    };

    CommentsCollection.prototype.removeCommentSoftly = function (comment,revert) {
        if (this.__commentsHash[comment.data.id]) {
            this.__commentsHash[comment.data.id].__removed = revert?false:true;
        }
        return false;
    };

    CommentsCollection.prototype.getCommentById = function (id) {
        return this.__commentsHash[id] || null;
    };

    return {
        restrict: "A",
        templateUrl: "main/partials/sub/comments.html",
        scope: {
            title: "@",

            /**
             * Type of resource to which these comments are being added
             * Currently supported values are: "article", "track"
             */
            objectType: "@",

            /**
             * Resource identifier
             */
            objectId: "@",

            order: "@",

            /**
            * Offset from top for scrolling to comment
            */
            scrollOffset: '='

        },
        controller: ['$scope', '$element', '$attrs', '$http', '$sce', 'Comments', 'BackendUtils', function ($scope, $element, $attrs, $http, $sce, Comments, BackendUtils) {

            var COMMENTS_PER_PAGE = 20;
            $scope.loadedTillCommentId = false;
            $scope.isSignedUser = SignedUser.isSigned();

            $scope.signedUser = SignedUser;

            $scope.commentsCollection = new CommentsCollection($scope.order || null);
            $scope.comments = $scope.commentsCollection.getComments();

            $scope.loading = false;

            $scope.scrollOffset = $scope.scrollOffset || 160;

            var _page = 1;

            function submit(message, parentComment, quoteText) {

                if (!message) {
                    $log.error("Comment message is empty");
                    return;
                }

                $http({
                    method: 'post',
                    url: '/api/comments/add/' + $scope.objectType + '/' + $scope.objectId,
                    data: {
                        authorId: SignedUser.isSigned() ? SignedUser.user.id : 0,
                        message: message,
                        object_type: $scope.objectType,
                        object_id: $scope.objectId,
                        parentCommentId: parentComment == null ? 0 : parentComment.data.id,
                        quoteText: quoteText || null,
                        track_sharing: false //TODO
                    },
                    transformRequest: BackendUtils.transformRequestToForm
                }).success(function (result) {

                        if (result.status != 'ok') {
                            return;
                        }
                        $scope.commentsCollection.addComment(result.response);
                        scrollToComment(result.response.id,true);
                    }).error(function () {

                    });

            }


            function scrollToComment(id,flash) {
                setTimeout(function () {
                    var commentElement = jQuery(document.getElementById('comment-' + id));
                    if (!commentElement.length) {
                        return;
                    }
                    //var firstCommentOffset = ($scope.comments[0].data.id == id?190:0);
                    var customOffset = 185;
                    window.scrollTo(0, commentElement.offset().top - $scope.scrollOffset - customOffset);
                    if (flash) {
                        commentElement.find('.com').addClass('hightlighted');
                        setTimeout(function () {
                            commentElement.find('.com').removeClass('hightlighted');
                        }, 1000);
                    }
                }, 100);
            }

            $scope.findAndShowComment = function (comment) {
                if (!comment.data || !comment.data.id) {
                    $log.warn("Unknown comment:", comment);
                    return;
                }
               // var isLoaded = !!comment.data.datewritten;
                var isLoaded = $scope.commentsCollection.getCommentById(comment.data.id);
                if (isLoaded) {
                    scrollToComment(comment.data.id,true);
                }
                else {
                    scrollToComment($scope.loadedTillCommentId,false);
                    $scope.loading = true;
                //    $scope.commentsCollection.clear();
                //    _page = 1;
                    $http({
                        method: 'get',
                        url: '/api/comments/' + $scope.objectType + '/' + $scope.objectId + '/' + comment.data.id + '-' + $scope.loadedTillCommentId,
                        data: {
                            ordering: $scope.order
                        }
                    }).success(function (result) {
                            $scope.loading = false;
                            if (result.status == 'ok') {
                                _page += result.response.pagesCount;
                                var comments = result.response.comments;
                                for (var i = 0; i < comments.length; i++) {
                                    $scope.commentsCollection.addComment(comments[i]);
                                }
                                if (i) $scope.loadedTillCommentId = comments[i-1].id;
                                scrollToComment(comment.data.id,true);
                            }

                        }).error(function () {

                        });
                }
            };

            $scope.nextPage = function () {
                if ($scope.loading) return;
                $scope.loading = true;
                $scope.complete = false;
                Comments.query({objectType: $scope.objectType, objectId: $scope.objectId, page: _page, ordering: $scope.order || null}).$promise.then(function (result) {

                    if (result.length >= COMMENTS_PER_PAGE) {
                        $scope.loading = false;
                    }
                    else {
                        $scope.complete = true;
                    }
                    for (var i = 0; i < result.length; i++) {
                        if (!$scope.loadedTillCommentId || $scope.loadedTillCommentId > result[i].id) {
                            $scope.commentsCollection.addComment(result[i]);
                        }
                    }
                    if (i) {
                        $scope.loadedTillCommentId = result[i-1].id;
                        _page++;
                    }
                });

            };

            $scope.submitNew = function (message) {
                submit(message, null);
            };

            $scope.submitResponse = function (comment, quoteText, message) {
                submit(message, comment, quoteText);
            };

            $scope.submitEdit = function (comment, message, quote) {
                $http({
                    method: 'post',
                    url: '/api/comment/' + comment.data.id + '/edit',
                    data: {
                        commentId: comment.data.id,
                        message: (quote ? '[quote]' + quote + '[/quote]' : '') + message,
                        lid: null,//trackId,
                        track_sharing: false,
                        answerid: 0,
                        object_type: $scope.objectType
                    },
                    transformRequest: BackendUtils.transformRequestToForm
                }).success(function (result) {
                        if (result.status == 'ok') {
                            comment.setData(result.response);
                        }

                    }).error(function () {

                    });
            };

            $scope.submitDelete = function (comment, revert) {
                $http({
                    method: 'post',
                    url: '/api/comment/' + comment.data.id + '/delete/' + (revert?'true':'false'),
                    data: {
                        commentId: comment.data.id
                    },
                    transformRequest: BackendUtils.transformRequestToForm
                }).success(function (result) {
                        if (result.status == 'ok') {
                        //    $scope.commentsCollection.setVisibility(0);
                        //    $scope.commentsCollection.removeComment(comment);
                            $scope.commentsCollection.removeCommentSoftly(comment, revert);
                        }
                    }).error(function () {

                    });
            };

            $scope.rate = function (comment, rank) {
                $http({
                    method: 'post',
                    url: '/api/comment/' + comment.data.id + '/rate',
                    data: {
                        rank: rank
                    },
                    transformRequest: BackendUtils.transformRequestToForm
                }).success(function (result) {
                        if (result.status == 'ok') {
                            comment.data.rating = result.response.rating;
                        }
                    }).error(function () {

                    });
            };

            $scope.addToFriends = function (user) {
                $log.log("addToFriends:", user);
            };

            $scope.ignore = function (user) {
                $log.log("ignore:", user);
            };

            $scope.complain = function (comment) {
                $log.log("complain:", comment);
            };

            $scope.clearQuotes = function (text) {
                return text.replace(/\[quote\][\s\S]*\[\/quote\]/g, '');
            };

            $scope.extractQuote = function (text) {
                var matches = text.match(/\[quote\]([\s\S]*)\[\/quote\]/, '');
                if (matches != null) {
                    return matches[1];
                }
                return null;
            };

            $scope.playMoment = function (timelabel) {
                var date = moment(timelabel, 'HH:mm:ss'),
                    seconds = date.second() + date.minutes() * 60 + date.hours() * 3600;
                // TODO: дальше нужно обращаться к Волне как с сервису, а не напрямую к плееру.
                if (jplayerInterface.getTrackId() !== $scope.objectId) {
                    jplayerInterface.setId($scope.objectId);
                }
                jplayerInterface.play(seconds);
            };

            $element.on('$destroy', function () {
                $log.log("Destroy comments");
            });
            $scope.$on('comments:addComment', function ($e, comment) {
                $scope.commentsCollection.addComment(comment);
            });
            $scope.$on('comments:showComment', function ($e, id) {
                var comment = $scope.commentsCollection.getCommentById(id);
                if (comment) {
                    $scope.findAndShowComment(comment);
								}
            });

        }],

        link: function (scope, element, attrs) {

        }

    };

}]);