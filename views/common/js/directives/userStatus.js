module.directive('userStatus', function() {
    'use strict';

    return {
        restrict: 'E',
        template: '<span class="user_stat"></span>',
        replace: true,
        scope: {
            user: '='
        },
        link: function(scope, element, attrs) {
            var statuses = {
                5: 'user_stat_1', // Pro
                4: 'user_stat_2', // Exp || Skilled
                3: 'user_stat_3', // Advanced
                2: 'user_stat_4' // Newbiew
            };

            if (scope.user && statuses.hasOwnProperty(scope.user.type_id)) {
                element.addClass(statuses[scope.user.type_id]);
            } else {
                element.hide();
            }
        }
    }
});