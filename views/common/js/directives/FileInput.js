(function() {
	var upload = function(files, scope) {
		var el = scope.el;
		if (files.length == 0) {
			console.log('There is no files to upload');
			return;
		}
		var filterFunc = (scope.filter) ? (scope.filter) : function() {return true};
		FileAPI.filterFiles(
			files,
			function(file, info) {
				return filterFunc({file: file, info: info});
			},
			function(file) {
				if (file.length > 0) {
					var filesData = {};
					filesData[scope.name] = files;
					FileAPI.upload({
						url: scope.url,
						data: scope.data(),
						files: filesData,
						upload: function() {
							scope.$apply(function() {
								scope.onstart();
							});
						},
						fileupload: function(file) {
							scope.$apply(function() {
								scope.onstartfile({file: file});
							});
						},
						progress: function(evt) {
							scope.$apply(function() {
								var pr = evt.loaded/evt.total * 100;
								scope.onprogress({progress: pr});
							});
						},
						fileprogress: function(evt, file) {
							scope.$apply(function() {
								var pr = evt.loaded/evt.total * 100;
								scope.onprogressfile({file: file, progress: pr});
							});
						},
						complete: function(error, xhr, file) {
							scope.$apply(function() {
								var response = (xhr.responseText) ? xhr.responseText : error;
								try {
									response = angular.fromJson(response);
								} catch(e) {}

								if (error) {
									scope.onerror({error: response, xhr: xhr, file: file});
								} else {
									scope.oncomplete({message: response, xhr: xhr, file: file});
									$(el).val('');
								}
							});
						},
						filecomplete: function(error, xhr, file) {
							scope.$apply(function() {
								var response = (xhr.responseText) ? xhr.responseText : error;
								try {
									response = angular.fromJson(response);
								} catch(e) {}
								if (error) {
									scope.onerrorfile({error: response, xhr: xhr, file: file});
								} else {
									scope.oncompletefile({message: response, xhr: xhr, file: file});
								}
							});
						}
					});
				}
			}
		);
	};
	var checkAttributes = function(attrs) {
		/*
		 * check attributes
		 */
		if (!attrs.url) {
			console.log('Fileinput requires "url" attribute');
			return false;
		}
		if (!attrs.name) {
			console.log('Fileinput requires "name" attribute');
			return false;
		}
		return true;
	};
	var scope = {
		url:  '@',
		data: '&',
		name: '@',
		filter: '&',
		onstart: '&',
		onprogress: '&',
		oncomplete: '&',
		onerror: '&',
		onstartfile: '&',
		onprogressfile: '&',
		oncompletefile: '&',
		onerrorfile: '&'
	};
	module.directive('fileinput', function() {
		return {
			restrict: 'AE',
			scope: scope,
			link: function(scope, element, attrs) {
				if (!checkAttributes(attrs)) return;
				scope.el = element.get(0);
				/*
				 * инициализация загрузки файла
				 */
				FileAPI.event.on(scope.el, 'change', function(e) {
					var files = FileAPI.getFiles(e); // Retrieve file list
					upload(files, scope);
				});
				if (!attrs.filter) {
					scope.filter = function() {return true;}
				}
			}
		}
	});
	scope = angular.copy(scope);
	scope.onhover = '&';
	scope.onout = '&';
	scope.hoverclass = '@';
	module.directive('filedrop', function() {
		return {
			restrict: 'AE',
			scope: scope,
			link: function(scope, element, attrs) {
				if (!checkAttributes(attrs)) return;
				scope.el = element.get(0);
				/*
				 * инициализация загрузки файла
				 */
				FileAPI.event.dnd(scope.el, function(over) {
					scope.$apply(function() {
						if (over) {
							element.addClass(scope.hoverclass);
							scope.onhover();
						} else {
							element.removeClass(scope.hoverclass);
							scope.onout();
						}
					});
				}, function(files) {
					upload(files, scope);
				});
				if (!attrs.filter) {
					scope.filter = function() {return true;}
				}
			}
		}
	});
})();
