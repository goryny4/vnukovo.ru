/**
 * Created by Evgeny on 07.04.2014.
 */
module.directive('toggleToPlaylist', function (playlist) {
	'use strict';
	var link = function (scope, element, attrs) {
		var
			addIcon = attrs.addIcon,
			removeIcon = attrs.removeIcon,
			addTitle = attrs.addTitle,
			removeTitle = attrs.removeTitle;

		function setAttrs() {
			var
				id = scope.track.id,
				contains = playlist.contains(id);

			if (contains) {
				element.removeClass(addIcon);
				element.addClass(removeIcon);
				element.attr('title', removeTitle);
			} else {
				element.removeClass(removeIcon);
				element.addClass(addIcon);
				element.attr('title', addTitle);
			}
		}

		element.on('click', function () {
			if (scope.track) {
				var id = scope.track.id;
				if (!playlist.contains(id)) {
					playlist.add(scope.track);
				} else {
					playlist.remove(id);
				}
			}
		});

		scope.$watch('track', function (track) {
			if (!track) {
				return;
			}

			if (track.hasOwnProperty('$promise')) {
				track.$promise.then(function (track) {
					setAttrs(track.id);
				});
			} else if (track) {
				setAttrs(track.id);
			}
		});

		scope.$on('playlist.changed', setAttrs);
	};

	return {
		restrict: 'A',
		scope: {
			track: '=toggleToPlaylist'
		},
		link: link
	};
});