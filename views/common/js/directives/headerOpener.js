module.directive('headerOpener', ['$rootScope', 'headerState', function ($rootScope, headerState) {
	'use strict';
	return {
		restrict: 'E',
		template: '<div class="header_opener"></div>',
		replace: true,
		link: function (scope, element) {
			var states = headerState.states;
			element.on('click', function () {
				headerState.setState(states.opened);
			});
		}
	};
}]);