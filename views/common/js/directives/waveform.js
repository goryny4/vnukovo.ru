module.directive('waveform', function ($rootScope, jplayerInterface, TracksResource) {
		'use strict';

		var
			colors = {
				defaultColor: '#575757',
				loadedColor: '#ffffff',
				playedColor: '#bd5f23'
			},
			getWidth = function(track) {
				var titleDelta = 140 - _.max([track.project.name.length, track.name.length]) * 5;
				return 260 + (titleDelta > 0 ? titleDelta : 0);
			},
			link = function (scope, element, attrs) {

				var parent = element.parent(),
					waveform, streamOptions,
					updateWidth = attrs.hasOwnProperty('updateWidth') || false;

				waveform = new Waveform({
					container: element[0],
					width: parent.width(),
					height: parent.height()
				});
				streamOptions = waveform.optionsForSyncedStream(colors);
				streamOptions.whileloading.call(jplayerInterface.info);

				waveform.update = function (options) {
					var _update = Waveform.prototype.update;

					if (updateWidth && options.width) {
						var canvas = this.container.getElementsByTagName('canvas');
						if (canvas.length) {
							this.container.removeChild(canvas[0]);
						}

						this.container.parentNode.style.width = options.width + 'px'; // Hack
						this.canvas = this.createCanvas(this.container, options.width, this.height);
						this.context = this.canvas.getContext("2d");
						this.width = parseInt(this.context.canvas.width, 10);
					}

					_update.call(waveform, options);
				};


				scope.$watch(
					function () {
						return jplayerInterface.info;
					},
					function () {
						if (streamOptions) {
							streamOptions.whileplaying();
						}
					},
					true
				);

				function getWave(id, width) {
					TracksResource.getWave({id: id}).$promise.then(function (data) {
						waveform.update({
							data: data.points,
							width: width
						});
						streamOptions.whileplaying();
					});
				}

				scope.$on('jplayerInterface:trackUpdated', function ($e, track) {
					var width = getWidth(track);
					getWave(track.id, width);
				});

				var track = jplayerInterface.getCurrentTrack();
				if (track && track.id) {
					var width = getWidth(track);
					getWave(track.id, width);
				}
			};

		return {
			restrict: 'AE',
			link: link,
			template: '<div class="waveform"></div>'
		};
	}
)
;