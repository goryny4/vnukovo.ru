module.filter('dateToISO', function () {
    'use strict';
    return function(input) {
        var p = input.split(/[- :]/);
        /* new Date(year, month [, day, hour, minute, second, millisecond]); */
        return new Date(p[0], p[1]-1, p[2], p[3], p[4], p[5]);
    };
});