module.filter('age', function () {
    return function(date) {
        var start = moment(date),
            end = moment(),
            years = end.diff(start, 'year'),
            months = end.diff(start, 'month') % 12;

        if (years == 0) {
            years = undefined;
        } else if ((years + 10) % 10 == 1) {
            years = years + ' год'
        } else if ((years + 10) % 10 in [2, 3, 4]) {
            years = years + ' года'
        } else {
            years = years + ' лет'
        }

        if (months == 0) {
            months = undefined;
        } else if ((years + 10) % 10 == 1) {
            months = months + ' месяц'
        } else if ((years + 10) % 10 in [2, 3, 4]) {
            months = months + ' месяца'
        } else {
            months = months + ' месяцев'
        }

        return [years, months].join(' и ');
    }
});