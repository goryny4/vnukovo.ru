var module = angular.module('VnukovoAdminApp', ['ngResource', 'ngRoute', 'templates', 'ui.bootstrap', 'flow']);

module.config(['flowFactoryProvider', function (flowFactoryProvider) {
    flowFactoryProvider.defaults = {
        permanentErrors: [404, 500, 501],
        maxChunkRetries: 1,
        chunkRetryInterval: 5000,
        simultaneousUploads: 1,
        singleFile: true,
        testChunks:false
    };
    flowFactoryProvider.on('catchAll', function (event) {
        console.log('catchAll', arguments);
    });
    // Can be used with different implementations of Flow.js
    // flowFactoryProvider.factory = fustyFlowFactory;
}]);

module.config(function ($routeProvider) {
/* оставил для примера
    var playlistFields = [
            { title: 'Название', type: 'text', name: 'name'},
            { title: 'ID пользователя', type: 'text', name: 'userId'},
            { title: 'Публичный плейлист', type: 'checkbox', name: 'public'},
            { title: 'Описание', type: 'textarea', name: 'description'},
            { title: 'ID', type: 'text', readonly: true, name: 'id'},
            { title: 'Создан', type: 'text', readonly: true, name: 'created'},
            { title: 'Изменен', type: 'text', readonly: true, name: 'modified'}
        ]
        , playlistButtons = [
            [
                {title: 'Добавить треки', color: 'warning', click: "addRelatedData()"},
                {title: 'Скачать торрент', color: 'warning', href: 'cfg.download', global: true }
            ],
            [
                {title: 'Сохранить', color: 'success', click: 'saveItem()'},
                {title: 'Применить', color: 'primary', click: 'applyItem()'},
                {title: 'Отмена', color: 'danger', href: 'cfg.backUrl'}
            ]
        ]
        , playlistRelated = {
            field: 'tracks',
            name: 'Треки',
            title: 'Добавить треки в плейлист',
            url: 'api/tracks',
            search: true,
            fields: ['name', 'timelengthStr']
        };
    var serviceFields = [
        { title: 'Название услуги', type: 'text', name: 'name'},
        { title: 'Описание', type: 'textAngular', name: 'descr'},
        { title: 'Тип', type: 'text', name: 'type', defList: [
            { title: 'Один раз', value: 'once' },
            { title: 'Месяц', value: 'month' },
            { title: 'Момент', value: 'moment' }
        ]},
        { title: 'Для', type: 'text', name: 'for', defList: [
            { title: 'Проекта', value: 'project' },
            { title: 'Трека', value: 'track' }
        ]},
        { title: 'Минимальный срок', type: 'number', name: 'min_period' },
        { title: 'Картинка', type: 'text', name: 'link'},
        { title: 'Демо (количество дней)', type: 'number', name: 'demo'},
        { title: 'Контроллер/действие', type: 'text', name: 'controller'},
        { title: 'Кол-во месяцов - Цена', type: 'array', name: 'prices' },
        { title: 'ID', type: 'text', readonly: true, name: 'id'},
        { title: 'Создан', type: 'text', readonly: true, name: 'created'},
        { title: 'Изменен', type: 'text', readonly: true, name: 'modified'},
        { title: 'Подписчиков', type: 'number', readonly: true, name: 'users', _get: 'length' }
    ];

    var tariffFields = [
            { title: 'Название тарифа', type: 'text', name: 'name'},
            { title: 'Описание', type: 'textAngular', name: 'descr'},
            { title: 'Ссылка', type: 'text', name: 'picture'},
            { title: 'Количество проектов', type: 'number', name: 'projectCount' },
            { title: 'Демо (количество дней)', type: 'number', name: 'demo' },
            { title: 'Кол-во месяцов - Цена', type: 'array', name: 'prices' },
            { title: 'ID', type: 'text', readonly: true, name: 'id' },
            { title: 'Создан', type: 'text', readonly: true, name: 'created'},
            { title: 'Изменен', type: 'text', readonly: true, name: 'modified'},
            { title: 'Подписчиков', type: 'number', readonly: true, name: 'users', _get: 'length' }
        ]
        , tariffButtons = [
            [
                { title: 'Добавить услуги', color: 'warning', click: 'addRelatedData()'}
            ],
            [
                {title: 'Сохранить', color: 'success', click: 'saveItem()'},
                {title: 'Применить', color: 'primary', click: 'applyItem()'},
                {title: 'Отмена', color: 'danger', href: 'cfg.backUrl'}
            ]
        ]
        , tariffRelated = {
            field: 'services',
            name: 'Услуги',
            title: 'Добавить услуги в тариф',
            url: 'api/services',
            fields: ['name', 'price']
        }
        ;
    var contentFields = [
        { title: 'Метка', type:"text", name:"label"},
        { title: 'Контент', type:"textAngular", name:"content"},
        { title: 'ID', type: 'text', readonly: true, name: 'id' },
    ];
*/
    var newsFieldsList = [
            { title: 'Id', readonly: true, name: 'id' },
            { title: 'Дата Публикации', type:"calendar", name: 'published_at'},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Заголовок', type:"text", name: 'title_ru'}
            /*   ,
             { title: 'Автор', type:"text", readonly: true, name: 'creator_id'}
             */
        ]
        , newsFieldsForm = [
            { title: 'Дата публикации', type:"calendar", name: 'published_at'},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка EN', type:"image", name: 'image_en'},

            { title: 'Заголовок', type:"text", name: 'title_ru'},
            { title: 'Текст', type:"textAngular", name: 'bbcode_ru'},

            { title: 'Заголовок EN', type:"text", name: 'title_en'},
            { title: 'Текст EN', type:"textAngular", name: 'bbcode_en'},

            { title: 'Id', readonly: true, name: 'id' },
            //    { title: 'Автор', type:"text", readonly: true, name: 'creator_id'},
            { title: 'Дата создания', readonly: true, type:"text", name: 'created_at'},
            { title: 'Дата изменения', readonly: true, type:"text", name: 'updated_at'}
        ];

    var cmsFieldsList = [
        { title: 'Id', readonly: true, name: 'id' },
        { title: 'Секция', type:"text", name: 'section'},
        { title: 'Главная страница', type:"checkbox", name: 'is_primary'},
  //      { title: 'Картинка', type:"image", name: 'image'},
        { title: 'Заголовок', type:"text", name: 'title'}
        /*   ,
         { title: 'Автор', type:"text", readonly: true, name: 'creator_id'}
         */
    ];

    function getCmsFieldsForm(type) {
        var howToGetSections = [
            { value: 'car', title: 'Автомобиль'},
            { value: 'aeroexpress', title: 'Аэроэкспресс'},
            { value: 'bus', title: 'Автобус'},
            { value: 'minibus', title: 'Маршрутное такси'},
            { value: 'order-taxi', title: 'Заказать такси'}
        ], servicesSections = [
            { value: 'vip', title: 'VIP'},
            { value: 'police', title: 'Полиция'},
            { value: 'emergency-assistance', title: 'Медицинская помощь'},
            { value: 'passengers-with-disabilities', title: 'Люди с ограниченными возможностями'},
            { value: 'passengers-with-children', title: 'Пассажиры с детьми'},
            { value: 'express-services', title: 'Экспресс-услуги'},
            { value: 'check-in-and-boarding', title: 'Регистрация на рейс'},
            { value: 'internet', title: 'Интернет'}
        ];

        return [
            { title: 'Секция', type: 'text', name: 'section', defList: ((type=='how-to-get')?howToGetSections:servicesSections)},
            { title: 'Основная страница секции', type: 'checkbox', name: 'is_primary'},

            { title: 'Заголовок', type:"text", name: 'title_ru'},
            { title: 'Заголовок EN', type:"text", name: 'title_en'},

            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка EN', type:"image", name: 'image_en'},

            { title: 'Текст', type:"textAngular", name: 'bbcode_ru'},
            { title: 'Текст EN', type:"textAngular", name: 'bbcode_en'},

            { title: 'Id', readonly: true, name: 'id' },
            //    { title: 'Автор', type:"text", readonly: true, name: 'creator_id'},
            { title: 'Дата создания', readonly: true, type:"text", name: 'created_at'},
            { title: 'Дата изменения', readonly: true, type:"text", name: 'updated_at'}
        ];
    }


    var saveApplyCancelButtons = [
        [
            { title: 'Сохранить', color: 'success', click: 'saveItem()'},
            { title: 'Применить', color: 'primary', click: 'applyItem()'},
            { title: 'Отмена', color: 'danger', href: 'cfg.backUrl'}
        ]
    ];

    var adFullFieldsList = [
            { title: 'Id', name: 'id' },
            { title: 'Название', type:"text", name: 'name'},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'}
        ]
        , adFullFieldsForm = [
            { title: 'Id', name: 'id', readonly: true},
            { title: 'Название', type:"text", name: 'name'},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'},
            { title: 'Картинка En', type:"image", name: 'image_en'},
            { title: 'Cсылка En', type:"text", name: 'target_url_en'}

        ];


    var adLinkFieldsList = [
            { title: 'Id', name: 'id' },
            { title: 'Название', type:"text", name: 'name'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'}
        ]
        , adLinkFieldsForm = [
            { title: 'Ссылка-название', type:"text", name: 'name'},
            { title: 'Cсылка Ру', type:"text", name: 'target_url_ru'},
            { title: 'Cсылка En', type:"text", name: 'target_url_en'}

        ];


    var adBlockFieldsList = [
            { title: 'Id', name: 'id' },
            { title: 'Название', type:"text", name: 'name' },
            { title: 'Расположение', type:"text", name: 'location_id', middle: {
                '1': 'Новости',
                '2': 'Duty Free',
                '3': 'Кафе и рестораны',
                '4': 'Прокат Авто'
            }},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'}
        ]
        , adBlockFieldsForm = [
            { title: 'Название', type:"text", name: 'name'},
            { title: 'Расположение', type:"text", name: 'location_id', defList:[
                { title: 'Новости', value: '1' },
                { title: 'Duty Free', value: '2' },
                { title: 'Кафе и рестораны', value: '3' },
                { title: 'Прокат Авто', value: '4' }
            ]},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка En', type:"image", name: 'image_en'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'},
            { title: 'Cсылка En', type:"text", name: 'target_url_en'}

        ];

    var adStatisticsFieldsList = [
            { title: 'Id', name: 'id' },
            { title: 'Название', type:"text", name: 'name' },
            { title: 'Тип', type:"text", name: 'type', middle: {
                'full': 'Полноэкранная реклама',
                'block': 'Рекламный блок',
                'link': 'Ссылка'
            }},
            { title: 'Расположение', type:"text", name: 'location_id', middle: {
                '0': '-',
                '1': 'Новости',
                '2': 'Duty Free',
                '3': 'Кафе и рестораны',
                '4': 'Прокат Авто'
            }},
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Cсылка', type:"text", name: 'target_url_ru'},
            { title: 'Просмотров', type:"text", name: 'times_shown'},
            { title: 'Переходов по ссылке', type:"text", name: 'times_opened'}
        ]
        ;

    var commercialFieldsList = [
            { title: 'Id', type:"text", readonly: true, name: 'id' },
            //   { title: 'Логотип', type:"image", name: 'image_file_name'},
            { title: 'Название', type:"text", name: 'name_ru'},
            { title: 'Терминал', type:"text", name: 'terminal'},
            { title: 'Этаж', type:"text", name: 'floor'},
        ]
        , commercialFieldsForm = [
//            { title: 'Логотип', type:"image", name: 'image_file_name'},

            { title: 'Название', type:"text", name: 'name_ru'},
            { title: 'Название (En)', type:"text", name: 'name_en'},
            { title: 'Описание', type:"textAngular", name: 'description_ru'},
            { title: 'Описание (En)', type:"textAngular", name: 'description_en'},
            { title: 'Терминал', type:"text", name: 'terminal', defList: [
                { title: 'A', value: 'A' },
                { title: 'B', value: 'B' }
            ]},
            { title: 'Этаж', type:"text", name: 'floor', defList: [
                { title: '1', value: '1' },
                { title: '2', value: '2' }
            ]},
            { title: 'Начало работы', type:"text", name: 'time_start'},
            { title: 'Завершение работы', type:"text", name: 'time_end'},
      //      { title: 'Широта', type:"text", name: 'centre_lat'},
      //      { title: 'Долгота', type:"text", name: 'centre_lon'},

            { title: 'Id', readonly: true, name: 'id' },

            { title: 'Создано', type: 'text', name: 'created_at', readonly: true },
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true }
        ]
        , commercialButtons = [
            [
       //         { title: 'Добавить специальное предложение', color: 'warning', click: 'addNewRelatedData()'},
      //          { title: 'Добавить отметку на карте', color: 'warning', click: 'addNewRelatedData()'}
            ],
            [
                { title: 'Сохранить', color: 'success', click: 'saveItem()'},
                { title: 'Применить', color: 'primary', click: 'applyItem()'},
                { title: 'Отмена', color: 'danger', href: 'cfg.backUrl'}
            ]
        ]
        ;
    var userFields = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Email', type: 'text', name: 'email' },
            { title: 'Пароль', type: 'text', name: 'password' },
            { title: 'Имя', type: 'text', name: 'first_name' },
            { title: 'Фамилия', type: 'text', name: 'last_name' },
            { title: 'Социальная сеть', type: 'text', name: 'social_network',  defList: [
                { title: 'Facebook', value: 'fb' },
                { title: 'VK', value: 'vk' }
            ]},
            { title: 'ID в социальной сети', type: 'text', name: 'social_network_id' },
            { title: 'Создано', type: 'text', name: 'created_at', readonly: true },
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true }
        ]
        , userFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Email', type: 'text', name: 'email' },
            { title: 'Имя', type: 'text', name: 'first_name' },
            { title: 'Фамилия', type: 'text', name: 'last_name' },
            { title: 'Социальная сеть', type: 'text', name: 'social_network'},
            { title: 'ID в социальной сети', type: 'text', name: 'social_network_id' },
            { title: 'Создано', type: 'text', name: 'created_at', readonly: true },
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true }

        ];

    var moderFields = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Имя пользователя', type: 'text', name: 'username' },
            { title: 'Пароль', type: 'text', name: 'password' },
            { title: 'Группа', type: 'text', name: 'group',  defList: [
                { title: 'Контент, Реклама', value: 'content' },
                { title: 'Статистика', value: 'analyst' }
            ]},
            { title: 'Создано', type: 'text', name: 'created_at', readonly: true },
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true }
        ]
        , moderFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Имя пользователя', type: 'text', name: 'username' },
            { title: 'Группа', type: 'text', name: 'group', middle:{
                'analyst' : "Статистика",
                'content' : 'Контент, Реклама'
            }},
            { title: 'Последний раз заходил', type: 'text', name: 'last_login_date' },
            { title: 'Последний ip', type: 'text', name: 'last_ip' },
            { title: 'Создано', type: 'text', name: 'created_at', readonly: true },
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true }

        ]
        , moderButtons = [
            /* [
             { title: 'Добавить контент', color: 'warning', click: 'addRelatedData()'}
             ],*/
            [
                { title: 'Сохранить', color: 'success', click: 'saveItem()'},
                { title: 'Применить', color: 'primary', click: 'applyItem()'},
                { title: 'Отмена', color: 'danger', href: 'cfg.backUrl'}
            ]
        ]
        ;
    var specialProposalsFields = [
        { title: 'Название', type: 'text', name: 'title_ru' },
        { title: 'Описание', type: 'text', name: 'description_ru' }
    ];

    var specialProposals = {
            field: 'special_proposals',
            name: 'Специальные предложения',
            title: 'Добавить специальное предложение',
            url: 'ang/special-proposals/',
            fieldsList:  ['title_ru','description_ru'],
            fieldsForm : specialProposalsFields
        },
        mapMarkers = {
            field: 'map-markers',
            name: 'Отметки на карте',
            title: 'Добавить отметку на карте',
            url: 'ang/map-markers',
            fields: ['terminal', 'floor', 'long', 'lat']
        };
    var logFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Операция', type: 'text', name: 'operation', middle:{
                'create' : 'Создание',
                'update' : 'Изменение',
                'delete' : 'Удаление'
            }},
            { title: 'Категория', type: 'text', name: 'category', middle:{
                'news' : 'Новости',
                'user' : 'Пользователи',
                'duty-free' : 'Duty Free',
                'cafes-and-restaurants' : 'Кафе и рестораны',
                'car-rent' : 'Прокат авто'
            }},
            { title: 'ID объекта', type: 'text', name: 'entity_id' },
            { title: 'Модератор', type: 'text', name: 'moder_name' },
            { title: 'Группа', type: 'text', name: 'moder_group', middle:{
                'analyst' : 'Статистика',
                'content' : 'Контент, Реклама',
                'admin' : 'Администрация',
                'superadmin' : 'Разработка Сайта'
            }},
            { title: 'ID модератора', type: 'text', name: 'moder_id' },
            { title: 'IP запроса', type: 'text', name: 'request_ip' },
            { title: 'Время', type: 'text', name: 'created_at', readonly: true }

        ]
    ;

    var versions = [
        { title: '1', value: '1' },
        { title: '2', value: '2' },
        { title: '3', value: '3' },
        { title: '4', value: '4' },
        { title: '5', value: '5' },
        { title: '6', value: '6' },
        { title: '7', value: '7' },
        { title: '8', value: '8' },
        { title: '9', value: '9' },
        { title: '10', value: '10' },
        { title: '11', value: '11' },
        { title: '12', value: '12' },
        { title: '13', value: '13' },
        { title: '14', value: '14' },
        { title: '15', value: '15' },
        { title: '16', value: '16' },
        { title: '17', value: '17' },
        { title: '18', value: '18' },
        { title: '19', value: '19' },
        { title: '20', value: '20' },
        { title: '21', value: '21' },
        { title: '22', value: '22' },
        { title: '23', value: '23' },
        { title: '24', value: '24' }
    ];

    var mapsFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Версия', type: 'text', name: 'version' },
            { title: 'Терминал', type: 'text', name: 'terminal'},
            { title: 'Этаж', type: 'text', name: 'floor'},
            { title: 'Изменено', type: 'text', name: 'updated_at', readonly: true },
            { title: 'Загрузил', type: 'text', name: 'moder_name', readonly: true  }
        ]
        , mapsFieldsForm = [
            { title: 'Id', readonly: true, name: 'id' },

            { title: 'Версия', type:"text", name: 'version', defList: versions},

            { title: 'Терминал', type:"text", name: 'terminal', defList: [
                { title: 'A', value: 'A' },
                { title: 'B', value: 'B' }
            ]},
            { title: 'Этаж', type:"text", name: 'floor', defList: [
                { title: '1', value: '1' },
                { title: '2', value: '2' }
            ]},

            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка EN', type:"image", name: 'image_en'},

            //  { title: 'Автор', type:"text", readonly: true, name: 'moder_id'},
            { title: 'Дата создания', readonly: true, type:"text", name: 'created_at'},
            { title: 'Дата изменения', readonly: true, type:"text", name: 'updated_at'}
        ];


    var citiesFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Город', type: 'text', name: 'city_en' },
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Версия', type: 'text', name: 'version' },
        ]
        , citiesFieldsForm = [
            { title: 'Id', readonly: true, name: 'id' },
            { title: 'Город', type: 'text', name: 'city_en' },


            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка EN', type:"image", name: 'image_en'},
            { title: 'Версия', type:"text", name: 'version', defList: versions},

            //  { title: 'Автор', type:"text", readonly: true, name: 'moder_id'},
            { title: 'Дата создания', readonly: true, type:"text", name: 'created_at'},
            { title: 'Дата изменения', readonly: true, type:"text", name: 'updated_at'}
        ];


    var airlinesFieldsList = [
            { title: 'ID', type: 'text', name: 'id', readonly: true },
            { title: 'Авиалиния', type: 'text', name: 'airline_name_rus' },
            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Версия', type: 'text', name: 'version' }
        ]
        , airlinesFieldsForm = [
            { title: 'Id', readonly: true, name: 'id' },
            
            { title: 'Авиалиния', type: 'text', name: 'airline_name_rus' },

            { title: 'Версия', type:"text", name: 'version', defList: versions},

            { title: 'Картинка', type:"image", name: 'image_ru'},
            { title: 'Картинка EN', type:"image", name: 'image_en'},

            //  { title: 'Автор', type:"text", readonly: true, name: 'moder_id'},
            { title: 'Дата создания', readonly: true, type:"text", name: 'created_at'},
            { title: 'Дата изменения', readonly: true, type:"text", name: 'updated_at'}
        ];


    $routeProvider.
        when('/', {
            templateUrl: 'adm/partials/index.html',
            controller: 'welcomePageCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Добро пожаловать!'
                    };
                }
            }
        }).
        when('/news', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Новости',
                        url: 'ang/news',
                        breadcrumb: {news:'Новости'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/news/new'}
                        ],
                        fields: newsFieldsList
                    };
                }
            }
        }).
        when('/news/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить новость',
                        url: 'ang/news/0',
                        breadcrumb: {news:'Новости',"news/new":'Добавить новость'},
                        fields: newsFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/news/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать новость',
                        url: 'ang/news/' + $route.current.params.id,
                        breadcrumb: {news:'Новости',"news/new":'Редактировать новость'},
                        fields: newsFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).


        when('/how-to-get', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Как добраться',
                        url: 'ang/how-to-get',
                        breadcrumb: {"how-to-get":'Как добраться'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/how-to-get/new'}
                        ],
                        fields: cmsFieldsList
                    };
                }
            }
        }).
        when('/how-to-get/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить страницу',
                        url: 'ang/how-to-get/0',
                        breadcrumb: {"how-to-get":'Как добраться',"how-to-get/new":'Добавить страницу'},
                        fields: getCmsFieldsForm('how-to-get'),
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/how-to-get/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать страницу',
                        url: 'ang/how-to-get/' + $route.current.params.id,
                        breadcrumb: {"how-to-get":'Как добраться',"how-to-get/new":'Редактировать страницу'},
                        fields: getCmsFieldsForm('how-to-get'),
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).




        when('/services', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Услуги',
                        url: 'ang/services',
                        breadcrumb: {"services":'Услуги'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/services/new'}
                        ],
                        fields: cmsFieldsList
                    };
                }
            }
        }).
        when('/services/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить страницу',
                        url: 'ang/services/0',
                        breadcrumb: {"services":'Услуги',"services/new":'Добавить страницу'},
                        fields: getCmsFieldsForm('services'),
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/services/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать страницу',
                        url: 'ang/services/' + $route.current.params.id,
                        breadcrumb: {"services":'Услуги',"services/new":'Редактировать страницу'},
                        fields: getCmsFieldsForm('services'),
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).




        when('/maps', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Карты',
                        url: 'ang/maps',
//                        disableOperations: true,
                        disableDelete: true,
                        breadcrumb: {maps: 'Карты'},
                        buttons: [],
                        fields: mapsFieldsList
                    };
                }
            }
        }).

        when('/maps/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать карту',
                        url: 'ang/maps/' + $route.current.params.id,
                        breadcrumb: {maps:'Карты',"maps":'Редактировать карту'},
                        fields: mapsFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).



        when('/cities', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Города',
                        url: 'ang/cities',
//                        disableOperations: true,
                        disableDelete: true,
                        breadcrumb: {cities: 'Города'},
                        buttons: [],
                        fields: citiesFieldsList
                    };
                }
            }
        }).

        when('/cities/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать город',
                        url: 'ang/cities/' + $route.current.params.id,
                        breadcrumb: {cities:'Города',"cities":'Редактировать города'},
                        fields: citiesFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).


        when('/airlines', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Авиалинии',
                        url: 'ang/airlines',
//                        disableOperations: true,
                        disableDelete: true,
                        breadcrumb: {airlines: 'Авиалинии'},
                        buttons: [],
                        fields: airlinesFieldsList
                    };
                }
            }
        }).

        when('/airlines/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать город',
                        url: 'ang/airlines/' + $route.current.params.id,
                        breadcrumb: {airlines:'Авиалинии',"airlines":'Редактировать авиалинию'},
                        fields: airlinesFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).



        /* Fullscreen Ads */
        when('/fullscreen-ads', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Полноэкранная реклама',
                        url: 'ang/ads?type=full',
                        breadcrumb: {"ads-full": 'Полноэкранная реклама'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/fullscreen-ads/new'}
                        ],
                        fields: adFullFieldsList
                    };
                }
            }
        }).
        when('/fullscreen-ads/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить полноэкранную рекламу',
                        url: 'ang/ads/0?type=full',
                        breadcrumb: {"ads-full" :'Полноэкранная реклама',"ads-full/new":'Добавить полноэкранную рекламу'},
                        fields: adFullFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/fullscreen-ads/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать полноэкранную рекламу',
                        url: 'ang/ads/' + $route.current.params.id,
                        breadcrumb: ['Полноэкранная реклама','Редактировать полноэкранную рекламу'],
                        fields: adFullFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).
        /* Ads block */
        when('/ads-block', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Рекламные блоки',
                        url: 'ang/ads?type=block',
                        breadcrumb: {"ads-block": 'Рекламные блоки'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/ads-block/new'}
                        ],
                        fields: adBlockFieldsList
                    };
                }
            }
        }).
        when('/ads-block/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить рекламный блок',
                        url: 'ang/ads/0?type=block',
                        breadcrumb: {"ads-block" :'Рекламные блоки',"ads-block/new": 'Добавить рекламный блок'},
                        fields: adBlockFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/ads-block/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать рекламный блок',
                        url: 'ang/ads/' + $route.current.params.id,
                        breadcrumb: ['Рекламный блок','Редактировать рекламный блок'],
                        fields: adBlockFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).
        /* Link Ads */
        when('/link-ads', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Рекламные ссылки',
                        url: 'ang/ads?type=link',
                        breadcrumb: {"ads-block": 'Рекламные ссылки'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/link-ads/new'}
                        ],
                        fields: adLinkFieldsList
                    };
                }
            }
        }).
        when('/link-ads/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить рекламную ссылку',
                        url: 'ang/ads/0?type=link',
                        breadcrumb: {"link-ads" :'Рекламные ссылки',"link-ads/new": 'Добавить рекламную ссылку'},
                        fields: adLinkFieldsList,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/link-ads/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать рекламную ссылку',
                        url: 'ang/ads/' + $route.current.params.id,
                        breadcrumb: ['Рекламная ссылка','Редактировать рекламную ссылку'],
                        fields: adLinkFieldsForm,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).


        /* Duty Free */
        when('/duty-free', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Магазины Duty Free',
                        url: 'ang/commercials?type=duty-free',
                        breadcrumb: {'duty-free':'Duty Free'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/duty-free/new'}
                        ],
                        fields: commercialFieldsList
                    };
                }
            }
        }).
        when('/duty-free/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить магазин Duty Free',
                        url: 'ang/commercials/0?type=duty-free',
                        breadcrumb: {'duty-free':'Duty Free',"duty-free/new":'Добавить Duty Free'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons
                    };
                }
            }
        }).
        when('/duty-free/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать магазин Duty Free',
                        url: 'ang/commercials/' + $route.current.params.id,
                        breadcrumb: {'duty-free':'Duty Free', '' : 'Редактировать Duty Free'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons,
                        related: specialProposals
                    };
                }]
            }
        }).

        /* Cafes and restaurants */
        when('/cafes-and-restaurants', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Кафе и рестораны',
                        url: 'ang/commercials?type=cafes-and-restaurants',
                        breadcrumb: {'cafes-and-restaurants':'Кафе и рестораны'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/cafes-and-restaurants/new'}
                        ],
                        fields: commercialFieldsList
                    };
                }
            }
        }).
        when('/cafes-and-restaurants/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить кафе / ресторан',
                        url: 'ang/commercials/0?type=cafes-and-restaurants',
                        breadcrumb: {'cafes-and-restaurants':'Кафе и рестораны',"cafes-and-restaurants/new":'Добавить Duty Free'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons
                    };
                }
            }
        }).
        when('/cafes-and-restaurants/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать кафе / ресторан',
                        url: 'ang/commercials/' + $route.current.params.id,
                        breadcrumb: {'cafes-and-restaurants':'Кафе и рестораны', '' : 'Редактировать кафе/ресторан'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons
                    };
                }]
            }
        }).

        /* Car Rent */
        when('/car-rent', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Прокат авто',
                        url: 'ang/commercials?type=car-rent',
                        breadcrumb: {'car-rent':'Прокат авто'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/car-rent/new'}
                        ],
                        fields: commercialFieldsList
                    };
                }
            }
        }).
        when('/car-rent/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Добавить салон',
                        url: 'ang/commercials/0?type=car-rent',
                        breadcrumb: {'car-rent':'Прокат авто','car-rent/new' : 'Добавить Салон'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons
                    };
                }
            }
        }).
        when('/car-rent/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать салон',
                        url: 'ang/commercials/' + $route.current.params.id,
                        breadcrumb: {'car-rent':'Прокат авто','' : 'Редактировать Салон'},
                        fields: commercialFieldsForm,
                        buttons: commercialButtons
                    };
                }]
            }
        }).


        /* Cafes and restaurants */
        when('/special-proposals', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Специальные предложения',
                        url: 'ang/special-proposals',
                        breadcrumb: {'special-proposals':'Специальные предложения'},
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/special-proposals/new'}
                        ],
                        fields: specialProposalsFields
                    };
                }
            }
        }).
        when('/special-proposals/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Создать специальное предложение',
                        url: 'ang/special-proposals/0',
                        breadcrumb: {'special-proposals':'Специальные предложения','special-proposals/new':'Создать специальное предложение'},
                        fields: specialProposalsFields,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/special-proposals/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактировать специальное предложение',
                        url: 'ang/special-proposals/' + $route.current.params.id,
                        breadcrumb: {'special-proposals':'Специальные предложения','special-proposals':'Редактировать специальное предложение'},
                        fields: specialProposalsFields,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).
        // Logs
        when('/logs', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Новости',
                        url: 'ang/logs',
                        disableOperations: true,
                        breadcrumb: {logs: 'История операций'},
                        fields: logFieldsList
                    };
                }
            }
        }).
        when('/ads-list', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Рекламные показы',
                        url: 'ang/ads-list',
                        disableOperations: true,
                        breadcrumb: { 'ads-list' : 'Рекламные показы'},
                        fields: adStatisticsFieldsList
                    };
                }
            }
        }).
        /* Playlists */
        when('/playlists', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Плейлисты',
                        breadcrumb: ['Плейлисты'],
                        download: '/api/playlists/download/',
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/playlists/new'}
                        ],
                        url: 'api/playlists',
                        fields: [
                            { title: 'Название', type: 'text', name: 'name', href: '"#"+cfg.itemUrl+"/"'},
                            {title: 'ID', type: 'number', name: 'id'}
                        ]
                    };
                }
            }
        }).
        when('/playlists/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Создать плейлист',
                        url: 'api/playlists/0',
                        breadcrumb: ['Плейлисты', 'Новый плейлист'],
                        fields: playlistFields,
                        buttons: playlistButtons,
                        related: playlistRelated
                    };
                }
            }
        }).
        when('/playlists/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактирование плейлиста',
                        url: 'api/playlists/' + $route.current.params.id,
                        download: '/api/playlists/download/' + $route.current.params.id,

                        breadcrumb: ['Плейлисты', 'Редактирование плейлиста'],
                        fields: playlistFields,
                        buttons: playlistButtons,

                        related: playlistRelated
                    };
                }]
            }
        }).
        /* Треки */
        when('/tracks', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Треки',
                        searchField: 'Название трека',
                        url: 'api/tracks',
                        fields: [
                            { title: 'Название', name: 'name', href: '"#"+cfg.itemUrl+"/"' },
                            { title: 'ID', name: 'id'}
                        ]
                    };
                }
            }
        }).
        when('/tracks/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Добавление услуги',
                        breadcrumb: ['Платные услуги', 'Новая услуга'],
                        url: 'api/services/0',
                        fields: serviceFields,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/tracks/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактирование услуги',
                        breadcrumb: ['Платные услуги', 'Редактирование услуги'],
                        url: 'api/services/' + $route.current.params.id,
                        fields: serviceFields,
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).
        /* Услуги
         when('/services', {
         templateUrl: 'adm/partials/crud.html',
         controller: 'CollectionCtrl',
         resolve: {
         CRUDConfig: function () {
         return {
         title: 'Список услуг',
         breadcrumb: ['Платные услуги'],
         url: 'api/services',
         buttons: [
         {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
         {title: 'Создать', color: 'success', href: '#/services/new'}
         ],
         fields: [
         { title: 'Название услуги', type: 'text', name: 'name', href: '"#"+cfg.itemUrl+"/"' },
         { title: 'Тип', type: 'text', middle: { once: 'Один раз', month: 'Месяц'}, name: 'type'},
         { title: 'Подписчиков', type: 'text', name: 'users', _get: 'length'},
         { title: 'ID', type: 'number', name: 'id'}
         ]
         };
         }
         }
         }).
         when('/services/new', {
         templateUrl: 'adm/partials/crud.html',
         controller: 'RowCtrl',
         resolve: {
         CRUDConfig: function () {
         return {
         title: 'Добавление услуги',
         breadcrumb: ['Платные услуги', 'Новая услуга'],
         url: 'api/services/0',
         fields: serviceFields,
         buttons: serviceButtons
         };
         }
         }
         }).
         when('/services/:id', {
         templateUrl: 'adm/partials/crud.html',
         controller: 'RowCtrl',
         resolve: {
         CRUDConfig: ['$route', function ($route) {
         return {
         title: 'Редактирование услуги',
         breadcrumb: ['Платные услуги', 'Редактирование услуги'],
         url: 'api/services/' + $route.current.params.id,
         fields: serviceFields,
         buttons: serviceButtons
         };
         }]
         }
         }). */
        /* Тарифы */
        when('/tariffs', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Список тарифов',
                        breadcrumb: ['Тарифы'],
                        url: 'api/tariffs',
                        buttons: [
                            {title: 'Удалить', color: 'danger', click: "deleteItemsList()"},
                            {title: 'Создать', color: 'success', href: '#/tariffs/new'}
                        ],
                        fields: [
                            {title: 'Название тарифа', type: 'text', name: 'name', href: '"#"+cfg.itemUrl+"/"' },
                            { title: 'Количество проектов', type: 'number', name: 'projectCount' },
                            { title: 'ID', type: 'number', name: 'id'}
                        ]
                    };
                }
            }
        }).
        when('/tariffs/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Добавление тарифа',
                        breadcrumb: ['Тарифы', 'Новый тариф'],
                        url: 'api/tariffs/0',
                        fields: tariffFields,
                        buttons: tariffButtons,
                        related: tariffRelated
                    };
                }
            }
        }).
        when('/tariffs/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактирование тарифа',
                        breadcrumb: ['Тарифы', 'Редактирование тарифа'],
                        url: 'api/tariffs/' + $route.current.params.id,
                        fields: tariffFields,
                        buttons: tariffButtons,
                        related: tariffRelated
                    };
                }]
            }
        }).
        when('/history', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        title: 'Список операций',
                        breadcrumb: ['История операций'],
                        url: 'api/moneylog?admin=true',
                        searchByField: 'purchase',
                        disableOperations: true,
                        buttons: [],
                        fields: [
                            { title: 'ID', type: 'number', name: 'id'},
                            { title: 'Операция', type: 'execute', name: 'getOperation' },
                            { title: 'Количество денег', type: 'number', name: 'moneyAmount' },
                            { title: 'Дата', type: 'text', name: 'date' }
                        ]
                    }
                }
            }
        }).
        /* Пользователи */
        when('/users', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        url: 'ang/users',
                        title: 'Список пользователей',
                        breadcrumb: ['Пользователи'],
                        buttons: [
                            {title: 'Удалить', color: 'danger',  click: 'deleteItemsList()'},
                            {title: 'Создать', color: 'success', href: '#/users/new'}
                        ],
                        fields: userFieldsList
                    }
                }
            }
        }).
        when('/users/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Создать пользователя',
                        url: 'ang/users/0',
                        breadcrumb: {users:'Пользователи',"users/new":'Создать пользователя'},
                        fields: userFields,
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/users/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    //    var $bread = 'users/' + $route.current.params.id;
                    return {
                        url: 'ang/users/' + $route.current.params.id,
                        title: 'Редактировать пользователя',
                        breadcrumb: {users:'Пользователи',"users/":'Редактировать пользователя'},
                        fields: userFields,
                        buttons: saveApplyCancelButtons
                    }
                }
            }
        }).
        /* Модераторы */
        when('/moders', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function () {
                    return {
                        url: 'ang/moders',
                        title: 'Аккаунты',
                        breadcrumb: ['Доступ к админке'],
                        buttons: [
                            {title: 'Удалить', color: 'danger',  click: 'deleteItemsList()'},
                            {title: 'Создать', color: 'success', href: '#/moders/new'}
                        ],
                        fields: moderFieldsList
                    }
                }
            }
        }).
        when('/moders/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        title: 'Создать аккаунт',
                        url: 'ang/moders/0',
                        breadcrumb: {moders:'Доступ к админке',"moders/new":'Создать аккаунт'},
                        fields: moderFields,
                        buttons: moderButtons
                    };
                }
            }
        }).
        when('/moders/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: function ($route) {
                    return {
                        url: 'ang/moders/' + $route.current.params.id,
                        title: 'Управление аккаунтами',
                        breadcrumb: {"moders":'Доступ к админке', "moders/":'Редактировать аккаунт'},
                        fields: moderFields,
                        buttons: moderButtons
                    }
                }
            }
        }).
        when('/mails', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'CollectionCtrl',
            resolve: {
                CRUDConfig: function(){
                    return {
                        url: 'api/view',
                        title: 'Шаблоны',
                        searchByField: 'descr',
                        breadcrumb: ['Настройка шаблонов'],
                        fields: [
                            { title: 'Код', type: 'text', name: 'code', href: '"#"+cfg.itemUrl+"/"' },
                            { title: 'Описание', type: 'text', name: 'descr' },
                            { title: 'Тема', type: 'text', name: 'subject' }
                        ],
                        buttons: [{title:'Создать',color:'success',href:'#/templates/new'}]
                    }
                }
            }
        }).
        when('/mails/new', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig:  function () {
                    return {
                        title: 'Редактирование шаблона',
                        breadcrumb:['Шаблоны', 'Редактирование шаблона'],
                        url: 'api/view/0',

                        fields: [
                            { title: 'Код', type: 'text', name: 'code' },
                            { title: 'Описание', type: 'text', name: 'descr' },
                            { title: 'ID', type: 'text', readonly: true, name: 'id'},
                            { title: 'Макросы', type: 'textarea', name: 'macros' },
                            { title: 'ID языка', type: 'number', name: 'lang_id' },
                            { title: 'Тема', type: 'text', name: 'subject' },
                            { title: 'От адреса', type: 'text', name: 'fromaddress' },
                            { title: 'Тело', type: 'textAngular', name: 'message' }
                        ],
                        buttons: saveApplyCancelButtons
                    };
                }
            }
        }).
        when('/mails/:id', {
            templateUrl: 'adm/partials/crud.html',
            controller: 'RowCtrl',
            resolve: {
                CRUDConfig: ['$route', function ($route) {
                    return {
                        title: 'Редактирование шаблона',
                        breadcrumb:['Шаблоны', 'Редактирование шаблона'],
                        url: 'api/view/' + $route.current.params.id,
                        fields: [
                            { title: 'Код', type: 'text', name: 'code' },
                            { title: 'Описание', type: 'text', name: 'descr' },
                            { title: 'ID', type: 'text', readonly: true, name: 'id'},
                            { title: 'Макросы', type: 'textarea', name: 'macros' },
                            { title: 'ID языка', type: 'number', name: 'lang_id' },
                            { title: 'Тема', type: 'text', name: 'subject' },
                            { title: 'От адреса', type: 'text', name: 'fromaddress' },
                            { title: 'Тело', type: 'textAngular', name: 'message' }
                        ],
                        buttons: saveApplyCancelButtons
                    };
                }]
            }
        }).

        otherwise({
            redirectTo: '/',
            templateUrl: 'adm/partials/index.html'
        });

});

module.filter('getById', function () {
    return function (input, id) {
        var i = 0, len = input.length;
        for (; i < len; i++) {
            if (+input[i].id == +id) {
                return input[i];
            }
        }
        return null;
    }
});

module.directive('scroll', ['$timeout', function ($t) {
    return {

        link: function (scope, elm, attrs) {

            var raw = elm[0];
            scope.page = 1;
            scope.validate = function (length) {
                $t(function () {
                    if (!(raw.scrollHeight > raw.offsetHeight) && length) {
                        scope.page++;
                        scope.validate = function () {
                        };
                        scope.$eval(attrs.scroll, scope);
                    }
                }, 300);
            };
            scope.$eval(attrs.scroll, scope);

            elm.bind('scroll', function () {
                if (raw.scrollTop + raw.offsetHeight + 5 >= raw.scrollHeight && !scope.$eval(attrs.scrollDisable, scope)) {
                    scope.page++;
                    if (attrs.scroll) scope.$eval(attrs.scroll, scope);
                    scope.$apply();
                }
            });

        }
    };
}
]);

/*
 * Управление высoтой блока
 * @params heigth-razer
 */
module.directive('heigthRazer', ['$window', function ($window) {
    return {
        link: function (scope, elem, attrs) {
            scope.onResize = function () {
                $(elem).height($window.innerHeight - parseInt(attrs.heigthRazer, 10));
            };
            scope.onResize();

            angular.element($window).bind('resize', function () {
                scope.onResize();
            })
        }
    }
}]);

/**
 * Управление системными сообщениями на странице
 */
module.factory('alertService', function ($rootScope, $timeout) {

    // create an array of alerts available globally
    $rootScope.alerts = [];

    return alertService = {
        add: function (type, msg, timeout) {
            timeout = timeout || 5000;

            $rootScope.alerts.push({
                type: type,
                msg: msg,
                close: function () {
                    return alertService.closeAlert(this);
                }
            });

            if (timeout) {
                $timeout(function () {
                    alertService.closeAlert(this);
                }, timeout);
            }
        },
        closeAlert: function (alert) {
            return this.closeAlertIdx($rootScope.alerts.indexOf(alert));
        },
        closeAlertIdx: function (index) {
            return $rootScope.alerts.splice(index, 1);
        }

    };
});

var redactorOptions = {};
module.constant('redactorOptions', redactorOptions)
    .directive('redactor', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {

                var updateModel = function updateModel(value) {
                        // $timeout to avoid $digest collision
                        $timeout(function () {
                            scope.$apply(function () {
                                ngModel.$setViewValue(value);
                            });
                        });
                    },
                    options = {
                        changeCallback: updateModel
                    },
                    additionalOptions = attrs.redactor ?
                        scope.$eval(attrs.redactor) : {},
                    editor,
                    $_element = angular.element(element);

                angular.extend(options, redactorOptions, additionalOptions);

                // put in timeout to avoid $digest collision.  call render() to
                // set the initial value.
                $timeout(function () {
                    editor = $_element.redactor(options);
                    ngModel.$render();
                });

                ngModel.$render = function () {
                    if (angular.isDefined(editor)) {
                        $timeout(function () {
                            $_element.redactor('set', ngModel.$viewValue || '');
                        });
                    }
                };
            }
        };
    }]);

module.directive('userFilter', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'adm/partials/user_filter.html',
        link: function (scope) {
            scope.searchModels = {};
            scope.searchModels.name = "";
            scope.searchModels.login = "";
            scope.searchModels.email = "";
            scope.hideFilter = true;

            scope.toggleFilter = function(){
                scope.hideFilter = !scope.hideFilter;
            }

            scope.seachUsers = function () {
                var params = {};
                for (var i in scope.searchModels) {
                    if (scope.searchModels[i].length > 0)
                        params[i] = scope.searchModels[i];
                }
                scope._loadPage = scope.loadPage;
                scope.loadPage = scope.searchByParam;
                scope.searchByParam(params);
            }

            scope.getUsers = function (term) {
                return $http.get(scope.$parent.cfg.url, {params: {
                    name: term
                }}).then(function (res) {
                    var users = [];
                    for (var i in res.data.data) {
                        users.push({name: res.data.data[i].name});
                    }
                    return users;
                });
            }

            scope.clearFilter = function () {
                scope.searchModels.name = "";
                scope.searchModels.login = "";
                scope.searchModels.email = "";
                scope.searchModels.type_id = "";
                scope.loadPage = scope._loadPage;
                scope.clearMainFilter();
                scope.loadPage();
            }

        }
    };
});


module.directive('ngSetFocus', function () {
    return function (scope, element, attrs) {
        scope.$watch(attrs.ngSetFocus,
            function (newValue) {
                newValue && element.focus();
            }, true);
    };
});
module.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

module.directive('editField', function ($http) {
    return {
        restrict: 'E',
        templateUrl: 'adm/partials/edit-field.html',
        scope: {
            editType: "=",
            itemVal: "=",
            field: "=",
            itemId: "="
        },
        controller: function ($scope) {

            if ($scope.editType == "popup") {
                $scope.itemVal = $scope.itemVal || "no text set";
            }

            if ($scope.editType == "text_to_select") {
                $scope.itemVal = $scope.itemVal.split(',');
            }

            $scope.selectEdit = function () {
                var params = {};
                params[$scope.field.name] = $scope.itemVal;
                $scope.$parent.updateFieldByName(params, $scope.itemId, function () {

                });
            }

            $scope.editText = function () {
                var params = {};
                params[$scope.field.name] = $scope.itemVal;
                $scope.$parent.updateFieldByName(params, $scope.itemId, function () {
                    //$scope.showInput=false;
                });
            }

            $scope.selectEditText = function () {
                var params = {};
                params[$scope.field.name] = $scope.itemVal.join();
                $scope.$parent.updateFieldByName(params, $scope.itemId, function () {
                    //$scope.showInput=false;
                });
            }

            $scope.popupEdit = function () {
                var tmpVal = prompt("Edit data", $scope.itemVal);
                if (tmpVal) {
                    var params = {};
                    params[$scope.field.name] = tmpVal;
                    $scope.$parent.updateFieldByName(params, $scope.itemId, function () {
                        $scope.itemVal = tmpVal;
                    });
                }
            }


        },
        link: function (scope) {
        }
    };
});

(function(){
    var matched, browser;

    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie)[\s?]([\w.]+)/.exec( ua ) ||
            /(trident)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
//IE 11+ fix (Trident)
    matched.browser = matched.browser == 'trident' ? 'msie' : matched.browser;
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;
})();
