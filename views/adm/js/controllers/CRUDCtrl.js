function _forEach( self, cb){
    for(var i in self){
        cb.call(self, self[i], i, self);
    }
}
function findInArrByKey( self, key, value){
    for(var i =0; i< self.length; i++){
        if(self[i].hasOwnProperty(key) && self[i][key] == value){
            return i;
        }
    }
    return false;
}

/**
 * Задать секции в панеле крошек
 * @param array bc массив строк
 */
function setBreadcrumb(bc){
    var container = angular.element(document.querySelector('#breadcrumb'));
    container.empty();
    container.append('<li><a href="#/">Панель управления</a></li>');

    angular.forEach(bc,function(value, key){
        container.append('<li><a href="#/'+key+'">'+value+'</a></li>');
    });
}
String.prototype.format = function() {
    var str = this.toString();
    if (!arguments.length)
        return str;
    var args = typeof arguments[0],
        args = (("string" == args || "number" == args) ? arguments : arguments[0]);
    for (arg in args)
        str = str.replace(RegExp("\\{" + arg + "\\}", "gi"), args[arg]);
    return str;
}


module.controller('CollectionCtrl', function( $scope, CRUDConfig, $http, $location, alertService, $timeout, $sce){

    // Текст поиска
    $scope.searchstr    = '';
    // Поле поиска
    $scope.field        = CRUDConfig.searchByField || 'name';
    $scope.data         = [];
    $scope.isCollection = true;
    $scope.page         = 1;
    $scope.complete     = false;
    $scope.loading      = false;
    $scope.markedAllToDelete = false;

    // Сортировка
    $scope.sortField    = '';
    $scope.revs         = true;
    $scope.sortChange   = function(v){
        if(!$scope.complete){
            $scope.data    = [];
            $scope.page    = 1;
            $scope.loading = false;
        }
        if($scope.sortField == v){
            $scope.revs = !$scope.revs;
        }
        else{
            $scope.revs = false;
        }
        $scope.sortField = v;
        if(!$scope.complete) $scope.loadPage( this.searchstr);
    };

    CRUDConfig.itemUrl  = $location.url();
    $scope.cfg          = CRUDConfig;

    setBreadcrumb($scope.cfg.breadcrumb);

    $scope.loadPage = function(searchstr){
        if ($scope.loading) return;
        $scope.loading = true;
        $scope.complete = false;
        $http.get(CRUDConfig.url, {params: { page: $scope.page, "wheres[]": [$scope.field, "LIKE", searchstr],
            order_field: $scope.sortField, order_dir: ($scope.revs ? 'DESC' :  'ASC') }})
            .success(function(response){
                if(angular.isArray(response.data) && response.data.length){

                    $scope.data = $scope.data.concat(response.data);
                /*  попытка вывести отформатированный текст в админке в списках
                    _forEach($scope.data, function( e, i){
                        $scope.data[i].content = $sce.trustAsHtml($scope.data[i].content);
                    }); */
                    if(response.data.length < 10){
                        $scope.complete = true;
                    }else{
                        $scope.loading = false;

                    }
                }
                else{
                    $scope.complete = true;
                }
            });

        $scope.page++;
    };

    var timeouts = [];
    $scope.search       = function(){
        $scope.loading = true;
        $scope.page    = 1;

        // удалить все проки ng-change до этого момента.
        if(timeouts.length) angular.forEach(timeouts, function(to, i){ $timeout.cancel(to); timeouts.splice(i, 1); });
        timeouts.push($timeout(function(){

            // загрузить данные, если последнюю секунду запрос не изменился.
            $http.get( CRUDConfig.url, { params: { page: $scope.page, "wheres[]": [$scope.field, "LIKE", this.searchstr],
                order_field: $scope.sortField, order_dir: ($scope.revs? 'DESC': 'ASC') }})
                .success(function( response){
                    $scope.data = response.data;
                    $scope.loading = false;
                });
            $scope.page++;
        }.bind(this), 1000));
    };

    $scope.buttonAction = function(action, href){
        return $scope.$eval(action);
    };

    $scope.parse = function( href, id){
        return $scope.$eval( href)+id;
    };
    $scope.deleteItemsList = function(){
        //todo: добавить проверку на кол-во выбранных позиций
        $.SmartMessageBox({
            sound:false,
            title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> Удалить выбранные записи</span> ?",
            content: "...",
            buttons: '[Нет][Да]'

        }, function (ButtonPressed) {
            if (ButtonPressed == "Да") {
                var _del = [];
                for(var i = $scope.data.length-1; i >= 0; i--){
                    if($scope.data[i].delete) _del.push($scope.data[i].id);
                }

                $http.delete(CRUDConfig.url, { params: { "ids[]" : _del } })
                    .success(function(res){
                        for(var i = $scope.data.length-1; i >= 0; i--){
                            if($scope.data[i].delete){
                                $scope.data.splice(i, 1);
                            }
                        }
                    });

                alertService.add("success", "Выбранные записи удалены");
//                $.root_.addClass('animated fadeOutUp');
//                setTimeout(logout, 1000)
            }

        });
    };

    $scope.markAllToDelete = function( v){
        _forEach($scope.data, function( e, i){
            $scope.data[i].delete = !v;
        });
    };

    $scope.deleteItem = function(id){
        $.SmartMessageBox({
            sound:false,
            title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> Удалить выбранную запись</span> ?",
            content: "...",
            buttons: '[Нет][Да]'
        }, function (ButtonPressed) {
            if (ButtonPressed == "Да") {
                var request = $http.delete(CRUDConfig.url, { params: { "ids[]": [id] }});
                request
                    .then(
                    function() {
                        var deletedElem = findInArrByKey( $scope.data, 'id', id);
                        $scope.data.splice( deletedElem, 1);
                        alertService.add("success", "Запись удалена");
                       $location.path($location.url());
                    },
                    function() {
                        alertService.add("danger", "Ошибка удаления");
                        $location.path($location.url());
                    }
                );

           }
        });
    };

    $scope.execute = function(fn, item){
        return $scope.$eval(fn, this).call(this, item);
    };

    $scope.getOperation = function( op){
        if(op.purchase)
            return $sce.trustAsHtml('Покупка услуги <b>{0}</b> пользователем  <a href="#/users/{1}"><b>{2}</b></a> до <b>{3}</b>'.format(op.purchase, op.user_id, op.user.name, op.paidTill));
        if(op.user && op.user_to)
            return $sce.trustAsHtml('Перевод средств от <a href="#/users/{0}"><b>{1}</b></a>  к  <a href="#/users/{2}"><b>{3}</b></a>'.format(op.user_id, op.user.name, op.user_id_to, op.user_to.name));

        return $sce.trustAsHtml('Пополнение счета пользователем  <a href="#/users/{0}"><b>{1}</b></a>'.format( op.user_id, op.user.name));
    }

    $scope.clearMainFilter = function(){
        $scope.searchComplete = false;
        $scope.data = [];
        $scope.page = 1;
    }

    $scope.searchByParam = function (params) {
        $scope.serchParams = params || $scope.serchParams;
        if ($scope.loading || $scope.searchComplete) return;

        $scope.loading = true;

        $http.get(CRUDConfig.url, {params: $scope.serchParams})
            .success(function (response) {
                if (angular.isArray(response.data) && response.data.length) {
                    $scope.data = response.data;
                    $scope.loading = false;
                    $scope.complete = true;
                    $scope.searchComplete = true;
                }
            }).error(function(response){
                $scope.loading = false;
                $.SmartMessageBox({
                    sound:false,
                    title: "<i class='fa txt-color-orangeDark'></i> Поиск не дал результатов</span>",
                    content: "",
                    buttons: '[ОК]'
                });
            })
        ;
    };

    $scope.updateFieldByName = function (params, id, onSuccess, onError) {

        $http.put(CRUDConfig.url + "/" + id, params)
            .success(function (response) {
                if (typeof(onSuccess) == "function") {
                    onSuccess(response);
                }
            }).error(function (response) {
                if (typeof(onError) == "function") {
                    onError(response)
                }
                $.SmartMessageBox({
                    sound: false,
                    title: "<i class='fa txt-color-orangeDark'></i> Error!</span>",
                    content: "",
                    buttons: '[ОК]'
                });
            })

    }
});

module.controller('RowCtrl', function( $scope, CRUDConfig, $http, $location, $modal, alertService){

    $scope.isCollection  = false;
    CRUDConfig.backUrl   = $location.url().split('/').slice(0, -1).join('/');
    $scope.cfg           = CRUDConfig;
    $scope.cfg.entity    = CRUDConfig.url.split('/').slice(1, -1).join('');
    $scope.cfg.id        = CRUDConfig.url.split('/').slice(2,3).join('');
    $scope.data          = {};

    setBreadcrumb($scope.cfg.breadcrumb);

    $http.get(CRUDConfig.url)
        .success(function(response){

            if (CRUDConfig.fields === undefined) {
                CRUDConfig.fields = response.data?  Object.keys(response.data) : Object.keys(response);
            }

            $scope.data = response.data || response;
        //    $scope.data.prices = !$scope.data.prices? [] : $scope.data.prices; зачем это повсюду слать?
        })
        .error(function(error) {
            var errorMessage = (error.status === 403) ? error.data : error.statusText;
//            console.log('REQUEST FAILED', CRUDConfig.url, errorMessage);
            alertService.add("danger", "REQUEST FAILED: "+errorMessage+' ('+CRUDConfig.url+')');
        });

    // shitti hack against Two-way binding. ;(
    function getObj_RelatedDataInIds( d){
        var result = {};
        _forEach(d, function( value, key){
            if(key == CRUDConfig.related.field){
                _forEach(value, function( obj){
                    if(!result[key]) result[key] = [];
                    result[key].push( obj.id);
                })
            }
            else
                result[key] = value;
        });
        return result;
    }

    $scope.buttonAction = function(action, href, global){
        if(global && href){
            window.location.pathname = $scope.$eval(href);
            return;
        }
        if(href){
            $location.path($scope.$eval(href));
        }
        return $scope.$eval(action);
    };

    $scope.sendForm = function(data, callback) {
        var request;

        angular.forEach($scope.cfg.fields, function(f){
            if(f.type == 'checkbox' && !data[f.name]) data[f.name] = 'no';
            if(f.type == 'imageOLD') {
                if (data[f.name]) {
                    fileUpload.uploadFileToUrl($scope.myFile, '/upload');
                } else {
                    // как-то удалить файл
                }
            }
        });
        if(CRUDConfig.related && CRUDConfig.related.field && !data[CRUDConfig.related.field]) data[CRUDConfig.related.field] = [];

//        if (angular.isNumber(data.id)) {
        if (data.id) {
            request = $http.put(CRUDConfig.url, CRUDConfig.related && data[CRUDConfig.related.field]? getObj_RelatedDataInIds(data) : data);
        } else {
            data.id = undefined;
            request = $http.post(CRUDConfig.url, CRUDConfig.related && data[CRUDConfig.related.field]? getObj_RelatedDataInIds(data) : data);
        }
        request
            .then(
            function(response) {
                alertService.add("success", "Данные сохранены");
                if (callback){
                    callback( response);
                }
            },
            function() {
                alertService.add("danger", "Ошибка сохранения данных");
//                console.log('error saving');
            }
        );
    };
    $scope.applyItem = function(){
        $scope.sendForm($scope.data);
    };

    $scope.saveItem = function(){
        $scope.sendForm($scope.data, function(){
            window.location.href = '#'+$scope.cfg.backUrl;
        });
    };
    $scope.deleteItem = function( d){
        var deletedElem = findInArrByKey( $scope.data[CRUDConfig.related.field], 'id', d.id);
        $scope.data[CRUDConfig.related.field].splice( deletedElem, 1);
    };

    $scope.addRelatedData = function(){
        var modalInstance = $modal.open({
            templateUrl: 'myModalContent.html',
            controller: "addCtrl",
            size: 'lg',

            resolve: {
                cfg: function(){
                    return $scope.cfg.related;
                },
                data: function(){
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function( rdata){
            if(rdata.length > 0) alertService.add("success", "Треки добавлены");
            if(!$scope.data[CRUDConfig.related.field]) $scope.data[CRUDConfig.related.field] = [];
            $scope.data[CRUDConfig.related.field] = $scope.data[CRUDConfig.related.field].concat( rdata);
        });
    };

    $scope.addNewRelatedData = function(){
        var modalInstance = $modal.open({
            templateUrl: 'myNewModalContent.html',
            controller: "newCtrl",
            size: 'lg',

            resolve: {
                cfg: function(){
                    return $scope.cfg.related;
                },
                data: function(){
                    return $scope.data;
                }
            }
        });

        modalInstance.result.then(function( rdata){
            if(rdata.length > 0) alertService.add("success", "Треки добавлены");
            if(!$scope.data[CRUDConfig.related.field]) $scope.data[CRUDConfig.related.field] = [];
            $scope.data[CRUDConfig.related.field] = $scope.data[CRUDConfig.related.field].concat( rdata);
        });
    };


});
module.controller('addCtrl', function( $scope, $modalInstance, data, cfg, $http, $timeout){

    $scope.cfg         = cfg;
    // Все данные
    $scope.relatedData = [];
    $scope.uploadAnotherPage = function(){
        var self = this;
        $http.get(cfg.url, { params: { page: this.page } })
            .success(function(response){
                $scope.relatedData = $scope.relatedData.concat(response.data);
                self.validate( response.data.length);
            });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.data = data;
    $scope.add = function(){
        $modalInstance.close($scope.package);
    };

    // Поиск
    $scope.searchData = [];
    $scope.searching = false;
    $scope.page = 1;
    $scope.search = function(){

        var self = this;
        $http.get(cfg.url, { params: { text: $scope.searchStr, page: $scope.page } })
            .success(function( response){
                $scope.searchData = $scope.searchData.concat( response.data);
                self.validate( response.data.length);
            });
        $scope.page++;
    };

    $scope.searchOn = function(){
        $scope.loading = true;
        $scope.searchOff();
        $scope.relatedData = [];

        $timeout(function(){
            $scope.searching = true;
            $scope.loading = false;
        }, 1000);

    };
    $scope.searchOff = function(){

        $scope.page = 1;
        $scope.searchData = [];
        $scope.searching = false;
        $scope.isSDone = false;
    };
    $scope.searchstrchanged = function(){
        $scope.searchStr = this.searchStr;
    };

    // Добавленые
    $scope.package = [];
    $scope.addToPackage = function( add, elem){
        if(add){
            $scope.package.push( elem)
        }
        else{
            $scope.package.splice( $scope.package.indexOf(elem), 1);
        }
    };

});

// по совместительству
module.controller('newCtrl', function($scope, $modalInstance, data, cfg, $http, $timeout){
    $scope.cfg         = cfg;
    // Все данные
    $scope.relatedData = [];

    $http.get(cfg.url + data.id + '/' + data.id)
        .success(function(response){

            if (cfg.fields === undefined) {
                cfg.fields = response.data ? Object.keys(response.data) : Object.keys(response);
            }

            $scope.data = response.data || response;
            $scope.package = 1;
        //    $modalInstance.close($scope.package);

            //    $scope.data.prices = !$scope.data.prices? [] : $scope.data.prices; зачем это повсюду слать?
        })
        .error(function(error) {
            var errorMessage = (error.status === 403) ? error.data : error.statusText;
//            console.log('REQUEST FAILED', CRUDConfig.url, errorMessage);
         //   alertService.add("danger", "REQUEST FAILED: "+errorMessage+' ('+cfg.url+')');
            $modalInstance.dismiss('cancel');
        });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.data = data;

    $scope.saveItem = function(){
        $scope.sendForm($scope.data, function(){
            $modalInstance.close($scope.package);
        });
    };
    $scope.sendForm = function(data, callback) {
        var request;

        angular.forEach($scope.cfg.fields, function(f){
            if(f.type == 'checkbox' && !data[f.name]) data[f.name] = 'no';
            if(f.type == 'image') {
                alert(data[f.name]);
                if (data[f.name]) {
                    fileUpload.uploadFileToUrl($scope.myFile, '/upload');
                } else {
                    // как-то удалить файл
                }
            }
        });
        //    if(CRUDConfig.related && CRUDConfig.related.field && !data[CRUDConfig.related.field]) data[CRUDConfig.related.field] = [];

//        if (angular.isNumber(data.id)) {
        if (data.id) {
            request = $http.put(cfg.url,  data);
        } else {
            data.id = undefined;
            request = $http.post(cfg.url,data);
        }
        request
            .then(
            function(response) {
                alertService.add("success", "Данные сохранены");
                if (callback){
                    callback( response);
                }
            },
            function() {
                alertService.add("danger", "Ошибка сохранения данных");
//                console.log('error saving');
            }
        );
    };
});


module.controller('redactorTemplates', function($scope, $http, CRUDConfig){
    $http.get(CRUDConfig)
        .success(function(response){
            $scope.template = response;
        });

    setBreadcrumb(['Настройка шаблонa']);
    $scope.save = function(){
        $http.put(CRUDConfig, { template: $scope.htmldd })
    };
});

module.controller('welcomePageCtrl', function($scope, $http, CRUDConfig, alertService){
    setBreadcrumb(['Добро пожаловать']);
    alertService.add("success","Аэропорт Внуково наш!");
});
