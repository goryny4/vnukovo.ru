<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Vnukovo Admin Panel</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <style>
        .progress-indicator {
            position: fixed;
            top:0;
            left:0;
            color: white;
            width: 100px;
            height: 20px;
            background-color: red;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
    <script src="libs/jquery.caret.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script src="libs/angular.min.js"></script>
    <script src="libs/angular-locale_ru.js"></script>
    <script src="libs/angular-loader.min.js"></script>
    <script src="libs/angular-route.min.js"></script>
    <script src="libs/angular-resource.min.js"></script>
    <script src="libs/angular-animate.min.js"></script>
    <script src="libs/angular-sanitize.min.js"></script>
    <script src="libs/underscore-min.js"></script>
    <script src="libs/ui-bootstrap-tpls-0.11.0.min.js"></script>
    <script src="//oss.maxcdn.com/angular.strap/2.0.0/angular-strap.min.js"></script>
    <script src="//oss.maxcdn.com/angular.strap/2.0.0/angular-strap.tpl.min.js"></script>


    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <?= HTML::script('adm/js/adminApp.js')?>
    <?= HTML::script('common/js/scripts.js')?>
    <?= HTML::script('adm/js/scripts.js')?>
    <?= HTML::script('adm/partials/partials.js')?>
    <?= HTML::script('libs/SmartNotification.min.js')?>
    <?= HTML::script('libs/jarvis.widget.min.js')?>
    <?= HTML::script('libs/ng-infinite-scroll.js')?>

    <!-- таблицы-->
    <!--    <script src="libs/plugin/datatables/jquery.dataTables-cust.min.js"></script>-->
    <!--    <script src="libs/plugin/datatables/ColReorder.min.js"></script>-->
    <!--    <script src="libs/plugin/datatables/FixedColumns.min.js"></script>-->
    <!--    <script src="libs/plugin/datatables/ColVis.min.js"></script>-->
    <!--    <script src="libs/plugin/datatables/ZeroClipboard.js"></script>-->
    <!--    <script src="libs/plugin/datatables/media/js/TableTools.min.js"></script>-->
    <!--    <script src="libs/plugin/datatables/DT_bootstrap.js"></script>-->

    <?= HTML::style('adm/css/styles.css') ?>
    <?= HTML::style('adm/css/font-awesome.min.css') ?>
    <?= HTML::style('adm/css/smartadmin-production.css') ?>
    <?= HTML::style('adm/css/smartadmin-skins.css') ?>

    <?= HTML::style('adm/css/smartadmin-skins.css') ?>
    <?= HTML::script('libs/jquery.datetimepicker.js')?>
    <?= HTML::script('libs/ng-flow-standalone.min.js')?>

</head>

<body ng-app="VnukovoAdminApp" class="fixed-page-footer fixed-header fixed-navigation fixed-ribbon">

<?= View::make('adm._header') ?>
<?= View::make('adm._left') ?>
<?= View::make('adm._main') ?>
<?= View::make('adm._footer') ?>
<?= View::make('adm._shortcat') ?>

<?= HTML::script('adm/js/themeApp.js')?>

</body>
</html>