<!-- Left panel : Navigation area -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a > <!--href="javascript:void(0);" id="show-shortcut"-->
                        <!--img src="../../front/adm/images/male.png" alt="me" class="online"/-->
						<span>
						<?=Auth::user()->username?>
						</span>
                        <!--i class="fa fa-angle-down"></i-->
                    </a>

				</span>
    </div>
    <!-- end user info -->

    <nav>

        <ul>
            <li>
                <a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Панель управления</span></a>
            </li>
            <?php if (in_array(Auth::user()->group,array('content','superadmin'))): ?>
            <li id="content-list">
                <a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span
                        class="menu-item-parent">Контент</span></a>
                <ul>
					<li><a href="#/news">Новости</a></li>
					<li class="divider"></li>
                    <li><a href="#/duty-free">Магазины Duty Free</a></li>
                    <li><a href="#/cafes-and-restaurants">Кафе и рестораны</a></li>
                    <li><a href="#/car-rent">Прокат авто</a></li>
                    <li class="divider"></li>
                    <li><a href="#/services">Услуги</a></li>
                    <li><a href="#/how-to-get">Как добраться</a></li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (in_array(Auth::user()->group,array('content','superadmin'))): ?>
            <li id="ad-list">
                <a href="#"><i class="fa fa-lg fa-fw fa-briefcase"></i> <span class="menu-item-parent">Реклама</span></a>
                <ul>
                    <li><a href="#/fullscreen-ads">Полноэкранная реклама</a></li>
                    <li><a href="#/ads-block">Рекламные блоки</a></li>
                    <li><a href="#/link-ads">Рекламные ссылки</a></li>
                    <li><a href="#/special-proposals">Специальные предложения</a></li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (in_array(Auth::user()->group,array('analyst','superadmin'))): ?>
            <li id="statistics-list">
                <a href="#"><i class="fa fa-lg fa-fw fa-tachometer"></i> <span class="menu-item-parent">Статистика</span></a>
                <ul>
                    <li><a href="#/users"><i class="fa fa-fw fa-user "></i>Пользователи</a></li>
                    <li><a href="#/ads-list">Рекламные показы</a></li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (in_array(Auth::user()->group,array('admin','superadmin'))): ?>
            <li id="system-list">
                <a href="#"><i class="fa fa-lg fa-fw fa-gears "></i> <span
                        class="menu-item-parent">Система</span></a>
                <ul>
                    <li><a href="#/maps">Карты терминалов</a></li>
                    <li><a href="#/cities">Города</a></li>
                    <li><a href="#/airlines">Авиакомпании</a></li>
                    <li><a style="color:#252525" href="#/settings">Настройки</a></li>

                    <!--li><a href="#/tracks"><i class="fa fa-fw fa-music "></i>Треки</a></li>
                    <li><a href="#/playlists"><i class="fa fa-fw fa-tasks"></i> Плейлисты</a></li>
                    <li><a href="#/musicstyles">Стили</a></li>
                    <li><a href="#/MarkSystem">Система оценок</a></li>
                    <li><a href="#/tops">Топы</a></li>
                    <li><a href="#/shops">Магазины</a></li>
                    <li class="divider"></li>
                    <li><a href="#/files">Файлы</a></li>
                    <li><a href="#/files?mod=config">Конфигурации</a></li>
                    <li class="divider"></li>
                    <li><a href="#/radio">Радио</a></li>
                    <li><a href="#/jingles">Джинглы</a></li>
                    <li><a href="#/RadioGrid">Сетка вещания</a></li-->
                </ul>
            </li>
            <?php endif; ?>
            <?php if (in_array(Auth::user()->group,array('admin','superadmin'))): ?>
            <li id="access-list">
                <a href="#"><i class="fa fa-lg fa-fw fa-group "></i> <span
                        class="menu-item-parent">Доступ к админке</span></a>
                <ul>
                    <li><a href="#/moders"><i class="fa fa-fw fa-user "></i>Аккаунты</a></li>
                    <li><a href="#/logs">История операций</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </nav>
    <span class="minifyme"> <i class="fa fa-arrow-circle-left hit"></i> </span>
</aside>
<!-- END NAVIGATION -->