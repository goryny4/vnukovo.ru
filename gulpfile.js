var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var minifycss = require('gulp-minify-css');
var gutil = require("gulp-util");
var gulpif = require("gulp-if");
var concat = require('gulp-concat');
var template = require('gulp-angular-templatecache');
var es = require('event-stream');
var uglify = require('gulp-uglify');
var ngmin = require('gulp-ngmin');
var fs = require('fs');
var path = require('path');
var apidoc = require('gulp-apidoc');

var languages = ['rus'];

function taskStylesFactory(dir) {
	'use strict';
	gulp.src('styles.scss', {cwd: 'views/' + dir + '/css'})
		.pipe(plumber())
		.pipe(sass({onError: errorHandler}))
		.pipe(autoprefixer('> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1', 'Explorer 9', 'iOs 6'))
		//.pipe(gulpif(isProduction(), minifycss()))
		.pipe(concat('styles.css'))
		.pipe(gulp.dest('', {cwd: 'public/' + dir + '/css'}));
	gulp.src('*.css', {cwd: 'views/' + dir + '/css'})
		.pipe(plumber())
		//.pipe(gulpif(isProduction(), minifycss()))
		.pipe(gulp.dest('', {cwd: 'public/' + dir + '/css'}));
}

function taskModuleFactory(name) {
	'use strict';
	var files = ['module.prefix', '**/*.js', 'module.suffix', '!module_*.js', '!apps/**'];
	if (!isDebugMode()) {
		files.push('!debug.js');
	}
	gulp.src(files, {cwd: 'views/' + name + '/js'})
		.pipe(plumber())
		.pipe(concat('scripts.js'))
		.pipe(gulpif(isProduction(), ngmin()))
//		.pipe(gulpif(isProduction(), uglify()))
		.pipe(gulp.dest('', {cwd: 'public/' + name + '/js'}));


	files = ['apps/*.js'];
	gulp.src(files, {cwd: 'views/' + name + '/js'})
//		.pipe(plumber())
		.pipe(gulpif(isProduction(), ngmin()))
//		.pipe(gulpif(isProduction(), uglify()))
		.pipe(gulp.dest('', {cwd: 'public/' + name + '/js'}));
}

function taskPartialsFactory(name, opts) {
	'use strict';
	var cwd = opts.cwd;
	var module = opts.module;
	var root = opts.root;
//	gulp.task(name, function() {
		var tasks = [];
		for (var i in languages) {
			var lang = languages[i];
			gulp.src(['**/*.html'], {cwd: cwd})
				.pipe(plumber())
				.pipe(renderPartial({lang: lang}))
				.pipe(template('partials.js', {module: module, root: root, standalone: opts.standalone}))
				//.pipe(gulpif(isProduction(), uglify()))
				.pipe(gulp.dest('', {cwd: 'public/' + name + '/partials'}));
		}
		return tasks;
//	});
}

function getFolders(dir){
	'use strict';
	return fs.readdirSync(dir)
		.filter(function(file){
			return fs.statSync(path.join(dir, file)).isDirectory();
		});
}

gulp.task('styles', function() {
	'use strict';
	var folders = getFolders('views');
	folders.map(function (folder){
		taskStylesFactory(folder);
	});
});
gulp.task('modules', function() {
	'use strict';
	var folders = getFolders('views');
	folders.map(function (folder){
		taskModuleFactory(folder);
	});
});
gulp.task('partials', function() {
	'use strict';
	var folders = getFolders('views');
	folders.map(function (folder){
		taskPartialsFactory(folder, {cwd: 'views/' + folder + '/partials', module: 'templates', root: folder + '/partials', standalone: true});
	});
});


gulp.task('default', ['apidoc'],  function(done) {// 'styles', 'modules', 'partials'], function(done) {
	'use strict';
	if (isProduction()) {
		done();
		return;
	}
    gulp.watch('laravel/app/controllers/*.php', ['apidoc']);
    gulp.watch('**/*.scss', {cwd: 'views'}, ['styles']);
    gulp.watch('**/*.css', {cwd: 'views'}, ['styles']);
	gulp.watch(['**/*.html'], {cwd: 'views'}, ['partials']);

	var files = ['module.prefix', '**/*.js', 'module.suffix', '!module.js', '!apps/**'];
	if (!isDebugMode()) {
		files.push('!debug.js');
	}
	gulp.watch(files, {cwd: 'views'}, ['modules']);
	gulp.watch(['*.ini'], {cwd: 'langs'}, ['partials']);
});


gulp.task('apidoc', function(){
    apidoc.exec({
        src: "laravel/app/controllers/",
        dest: "doc/"
    });
});


function errorHandler(err) {
	'use strict';
	if (!(err instanceof Error)) {
		err = new Error(err);
	}
	require('node-notifier').notify({message: err.message});
	console.log(gutil.colors.red(err.message));
}

function plumber(options) {
	'use strict';
	options = Object(options) || {};
	options.errorHandler = errorHandler;
	return require('gulp-plumber')(options);
}

function isProduction() {
	'use strict';
	return gutil.env.type === 'production';
}

function isDebugMode() {
	'use strict';
	return gutil.env['js-debug'];
}

function files() {
	'use strict';
	return es.map(function(file, cb) {
		console.log(file.path);
		cb(null, file);
	});
}

function renderPartial(options) {
	'use strict';
	options = Object(options) || {};

	var ini = require('ini');
	var path = require('path');
	var fs = require('fs');
	var util = require('util');
	var langs = ini.parse(fs.readFileSync(path.join(process.cwd(), 'views/lang.ini'), {encoding: 'utf8'}));
	var strings = {};
	Object.keys(langs).forEach(function(name) {
		var section = langs[name];
		Object.keys(section).forEach(function(name) {
			strings[name] = section[name];
		});
	});

	function s(key) {
		var args = Array.prototype.slice.call(arguments, 1);
		if (!strings.hasOwnProperty(key)) throw new Error('Undeclared lang identifier: '+key);
		args.unshift(strings[key]);
		return util.format.apply(util, args);
	}
	function date(format) {
		var dt = new Date();
		var f = {
			Y: function (dt) { return dt.getFullYear(); }
		};
		return format.replace(/[\\]?([a-zA-Z])/g, function (m0, m) {
			if (m0 !== m || !f.hasOwnProperty(m)) {
				return m;
			} else {
				return f[m](dt);
			}
		});
	}


	return es.map(function(file, cb) {
		try {
			file.contents = new Buffer(render(file.contents.toString(), options));
		} catch (err) {
			cb(new gutil.PluginError('gulp-render', err));
		}
		cb(null, file);
	});

	function render(content, options) {
		return content.replace(/<\?=([^?]+)\?>/g, function(match, expr) {
			return eval(expr);
		});
	}
}
